import { result_model } from "./result_model.interface";

export interface doing_job_result_view_model extends result_model {
  id_done: boolean;
}
