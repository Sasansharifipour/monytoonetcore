import { result_model } from "./result_model.interface";

export interface users_share_my_basket_result_view_model extends result_model {
  informations: number;
}

