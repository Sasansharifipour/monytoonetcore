export interface users_get_get_my_balance_Result {
  real_balance: number;
  virtual_balance: number;
}
