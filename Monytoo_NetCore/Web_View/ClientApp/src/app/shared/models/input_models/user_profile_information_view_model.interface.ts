export interface user_profile_information_view_model {
  key_name: string;
  name: string;
  value: string;
  parent_url: string;
}
