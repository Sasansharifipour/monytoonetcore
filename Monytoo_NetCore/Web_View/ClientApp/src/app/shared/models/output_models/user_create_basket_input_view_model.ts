import { get_base_data_model } from "./get_base_data_model.interface";

export interface user_create_basket_input_view_model extends get_base_data_model {
  name: string;
  basket_balance: number;
  type_id: number;
}
