﻿using System;

namespace DataLayer.Models
{
    public class Engine_Codal_Temp
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
        public string Namad_name { get; set; }
        public Nullable<long> Post_Id { get; set; }
        public System.DateTime Insert_date { get; set; }
    }
}
