import { get_base_data_model } from "./get_base_data_model.interface";

export interface users_create_factor_API_View_Model extends get_base_data_model {
  package_id: number;
  coupon_id: number;
}
