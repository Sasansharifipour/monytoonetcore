import { get_base_data_model } from "./get_base_data_model.interface";

export interface user_create_basket_order_input_view_model extends get_base_data_model {
  namad_id: number;
  order_price: number;
  count: number;
  basket_id: number;
}
