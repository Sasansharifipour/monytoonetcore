﻿using System;

namespace DataLayer.Models
{
    public class users_get_share_baskets_Result
    {
        public Nullable<long> ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> total_value { get; set; }
        public Nullable<System.DateTime> create_time { get; set; }
        public Nullable<System.DateTime> last_evaluation_time { get; set; }
        public Nullable<bool> is_shareable { get; set; }
        public Nullable<int> item_count { get; set; }
        public Nullable<System.DateTime> expiration_date { get; set; }
    }
}
