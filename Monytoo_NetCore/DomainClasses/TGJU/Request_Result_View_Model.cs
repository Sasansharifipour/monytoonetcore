﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.TGJU
{
    public class Request_Result_View_Model
    {
        public int total { get; set; } = 0;

        public int per_page { get; set; } = 0;

        public int current_page { get; set; } = 0;

        public int last_page { get; set; } = 0;

        public string next_page_url { get; set; } = "";

        public string prev_page_url { get; set; } = "";

        public int from { get; set; } = 0;

        public int to { get; set; } = 0;

        public List<TGJU_Data_View_Model> data { get; set; } = new List<TGJU_Data_View_Model>();
    }
}