import { get_base_data_model } from "./get_base_data_model.interface";

export interface get_user_follow_view_model extends get_base_data_model {
  user_profile_id: string;
  count: number;
  index: number;
}
