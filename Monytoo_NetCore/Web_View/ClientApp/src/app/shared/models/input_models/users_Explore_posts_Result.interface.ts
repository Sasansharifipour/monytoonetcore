import { SafeUrl } from "@angular/platform-browser";

export interface users_Explore_posts_Result {
  post_id: number;
  caption: string;
  image_id: number;
  comment_count: number;
  from_user_id: number;
  insert_time: Date;
  like_count: number;
  user_id: number;
  username: string;
  name: string;
  lastname: string;
  user_type: number;
  post_image_data: SafeUrl;
  is_liked: string;
  comment_id: number;
  comment_text: string;
  comment_time: Date;
  comment_user_id: number;
  reply_id: number;
  reply_text: string;
  reply_time: Date;
  replay_user_id: number;
  r_number: number;
  profile_image_t: number;
}
