import { get_base_data_model } from "./get_base_data_model.interface";

export interface CommentBaseViewModel extends get_base_data_model {
  comment_id: number;
}
