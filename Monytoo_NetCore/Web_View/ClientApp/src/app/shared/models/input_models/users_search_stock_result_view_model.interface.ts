import { result_model } from "./result_model.interface";
import { users_search_stock_Result } from "./users_search_stock_Result";

export interface users_search_stock_result_view_model extends result_model {
  informations: users_search_stock_Result[];
}

