﻿
namespace DomainClasses.Web_Services
{
    public class add_data_result_view_model : result_model
    {
        public int instrument_count { get; set; } = 0;

        public int added_count { get; set; } = 0;

        public int not_added_count { get; set; } = 0;
    }
}