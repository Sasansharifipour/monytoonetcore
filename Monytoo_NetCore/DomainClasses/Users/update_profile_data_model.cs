﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Users
{
    public class update_profile_data_model : get_base_data_model
    {
        public string username { get; set; }

        public string name { get; set; }

        public string lastname { get; set; }
    }
}