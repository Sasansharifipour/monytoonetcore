﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.RSI
{
    public class RSI_Set_Impact_View_Model : get_base_data_model
    {
        public int namad_id { get; set; } = 0;

        public double strength { get; set; } = 0;
    }
}