import { SafeUrl } from "@angular/platform-browser";

export interface users_get_notification_list_Result {
  id: number;
  create_time: Date;
  is_seen: boolean;
  notification_type_id: number;
  actor_id: number;
  actor_name: string;
  actor_lastname: string;
  actor_username: string;
  actor_type_id: number;
  act_on_id: number;
  act_on_name: string;
  act_on_lastname: string;
  act_on_username: string;
  act_on_type_id: number;
  notification_key_name: string;
  r_number: number;
  actor_profile_image_t: number;
  act_on_profile_image_t: number;
  action: string;
  action_text: string;
  profile_image_data: SafeUrl;
}
