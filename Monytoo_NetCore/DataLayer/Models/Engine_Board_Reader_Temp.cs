﻿
namespace DataLayer.Models
{
    public class Engine_Board_Reader_Temp
    {
        public long ID { get; set; }
        public string Title { get; set; }
        public System.DateTime insert_date { get; set; }
        public string namad_name { get; set; }
        public int idx { get; set; }
    }
}
