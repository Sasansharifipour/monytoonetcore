﻿using System;

namespace DataLayer.Models
{
    public class users_get_last_3_posts_Result
    {
        public long post_id { get; set; }
        public string caption { get; set; }
        public Nullable<long> image_id { get; set; }
        public Nullable<int> comment_count { get; set; }
        public Nullable<long> from_user_id { get; set; }
        public Nullable<System.DateTime> insert_time { get; set; }
        public Nullable<int> like_count { get; set; }
        public Nullable<long> user_id { get; set; }
    }
}
