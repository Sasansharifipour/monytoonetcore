import { get_base_data_model } from "./get_base_data_model.interface";

export interface user_set_basket_item_price_view_model extends get_base_data_model {
  basket_item_id: number;
  current_price: number;
}
