﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class get_basket_share_package_input_view_model : get_base_data_model
    {
        public int basket_id { get; set; } = 0;
    }
}
