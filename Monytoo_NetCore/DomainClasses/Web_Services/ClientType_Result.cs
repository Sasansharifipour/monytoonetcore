﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace DomainClasses.Web_Services
{
    public class ClientType_Result
    {
        public long InsCode { get; set; } = 0;

        public int Buy_CountI { get; set; } = 0;

        public int Buy_CountN { get; set; } = 0;

        public decimal Buy_I_Volume { get; set; } = 0;

        public decimal Buy_N_Volume { get; set; } = 0;

        public int Sell_CountI { get; set; } = 0;

        public int Sell_CountN { get; set; } = 0;

        public decimal Sell_I_Volume { get; set; } = 0;

        public decimal Sell_N_Volume { get; set; } = 0;

        public decimal get_Buy_N_Percentage()
        {
            decimal buy_all = Buy_I_Volume + Buy_N_Volume;
            if (buy_all <= 0)
                return 0;

            return Buy_N_Volume / buy_all;
        }

        public decimal get_Buy_I_Percentage()
        {
            decimal buy_all = Buy_I_Volume + Buy_N_Volume;
            if (buy_all <= 0)
                return 0;

            return Buy_I_Volume / buy_all;
        }

        public decimal get_Sell_N_Percentage()
        {
            decimal sell_all = Sell_I_Volume + Sell_N_Volume;
            if (sell_all <= 0)
                return 0;

            return Sell_N_Volume / sell_all;
        }

        public decimal get_Sell_I_Percentage()
        {
            decimal sell_all = Sell_I_Volume + Sell_N_Volume;
            if (sell_all <= 0)
                return 0;

            return Sell_I_Volume / sell_all;
        }
    }
}