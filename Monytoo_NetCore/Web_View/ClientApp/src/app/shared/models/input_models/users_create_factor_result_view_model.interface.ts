import { result_model } from "./result_model.interface";
import { users_create_factor_Result } from "./users_create_factor_Result.interface";

export interface users_create_factor_result_view_model extends result_model {
  informations: users_create_factor_Result;
}

