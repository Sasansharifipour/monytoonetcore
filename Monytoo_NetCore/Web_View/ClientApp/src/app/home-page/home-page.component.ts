import { Component, Inject, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { UserService } from '../shared/services/users.services';
import { check_user_name_available_model } from '../shared/models/output_models/check_user_name_available_model.interface';
import { UserRegistration } from '../shared/models/inline_models/user.registration.interface';
import { register_model } from '../shared/models/output_models/register_model.interface';
import { register_result_model } from '../shared/models/input_models/register_result_model.interface';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router } from '@angular/router';
import { get_post_by_index_view_model } from '../shared/models/output_models/get_post_by_index_view_model.interface';
import { users_home_posts_result_view_model } from '../shared/models/input_models/users_home_posts_result_view_model.interface';
import { users_home_posts_Result } from '../shared/models/input_models/users_home_posts_Result.interface';
import { get_profile_data_model } from '../shared/models/output_models/get_profile_data_model.interface';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { get_post_image_view_model } from '../shared/models/output_models/get_post_image_view_model';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html'
})
export class HomePageComponent  {
  private result_model: register_result_model;
  private mobile_number: string = '';
  private pg_idx: number = -1;
  isLoading = false;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private router: Router, private dialogService: MatDialog, private sanitizer: DomSanitizer) {
    this.APIKEY = localStorage.getItem('API_KEY');

    if (this.APIKEY == null) {
      this.router.navigate(['']);
    }

    this.loadInfo();
  }
  private APIKEY: string = '';
  private data: users_home_posts_Result[] = [];

  @HostListener("window:scroll", ["$event"])
  getScrollHeight(): void {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight && !this.isLoading) {
      this.loadInfo();
    }
  }

  loadInfo() {
    this.isLoading = true;
    var model: get_post_by_index_view_model = { count: 10, index: this.pg_idx + 1, API_Key: this.APIKEY };
    
    this.userService.users_home_posts(model).subscribe((result: users_home_posts_result_view_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.pg_idx = this.pg_idx + 1;
        let t_data: users_home_posts_Result[];
        t_data = result.informations;
        
        t_data.forEach(item => {

          this.get_profile_image(item.user_id, item.profile_image_t).subscribe(result => {
            let objectURL = URL.createObjectURL(result);
              item.profile_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
          });

          this.get_post_image(item.post_id, item.image_id).subscribe(result => {
            let objectURL = URL.createObjectURL(result);
            item.post_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
          });
        });
        t_data.forEach(item => {
          this.data.push(item);
        });
      }

    });

    this.isLoading = false;
  }

  get_profile_image(user_profile_id, t): Observable<string> {

    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_profile_id, t: t };

    return this.userService.get_profile_image(model);
  }

  get_post_image(post_id, image_id): Observable<string> {

    var model: get_post_image_view_model = { API_Key: this.APIKEY, post_id: post_id, image_id: image_id };

    return this.userService.users_get_image(model);
  }

  register({ value }: { value: UserRegistration }) {
    
    let model: register_model = {
      mobile_number: value.mobile, firebase_id: null, invitation_code: null,
      lastname: null, name: null, username: null
    };

    this.mobile_number = value.mobile;

    this.userService.register(model).subscribe(result => {
      this.result_model = result;

      if (this.result_model.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        if (this.result_model.is_new_user) {
          this.router.navigate(['register']);
        }
        else {
          this.router.navigate(['login-approve', { mobile_number: this.mobile_number, user_session_ID: this.result_model.user_session_ID }]);
        }
      }

    });
  }
}

