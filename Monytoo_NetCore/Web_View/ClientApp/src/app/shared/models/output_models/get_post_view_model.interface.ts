import { get_base_data_model } from "./get_base_data_model.interface";

export interface get_post_view_model extends get_base_data_model {
  post_id: number;
  index: number;
  count: number;
  caption: string;
}
