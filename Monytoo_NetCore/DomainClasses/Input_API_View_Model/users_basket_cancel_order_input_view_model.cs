﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class users_basket_cancel_order_input_view_model : get_base_data_model
    {
        public int order_id { get; set; } = 0;
    }
}
