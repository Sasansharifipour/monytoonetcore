export interface users_get_basket_history_Result {
  id: number;
  balance: number;
  daily_profit: number;
  insert_time: Date;
  total_value: number;
  total_value_CHF: number;
  total_value_USD: number;
}
