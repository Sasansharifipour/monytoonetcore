﻿using DomainClasses.Instrument;
using DomainClasses.Web_Services;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public interface IDataProvider
    {
        List<Instrument_Trade_View_Model> get_Trade_From_Date_To_Date(long Instrument_Code,
            DateTime from_datetime, DateTime to_datetime);

        Instrument_Price_View_Model get_Instruments_Trade_One_Day(long Instrument_Code, DateTime datetime);

        Instrument_Price_View_Model get_Instruments_Price(long Instrument_Code);

        Task<List<Instrument_Result>> get_all_instruments();

        Task update_info_for_all_instrumentsAsync(long user_id, string secret_key,
             List<Engine_get_all_namad_Result> all_namads);
    }

    public class NullDataProvider : IDataProvider
    {
        public Task<List<Instrument_Result>> get_all_instruments()
        {
            return new Task<List<Instrument_Result>>(() => new List<Instrument_Result>());
        }

        public Instrument_Price_View_Model get_Instruments_Price(long Instrument_Code)
        {
            return new Instrument_Price_View_Model();
        }

        public Instrument_Price_View_Model get_Instruments_Trade_One_Day(long Instrument_Code, DateTime datetime)
        {
            return new Instrument_Price_View_Model();
        }

        public List<Instrument_Trade_View_Model> get_Trade_From_Date_To_Date(long Instrument_Code, DateTime from_datetime, DateTime to_datetime)
        {
            return new List<Instrument_Trade_View_Model>();
        }

        public Task update_info_for_all_instrumentsAsync(long user_id, string secret_key, List<Engine_get_all_namad_Result> all_namads)
        {
            throw new NotImplementedException();
        }
    }
}