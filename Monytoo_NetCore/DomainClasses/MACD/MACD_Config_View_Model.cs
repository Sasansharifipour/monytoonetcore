﻿
using CommonCode;
using System;

namespace DomainClasses.MACD
{
    public class MACD_Config_View_Model
    {
        public int get_day_counts { get { return (int)(Math.Ceiling((decimal)(calculate_day_counts / Config.week_open_days_count)) * Config.week_days_count) + Config.eve_days_count; } }

        public int calculate_day_counts { get; set; } = 0;
    }
}