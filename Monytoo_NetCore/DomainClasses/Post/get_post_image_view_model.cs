﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Post
{
    public class get_post_image_view_model : get_base_data_model
    {
        public long? post_id { get; set; } = 0;

        public long? image_id { get; set; } = 0;
    }
}