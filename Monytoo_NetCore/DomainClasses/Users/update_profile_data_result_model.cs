﻿
namespace DomainClasses.Users
{
    public class update_profile_data_result_model : result_model
    {
        public bool is_updated { get; set; } = false;

        public string field_key { get; set; } = "";
    }
}