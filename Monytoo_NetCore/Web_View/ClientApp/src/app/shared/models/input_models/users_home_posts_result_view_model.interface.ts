import { result_model } from "./result_model.interface";
import { users_home_posts_Result } from "./users_home_posts_Result.interface";

export interface users_home_posts_result_view_model extends result_model {
  informations: users_home_posts_Result[];
}

