﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Instrument
{
    public class BestLimitOneIns_Result_View_Model
    {
        public long InsCode { get; set; } = 0;

        public long QTitMeDem { get; set; } = 0;

        public decimal PMeDem { get; set; } = 0;

        public decimal PMeOf { get; set; } = 0;

        public long QTitMeOf { get; set; } = 0;
    }
}
