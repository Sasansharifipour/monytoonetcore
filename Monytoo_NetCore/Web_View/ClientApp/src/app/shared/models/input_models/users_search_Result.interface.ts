export interface users_search_Result {
  iD: number;
  type_id: number;
  username: string;
  name: string;
  lastname: string;
  profile_image_t: number;
}
