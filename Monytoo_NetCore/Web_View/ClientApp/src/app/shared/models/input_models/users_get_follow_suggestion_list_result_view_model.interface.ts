import { result_model } from "./result_model.interface";
import { users_get_follow_suggestion_list_Result } from "./users_get_follow_suggestion_list_Result";

export interface users_get_follow_suggestion_list_result_view_model extends result_model {
  informations: users_get_follow_suggestion_list_Result[];
}

