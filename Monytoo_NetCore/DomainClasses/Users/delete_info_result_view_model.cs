﻿
namespace DomainClasses.Users
{
    public class delete_info_result_view_model : result_model
    {
        public bool is_deleted { get; set; } = false;
    }
}