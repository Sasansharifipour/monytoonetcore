import { result_model } from "./result_model.interface";

export interface check_user_name_available_result_model extends result_model {
  is_available: number;
}
