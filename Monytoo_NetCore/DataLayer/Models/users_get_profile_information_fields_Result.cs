﻿
namespace DataLayer.Models
{
    public class users_get_profile_information_fields_Result
    {
        public string key_name { get; set; }
        public string name { get; set; }
        public string value { get; set; }
        public string parent_url { get; set; }
    }
}
