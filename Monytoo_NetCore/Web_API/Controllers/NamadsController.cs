﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DomainClasses.Instrument;
using DomainClasses.RSI;
using DomainClasses.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace Web_API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class NamadsController : ControllerBase
    {
        private INamadService _namadService;

        public NamadsController(INamadService namadService)
        {
            _namadService = namadService;
        }

        [Route("get_all")]
        [HttpPost]
        public List<Namad_View_Model> get_all_namads([FromBody] get_base_data_model model)
        {
            List<Namad_View_Model> result = _namadService.get_all_namads(model);
            return (result);
        }

        [Route("get_namad_price")]
        [HttpPost]
        public List<Trades_View_Model> get_namad_price([FromBody] Namad_Get_Price_View_Model model)
        {
            List<Trades_View_Model> result = _namadService.get_namad_price(model);
            return (result);
        }

        [Route("set_impact_on_Namad")]
        [HttpPost]
        public update_profile_data_result_model set_impact_on_Namad([FromBody] RSI_Set_Impact_View_Model model)
        {
            update_profile_data_result_model result = _namadService.set_impact_on_Namad(model);
            return (result);
        }

        [Route("update_namad_information_fields")]
        [HttpPost]
        public update_profile_data_result_model update_namad_information_fields([FromBody] Instrument_Update_Information_Field_API_View_Model model)
        {
            var result = _namadService.update_namad_information_fields(model);
            return (result);
        }
    }
}