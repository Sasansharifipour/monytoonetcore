﻿using Unity;

namespace Services
{
    public interface ICreator<T>
    {
        T FactoryMethod(int value);
    }

    public class DataProviderFactoryCreator : ICreator<IDataProvider>
    {
        private IDataProvider _bourseDataProvider;
        private IDataProvider _tgjuDataProvider;
        private IDataProvider _nullDataProvider;

        public DataProviderFactoryCreator([Dependency("Bourse")] IDataProvider bourseDataProvider,
            [Dependency("TGJU")] IDataProvider tgjuDataProvider,
            [Dependency("Null")] IDataProvider nullDataProvider)
        {
            _bourseDataProvider = bourseDataProvider;
            _tgjuDataProvider = tgjuDataProvider;
            _nullDataProvider = nullDataProvider;
        }

        public IDataProvider FactoryMethod(int value)
        {
            switch (value)
            {
                case 1: return _bourseDataProvider;
                case 2: return _tgjuDataProvider;
                default: return _nullDataProvider;
            }
        }

    }

    public class RSSProviderFactoryCreator : ICreator<IRSSProvider>
    {
        private IRSSProvider _nabzeBourseRSSReader;
        private IRSSProvider _eghtesadonlineRSSReader;
        private IRSSProvider _nullRSSReader;

        public RSSProviderFactoryCreator([Dependency("NabzeBourse")] IRSSProvider nabzeBourseRSSReader,
            [Dependency("EghtesadOnline")] IRSSProvider eghtesadonlineRSSReader,
            [Dependency("Null")] IRSSProvider nullRSSReader)
        {
            _nabzeBourseRSSReader = nabzeBourseRSSReader;
            _eghtesadonlineRSSReader = eghtesadonlineRSSReader;
            _nullRSSReader = nullRSSReader;
        }

        public IRSSProvider FactoryMethod(int value)
        {
            switch (value)
            {
                case 1: case 4: return _nabzeBourseRSSReader;
                case 2: case 3: case 5: return _eghtesadonlineRSSReader;
                default: return _nullRSSReader;
            }
        }

    }
}