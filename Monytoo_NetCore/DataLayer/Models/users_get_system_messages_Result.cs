﻿
namespace DataLayer.Models
{
    public class users_get_system_messages_Result
    {
        public string terms_of_condition { get; set; }
        public string privacy_policy { get; set; }
        public string first_popup { get; set; }
    }
}
