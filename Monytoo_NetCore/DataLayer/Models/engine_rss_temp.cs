﻿using System;

namespace DataLayer.Models
{
    public class engine_rss_temp
    {
        public long ID { get; set; }
        public string rss_url { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
        public Nullable<System.DateTime> Date { get; set; }
        public string Image_path { get; set; }
        public Nullable<long> post_id { get; set; }
    }
}
