import { result_model } from "./result_model.interface";

export interface register_result_model extends result_model {
  user_session_ID: number;

  is_new_user: boolean;
}
