import { result_model } from "./result_model.interface";

export interface privacy_result_model extends result_model {
  text: string;
}

