import { get_base_data_model } from "./get_base_data_model.interface";

export interface users_basket_cancel_order_input_view_model extends get_base_data_model {
  order_id: number;
}
