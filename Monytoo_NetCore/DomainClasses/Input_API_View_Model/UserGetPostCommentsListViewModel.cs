﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Input_API_View_Model
{
    public class UserGetPostCommentsListViewModel : PostBaseViewModel
    {
        public int count { get; set; } = 0;

        public int index { get; set; } = 0;
    }
}