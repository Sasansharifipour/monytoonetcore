﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Tablokhani
{
    public class Table_Data_View_Model
    {
        public string Name { get; set; } = "";

        public string Title { get; set; } = "";

        public List<table_cell_data> Data { get; set; } = new List<table_cell_data>();
    }
}