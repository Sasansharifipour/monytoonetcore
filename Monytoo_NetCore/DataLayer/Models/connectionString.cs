﻿
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace DataLayer.Models
{

    public partial class connectionString : DbContext
    {
        public connectionString(string connString)
            : base(connString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //throw new UnintentionalCodeFirstException();
        }

        public virtual DbSet<engine_rss_temp> engine_rss_temp { get; set; }
        public virtual DbSet<Engine_Board_Reader_Temp> Engine_Board_Reader_Temp { get; set; }
        public virtual DbSet<Engine_Codal_Temp> Engine_Codal_Temp { get; set; }
        public virtual DbSet<Engine_Board_Reader_Posting_Temp> Engine_Board_Reader_Posting_Temp { get; set; }

        public virtual int users_authentication(Nullable<long> user_id, string secret_key, SqlParameter res, SqlParameter type_id)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.ExecuteSqlCommand("users_authentication @user_id, @secret_key, @res out, @type_id out", user_idParameter, secret_keyParameter, res, type_id);
        }

        public virtual DbRawSqlQuery<Nullable<long>> users_check_username_availability(Nullable<long> user_id, string username, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var usernameParameter = username != null ?
                new SqlParameter("username", username) :
                new SqlParameter("username", DBNull.Value);

            return Database.SqlQuery<Nullable<long>>("users_check_username_availability @user_id, @username, @res out", user_idParameter, usernameParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_system_messages_Result> users_get_system_messages()
        {
            return Database.SqlQuery<users_get_system_messages_Result>("users_get_system_messages");
        }

        public virtual int users_register(string login_input, string verification_code, string username, 
            string name, string lastname, Nullable<long> inviter_id, string inviter_code, 
            string firebase_id, string invitation_code, int auth_type, SqlParameter is_new_user, 
            SqlParameter res)
        {
            var login_inputParameter = login_input != null ?
                new SqlParameter("login_input", login_input) :
                new SqlParameter("login_input", DBNull.Value);

            var verification_codeParameter = verification_code != null ?
                new SqlParameter("verification_code", verification_code) :
                new SqlParameter("verification_code", DBNull.Value);

            var usernameParameter = username != null ?
                new SqlParameter("username", username) :
                new SqlParameter("username", DBNull.Value);

            var nameParameter = name != null ?
                new SqlParameter("name", name) :
                new SqlParameter("name", DBNull.Value);

            var lastnameParameter = lastname != null ?
                new SqlParameter("lastname", lastname) :
                new SqlParameter("lastname", DBNull.Value);

            var inviter_idParameter = inviter_id.HasValue ?
                new SqlParameter("inviter_id", inviter_id) :
                new SqlParameter("inviter_id", DBNull.Value);

            var inviter_codeParameter = inviter_code != null ?
                new SqlParameter("inviter_code", inviter_code) :
                new SqlParameter("inviter_code", DBNull.Value);

            var firebase_idParameter = firebase_id != null ?
                new SqlParameter("firebase_id", firebase_id) :
                new SqlParameter("firebase_id", DBNull.Value);

            var invitation_codeParameter = invitation_code != null ?
                new SqlParameter("invitation_code", invitation_code) :
                new SqlParameter("invitation_code", DBNull.Value);

            var auth_typeParameter = auth_type != null ?
                new SqlParameter("auth_type", auth_type) :
                new SqlParameter("auth_type", DBNull.Value);

            return Database.ExecuteSqlCommand("users_register @login_input, @verification_code, " +
                "@username, @name, @lastname, @inviter_id, @inviter_code, @firebase_id, @invitation_code, @auth_type, " +
                "@is_new_user out, @res out", login_inputParameter, verification_codeParameter, usernameParameter, 
                nameParameter, lastnameParameter, inviter_idParameter, inviter_codeParameter, firebase_idParameter, 
                invitation_codeParameter, auth_typeParameter, is_new_user, res);
        }

        public virtual int users_verify_user_session(Nullable<long> user_session_id, string verification_code,
            string secret_key, SqlParameter user_id, SqlParameter res)
        {
            var user_session_idParameter = user_session_id.HasValue ?
                new SqlParameter("user_session_id", user_session_id) :
                new SqlParameter("user_session_id", DBNull.Value);

            var verification_codeParameter = verification_code != null ?
                new SqlParameter("verification_code", verification_code) :
                new SqlParameter("verification_code", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.ExecuteSqlCommand("users_verify_user_session @user_session_id, @verification_code, @secret_key, @user_id out, @res out", user_session_idParameter, verification_codeParameter, secret_keyParameter, user_id, res);
        }

        public virtual DbRawSqlQuery<users_get_profile_information_fields_Result> users_get_profile_information_fields(Nullable<long> user_id, string secret_key, Nullable<long> profile_user_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_user_idParameter = profile_user_id.HasValue ?
                new SqlParameter("profile_user_id", profile_user_id) :
                new SqlParameter("profile_user_id", DBNull.Value);

            return Database.SqlQuery<users_get_profile_information_fields_Result>("users_get_profile_information_fields @user_id, @secret_key, @profile_user_id, @res out", user_idParameter, secret_keyParameter, profile_user_idParameter, res);
        }

        public virtual int users_update_information_field_value(Nullable<long> user_id, string secret_key, string field_key_name, string field_value, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var field_key_nameParameter = field_key_name != null ?
                new SqlParameter("field_key_name", field_key_name) :
                new SqlParameter("field_key_name", DBNull.Value);

            var field_valueParameter = field_value != null ?
                new SqlParameter("field_value", field_value) :
                new SqlParameter("field_value", DBNull.Value);

            return Database.ExecuteSqlCommand("users_update_information_field_value @user_id, @secret_key, @field_key_name, @field_value, @res out", user_idParameter, secret_keyParameter, field_key_nameParameter, field_valueParameter, res);
        }

        public virtual int users_update_profile_data(Nullable<long> user_id, string secret_key, string name, string lastname, string username, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var nameParameter = name != null ?
                new SqlParameter("name", name) :
                new SqlParameter("name", DBNull.Value);

            var lastnameParameter = lastname != null ?
                new SqlParameter("lastname", lastname) :
                new SqlParameter("lastname", DBNull.Value);

            var usernameParameter = username != null ?
                new SqlParameter("username", username) :
                new SqlParameter("username", DBNull.Value);

            return Database.ExecuteSqlCommand("users_update_profile_data @user_id, @secret_key, @name, @lastname, @username, @res out", user_idParameter, secret_keyParameter, nameParameter, lastnameParameter, usernameParameter, res);
        }

        public virtual int users_update_profile_picture(Nullable<long> user_id, string secret_key, string profile_picture_address, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_picture_addressParameter = profile_picture_address != null ?
                new SqlParameter("profile_picture_address", profile_picture_address) :
                new SqlParameter("profile_picture_address", DBNull.Value);

            return Database.ExecuteSqlCommand("users_update_profile_picture @user_id, @secret_key, @profile_picture_address, @res out", user_idParameter, secret_keyParameter, profile_picture_addressParameter, res);
        }

        public virtual int users_check_expert(Nullable<long> expert_id, Nullable<long> user_id, SqlParameter res)
        {
            var expert_idParameter = expert_id.HasValue ?
                new SqlParameter("expert_id", expert_id) :
                new SqlParameter("expert_id", DBNull.Value);

            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_check_expert @expert_id, @user_id, @res out", expert_idParameter, user_idParameter, res);
        }

        public virtual int users_get_profile_image(Nullable<long> user_session_id, string secret_key, Nullable<long> profile_id, SqlParameter profile_image_address, SqlParameter res)
        {
            var user_session_idParameter = user_session_id.HasValue ?
                new SqlParameter("user_session_id", user_session_id) :
                new SqlParameter("user_session_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_idParameter = profile_id.HasValue ?
                new SqlParameter("profile_id", profile_id) :
                new SqlParameter("profile_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_get_profile_image @user_session_id, @secret_key, @profile_id, @profile_image_address out, @res out"
                , user_session_idParameter, secret_keyParameter, profile_idParameter, profile_image_address, res);
        }

        public virtual int users_send_post(Nullable<long> user_id, string secret_key, string image_address, string caption, Nullable<long> from_user_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var image_addressParameter = image_address != null ?
                new SqlParameter("image_address", image_address) :
                new SqlParameter("image_address", DBNull.Value);

            var captionParameter = caption != null ?
                new SqlParameter("caption", caption) :
                new SqlParameter("caption", DBNull.Value);

            var from_user_idParameter = from_user_id.HasValue ?
                new SqlParameter("from_user_id", from_user_id) :
                new SqlParameter("from_user_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_send_post @user_id, @secret_key, @image_address, @caption, @from_user_id, @res out", user_idParameter, secret_keyParameter, image_addressParameter, captionParameter, from_user_idParameter, res);
        }

        public virtual int users_follow(Nullable<long> user_id, string secret_key, Nullable<long> following_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var following_idParameter = following_id.HasValue ?
                new SqlParameter("following_id", following_id) :
                new SqlParameter("following_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_follow @user_id, @secret_key, @following_id, @res out",
                user_idParameter, secret_keyParameter, following_idParameter, res);
        }

        public virtual int users_unfollow(Nullable<long> user_id, string secret_key, Nullable<long> following_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var following_idParameter = following_id.HasValue ?
                new SqlParameter("following_id", following_id) :
                new SqlParameter("following_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_unfollow @user_id, @secret_key, @following_id, @res out", user_idParameter, secret_keyParameter, following_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_profile_data_Result> users_get_profile_data(Nullable<long> user_id, string secret_key, Nullable<long> profile_user_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", SqlDbType.BigInt);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", SqlDbType.NVarChar);

            var profile_user_idParameter = profile_user_id.HasValue ?
                new SqlParameter("profile_user_id", profile_user_id) :
                new SqlParameter("profile_user_id", DBNull.Value);

            return Database.SqlQuery<users_get_profile_data_Result>("users_get_profile_data @user_id, @secret_key, @profile_user_id, @res out", user_idParameter, secret_keyParameter, profile_user_idParameter, res);
        }

        public virtual int admin_register_Namad(string insCode, string lVal18, string lVal18AFC, string lVal30, string secret_key, Nullable<int> data_provider_id, SqlParameter res)
        {
            var insCodeParameter = insCode != null ?
                new SqlParameter("InsCode", insCode) :
                new SqlParameter("InsCode", DBNull.Value);

            var lVal18Parameter = lVal18 != null ?
                new SqlParameter("LVal18", lVal18) :
                new SqlParameter("LVal18", DBNull.Value);

            var lVal18AFCParameter = lVal18AFC != null ?
                new SqlParameter("LVal18AFC", lVal18AFC) :
                new SqlParameter("LVal18AFC", DBNull.Value);

            var lVal30Parameter = lVal30 != null ?
                new SqlParameter("LVal30", lVal30) :
                new SqlParameter("LVal30", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var data_provider_idParameter = data_provider_id.HasValue ?
                new SqlParameter("data_provider_id", data_provider_id) :
                new SqlParameter("data_provider_id", DBNull.Value);

            return Database.ExecuteSqlCommand("admin_register_Namad @insCode, @lVal18, @lVal18AFC, @lVal30, @secret_key, @data_provider_id, @res out", insCodeParameter, lVal18Parameter, lVal18AFCParameter, lVal30Parameter, secret_keyParameter, data_provider_idParameter, res);
        }

        public virtual DbRawSqlQuery<Engine_get_all_namad_Result> Engine_get_all_namad(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.SqlQuery<Engine_get_all_namad_Result>("Engine_get_all_namad @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual int Engine_set_impact_on_Namad(Nullable<long> user_id, string secret_key, Nullable<long> namad_id, Nullable<double> strength, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var namad_idParameter = namad_id.HasValue ?
                new SqlParameter("namad_id", namad_id) :
                new SqlParameter("namad_id", DBNull.Value);

            var strengthParameter = strength.HasValue ?
                new SqlParameter("strength", strength) :
                new SqlParameter("strength", typeof(double));

            return Database.ExecuteSqlCommand("Engine_set_impact_on_Namad @user_id, @secret_key, @namad_id, @strength, @res out", user_idParameter, secret_keyParameter, namad_idParameter, strengthParameter, res);
        }

        public virtual int Engine_update_namad_information_fields(Nullable<long> user_id, string secret_key, Nullable<long> namad_id, string field_key_name, string field_value, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var namad_idParameter = namad_id.HasValue ?
                new SqlParameter("namad_id", namad_id) :
                new SqlParameter("namad_id", DBNull.Value);

            var field_key_nameParameter = field_key_name != null ?
                new SqlParameter("field_key_name", field_key_name) :
                new SqlParameter("field_key_name", DBNull.Value);

            var field_valueParameter = field_value != null ?
                new SqlParameter("field_value", field_value) :
                new SqlParameter("field_value", DBNull.Value);

            return Database.ExecuteSqlCommand("Engine_update_namad_information_fields @user_id, @secret_key, @namad_id, @field_key_name, @field_value, @res out", user_idParameter, secret_keyParameter, namad_idParameter, field_key_nameParameter, field_valueParameter, res);
        }

        public virtual DbRawSqlQuery<users_Explore_posts_Result> users_Explore_posts(Nullable<long> user_id, string secret_key, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_Explore_posts_Result>("users_Explore_posts @user_id, @secret_key, @index, @count, @res out", user_idParameter, secret_keyParameter, indexParameter, countParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_profile_posts_Result> users_get_profile_posts(Nullable<long> user_id, string secret_key, Nullable<long> profile_user_id, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_user_idParameter = profile_user_id.HasValue ?
                new SqlParameter("profile_user_id", profile_user_id) :
                new SqlParameter("profile_user_id", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_profile_posts_Result>("users_get_profile_posts @user_id, @secret_key, @profile_user_id, @index, @count, @res out", user_idParameter, secret_keyParameter, profile_user_idParameter, indexParameter, countParameter, res);
        }

        public virtual DbRawSqlQuery<users_home_posts_Result> users_home_posts(Nullable<long> user_id, string secret_key, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_home_posts_Result>("users_home_posts @user_id, @secret_key, @index, @count, @res out", user_idParameter, secret_keyParameter, indexParameter, countParameter, res);
        }

        public virtual DbRawSqlQuery<users_search_Result> users_search(Nullable<long> user_id, string secret_key, string search_str, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var search_strParameter = search_str != null ?
                new SqlParameter("search_str", search_str) :
                new SqlParameter("search_str", DBNull.Value);

            return Database.SqlQuery<users_search_Result>("users_search @user_id, @secret_key, @search_str, @res out", user_idParameter, secret_keyParameter, search_strParameter, res);
        }

        public virtual int users_get_image(Nullable<long> user_id, string secret_key, Nullable<long> post_id, Nullable<long> image_id, SqlParameter image_address, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            var image_idParameter = image_id.HasValue ?
                new SqlParameter("image_id", image_id) :
                new SqlParameter("image_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_get_image @user_id, @secret_key, @post_id, @image_id, @image_address out, @res out", user_idParameter, secret_keyParameter, post_idParameter, image_idParameter, image_address, res);
        }

        public virtual DbRawSqlQuery<users_get_single_post_Result> users_get_single_post(Nullable<long> user_id, string secret_key, Nullable<long> post_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            return Database.SqlQuery<users_get_single_post_Result>("users_get_single_post @user_id, @secret_key, @post_id, @res out", user_idParameter, secret_keyParameter, post_idParameter, res);
        }

        public virtual int users_like_post(Nullable<long> user_id, string secret_key, Nullable<long> post_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_like_post @user_id, @secret_key, @post_id, @res out", user_idParameter, secret_keyParameter, post_idParameter, res);
        }

        public virtual int users_unlike_post(Nullable<long> user_id, string secret_key, Nullable<long> post_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_unlike_post @user_id, @secret_key, @post_id, @res out", user_idParameter, secret_keyParameter, post_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_post_likes_list_Result> users_get_post_likes_list(Nullable<long> user_id, string secret_key, Nullable<long> post_id, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_post_likes_list_Result>("users_get_post_likes_list @user_id, @secret_key, @post_id, @index, @count, @res out", user_idParameter, secret_keyParameter, post_idParameter, indexParameter, countParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_profile_followers_Result> users_get_profile_followers(Nullable<long> user_id, string secret_key, Nullable<long> profile_user_id, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_user_idParameter = profile_user_id.HasValue ?
                new SqlParameter("profile_user_id", profile_user_id) :
                new SqlParameter("profile_user_id", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_profile_followers_Result>("users_get_profile_followers @user_id, @secret_key, @profile_user_id, @index, @count, @res out", user_idParameter, secret_keyParameter, profile_user_idParameter, indexParameter, countParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_profile_followings_Result> users_get_profile_followings(Nullable<long> user_id, string secret_key, Nullable<long> profile_user_id, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_user_idParameter = profile_user_id.HasValue ?
                new SqlParameter("profile_user_id", profile_user_id) :
                new SqlParameter("profile_user_id", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_profile_followings_Result>("users_get_profile_followings @user_id, @secret_key, @profile_user_id, @index, @count, @res out", user_idParameter, secret_keyParameter, profile_user_idParameter, indexParameter, countParameter, res);
        }

        public virtual int users_delete_post(Nullable<long> user_id, string secret_key, Nullable<long> post_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_delete_post @user_id, @secret_key, @post_id, @res out", user_idParameter, secret_keyParameter, post_idParameter, res);
        }

        public virtual int users_edit_post(Nullable<long> user_id, string secret_key, Nullable<long> post_id, string caption, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            var captionParameter = caption != null ?
                new SqlParameter("caption", caption) :
                new SqlParameter("caption", DBNull.Value);

            return Database.ExecuteSqlCommand("users_edit_post @user_id, @secret_key, @post_id, @caption, @res out", user_idParameter, secret_keyParameter, post_idParameter, captionParameter, res);
        }

        public virtual DbRawSqlQuery<Engine_get_namad_info_Result> Engine_get_namad_info(Nullable<long> user_id, string secret_key, Nullable<long> namad_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var namad_idParameter = namad_id.HasValue ?
                new SqlParameter("namad_id", namad_id) :
                new SqlParameter("namad_id", DBNull.Value);

            return Database.SqlQuery<Engine_get_namad_info_Result>("Engine_get_namad_info @user_id, @secret_key, @namad_id, @res out", user_idParameter, secret_keyParameter, namad_idParameter, res);
        }

        public virtual int users_create_factor(Nullable<long> user_id, string secret_key, Nullable<int> package_id, Nullable<long> coupon_id, SqlParameter res, SqlParameter price)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var package_idParameter = package_id.HasValue ?
                new SqlParameter("package_id", package_id) :
                new SqlParameter("package_id", DBNull.Value);

            var coupon_idParameter = coupon_id.HasValue ?
                new SqlParameter("coupon_id", coupon_id) :
                new SqlParameter("coupon_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_create_factor @user_id, @secret_key, @package_id, @coupon_id, @res out, @price out", user_idParameter, secret_keyParameter, package_idParameter, coupon_idParameter, res, price);
        }

        public virtual DbRawSqlQuery<users_get_price_packages_Result> users_get_price_packages(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.SqlQuery<users_get_price_packages_Result>("users_get_price_packages @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual int admin_Confirm_payment_factor(Nullable<int> user_id, Nullable<long> factor_id, string state, string total, string wage, string gateway, string terminal, string pay_ref, string pay_trace, string pay_pan, string pay_cid, string pay_time, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var factor_idParameter = factor_id.HasValue ?
                new SqlParameter("factor_id", factor_id) :
                new SqlParameter("factor_id", DBNull.Value);

            var stateParameter = state != null ?
                new SqlParameter("state", state) :
                new SqlParameter("state", DBNull.Value);

            var totalParameter = total != null ?
                new SqlParameter("total", total) :
                new SqlParameter("total", DBNull.Value);

            var wageParameter = wage != null ?
                new SqlParameter("wage", wage) :
                new SqlParameter("wage", DBNull.Value);

            var gatewayParameter = gateway != null ?
                new SqlParameter("gateway", gateway) :
                new SqlParameter("gateway", DBNull.Value);

            var terminalParameter = terminal != null ?
                new SqlParameter("terminal", terminal) :
                new SqlParameter("terminal", DBNull.Value);

            var pay_refParameter = pay_ref != null ?
                new SqlParameter("pay_ref", pay_ref) :
                new SqlParameter("pay_ref", DBNull.Value);

            var pay_traceParameter = pay_trace != null ?
                new SqlParameter("pay_trace", pay_trace) :
                new SqlParameter("pay_trace", DBNull.Value);

            var pay_panParameter = pay_pan != null ?
                new SqlParameter("pay_pan", pay_pan) :
                new SqlParameter("pay_pan", DBNull.Value);

            var pay_cidParameter = pay_cid != null ?
                new SqlParameter("pay_cid", pay_cid) :
                new SqlParameter("pay_cid", DBNull.Value);

            var pay_timeParameter = pay_time != null ?
                new SqlParameter("pay_time", pay_time) :
                new SqlParameter("pay_time", DBNull.Value);

            return Database.ExecuteSqlCommand("admin_Confirm_payment_factor @user_id, @factor_id, @state, @total, @wage, @gateway, @terminal, @pay_ref, @pay_trace, @pay_pan, @pay_cid, @pay_time, @res out", user_idParameter, factor_idParameter, stateParameter, totalParameter, wageParameter, gatewayParameter, terminalParameter, pay_refParameter, pay_traceParameter, pay_panParameter, pay_cidParameter, pay_timeParameter, res);
        }

        public virtual DbRawSqlQuery<admin_get_factor_info_Result> admin_get_factor_info(Nullable<long> factor_id)
        {
            var factor_idParameter = factor_id.HasValue ?
                new SqlParameter("factor_id", factor_id) :
                new SqlParameter("factor_id", DBNull.Value);

            return Database.SqlQuery<admin_get_factor_info_Result>("admin_get_factor_info @factor_id", factor_idParameter);
        }

        public virtual DbRawSqlQuery<users_get_notification_list_Result> users_get_notification_list(Nullable<long> user_id, string secret_key, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_notification_list_Result>("users_get_notification_list @user_id, @secret_key, @index, @count, @res out", user_idParameter, secret_keyParameter, indexParameter, countParameter, res);
        }

        public virtual int users_seen_notification_list(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.ExecuteSqlCommand("users_seen_notification_list @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual int users_delete_comment(Nullable<long> user_id, string secret_key, Nullable<long> comment_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var comment_idParameter = comment_id.HasValue ?
                new SqlParameter("comment_id", comment_id) :
                new SqlParameter("comment_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_delete_comment @user_id, @secret_key, @comment_id, @res out", user_idParameter, secret_keyParameter, comment_idParameter, res);
        }

        public virtual int users_send_comment(Nullable<long> user_id, string secret_key, Nullable<long> post_id, string text, Nullable<long> reply_to_comment_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            var textParameter = text != null ?
                new SqlParameter("text", text) :
                new SqlParameter("text", DBNull.Value);

            var reply_to_comment_idParameter = reply_to_comment_id.HasValue ?
                new SqlParameter("reply_to_comment_id", reply_to_comment_id) :
                new SqlParameter("reply_to_comment_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_send_comment @user_id, @secret_key, @post_id, @text, @reply_to_comment_id, @res out", user_idParameter, secret_keyParameter, post_idParameter, textParameter, reply_to_comment_idParameter, res);
        }

        public virtual int users_report_post(Nullable<long> user_id, string secret_key, Nullable<long> post_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_report_post @user_id, @secret_key, @post_id, @res out", user_idParameter, secret_keyParameter, post_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_comment_reply_list_Result> users_get_comment_reply_list(Nullable<long> user_id, string secret_key, Nullable<long> comment_id, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var comment_idParameter = comment_id.HasValue ?
                new SqlParameter("comment_id", comment_id) :
                new SqlParameter("comment_id", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_comment_reply_list_Result>("users_get_comment_reply_list @user_id, @secret_key, @comment_id, @index, @count, @res out", user_idParameter, secret_keyParameter, comment_idParameter, indexParameter, countParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_post_comments_list_Result> users_get_post_comments_list(Nullable<long> user_id, string secret_key, Nullable<long> post_id, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var post_idParameter = post_id.HasValue ?
                new SqlParameter("post_id", post_id) :
                new SqlParameter("post_id", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_post_comments_list_Result>("users_get_post_comments_list @user_id, @secret_key, @post_id, @index, @count, @res out", user_idParameter, secret_keyParameter, post_idParameter, indexParameter, countParameter, res);
        }

        public virtual int Engine_update_namad_information_fields_list(Nullable<long> user_id, string secret_key, System.Data.DataTable input)
        {
            var user_idParameter = new SqlParameter("user_id", SqlDbType.Int);
            if (user_id.HasValue)
                user_idParameter.Value = user_id.Value;

            var secret_keyParameter = new SqlParameter("secret_key", SqlDbType.NVarChar);
            secret_keyParameter.Value = secret_key;

            var data = new SqlParameter("input_table", SqlDbType.Structured);
            data.Value = input;
            data.TypeName = "dbo.tmp_information_field_table";

            return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreCommand("EXEC Engine_update_namad_information_fields_list @user_id, @secret_key, @input_table", user_idParameter, secret_keyParameter, data);
        }

        public virtual int users_delete_profile_picture(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.ExecuteSqlCommand("users_delete_profile_picture @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual DbRawSqlQuery<users_check_version_Result> users_check_version(Nullable<int> version_id)
        {
            var version_idParameter = version_id.HasValue ?
                new SqlParameter("version_id", version_id) :
                new SqlParameter("version_id", DBNull.Value);

            return Database.SqlQuery<users_check_version_Result>("users_check_version @version_id", version_idParameter);
        }

        public virtual DbRawSqlQuery<users_get_follow_suggestion_list_Result> users_get_follow_suggestion_list(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.SqlQuery<users_get_follow_suggestion_list_Result>("users_get_follow_suggestion_list @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_last_3_posts_Result> users_get_last_3_posts(Nullable<long> user_id, string secret_key, Nullable<long> profile_user_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_user_idParameter = profile_user_id.HasValue ?
                new SqlParameter("profile_user_id", profile_user_id) :
                new SqlParameter("profile_user_id", DBNull.Value);

            return Database.SqlQuery<users_get_last_3_posts_Result>("users_get_last_3_posts @user_id, @secret_key, @profile_user_id, @res out", user_idParameter, secret_keyParameter, profile_user_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_shared_accounts_Result> users_get_shared_accounts(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.SqlQuery<users_get_shared_accounts_Result>("users_get_shared_accounts @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual DbRawSqlQuery<admin_get_notification_list_Result> admin_get_notification_list()
        {
            return Database.SqlQuery<admin_get_notification_list_Result>("admin_get_notification_list");
        }

        public virtual DbRawSqlQuery<users_get_comment_likes_list_Result> users_get_comment_likes_list(Nullable<long> user_id, string secret_key, Nullable<long> comment_id, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var comment_idParameter = comment_id.HasValue ?
                new SqlParameter("comment_id", comment_id) :
                new SqlParameter("comment_id", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_comment_likes_list_Result>("users_get_comment_likes_list @user_id, @secret_key, @comment_id, @index, @count, @res out", user_idParameter, secret_keyParameter, comment_idParameter, indexParameter, countParameter, res);
        }

        public virtual int users_like_comment(Nullable<long> user_id, string secret_key, Nullable<long> comment_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var comment_idParameter = comment_id.HasValue ?
                new SqlParameter("comment_id", comment_id) :
                new SqlParameter("comment_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_like_comment @user_id, @secret_key, @comment_id, @res out", user_idParameter, secret_keyParameter, comment_idParameter, res);
        }

        public virtual int users_unlike_comment(Nullable<long> user_id, string secret_key, Nullable<long> comment_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var comment_idParameter = comment_id.HasValue ?
                new SqlParameter("comment_id", comment_id) :
                new SqlParameter("comment_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_unlike_comment @user_id, @secret_key, @comment_id, @res out", user_idParameter, secret_keyParameter, comment_idParameter, res);
        }

        public virtual int users_basket_cancel_order(Nullable<long> user_id, string secret_key, Nullable<long> order_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var order_idParameter = order_id.HasValue ?
                new SqlParameter("order_id", order_id) :
                new SqlParameter("order_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_basket_cancel_order @user_id, @secret_key, @order_id, @res out", user_idParameter, secret_keyParameter, order_idParameter, res);
        }

        public virtual int users_create_basket(Nullable<long> user_id, string secret_key, string name, Nullable<int> basket_balance, Nullable<int> type_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var nameParameter = name != null ?
                new SqlParameter("name", name) :
                new SqlParameter("name", DBNull.Value);

            var basket_balanceParameter = basket_balance.HasValue ?
                new SqlParameter("basket_balance", basket_balance) :
                new SqlParameter("basket_balance", DBNull.Value);

            var type_idParameter = type_id.HasValue ?
                new SqlParameter("type_id", type_id) :
                new SqlParameter("type_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_create_basket @user_id, @secret_key, @name, @basket_balance, @type_id, @res out", user_idParameter, secret_keyParameter, nameParameter, basket_balanceParameter, type_idParameter, res);
        }

        public virtual int users_create_basket_order(Nullable<long> user_id, string secret_key, Nullable<long> namad_id, Nullable<int> order_price, Nullable<int> count, Nullable<long> basket_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var namad_idParameter = namad_id.HasValue ?
                new SqlParameter("namad_id", namad_id) :
                new SqlParameter("namad_id", DBNull.Value);

            var order_priceParameter = order_price.HasValue ?
                new SqlParameter("order_price", order_price) :
                new SqlParameter("order_price", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_create_basket_order @user_id, @secret_key, @namad_id, @order_price, @count, @basket_id, @res out", user_idParameter, secret_keyParameter, namad_idParameter, order_priceParameter, countParameter, basket_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_basket_history_Result> users_get_basket_history(Nullable<long> user_id, string secret_key, Nullable<long> basket_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            return Database.SqlQuery<users_get_basket_history_Result>("users_get_basket_history @user_id, @secret_key, @basket_id, @res out", user_idParameter, secret_keyParameter, basket_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_basket_info_Result> users_get_basket_info(Nullable<long> user_id, string secret_key, Nullable<long> basket_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            return Database.SqlQuery<users_get_basket_info_Result>("users_get_basket_info @user_id, @secret_key, @basket_id, @res out", user_idParameter, secret_keyParameter, basket_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_basket_items_Result> users_get_basket_items(Nullable<long> user_id, string secret_key, Nullable<long> basket_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            return Database.SqlQuery<users_get_basket_items_Result>("users_get_basket_items @user_id, @secret_key, @basket_id, @res out", user_idParameter, secret_keyParameter, basket_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_basket_order_history_Result> users_get_basket_order_history(Nullable<long> user_id, string secret_key, Nullable<long> basket_id, Nullable<int> index, Nullable<int> count, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            var indexParameter = index.HasValue ?
                new SqlParameter("index", index) :
                new SqlParameter("index", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            return Database.SqlQuery<users_get_basket_order_history_Result>("users_get_basket_order_history @user_id, @secret_key, @basket_id, @index, @count, @res out", user_idParameter, secret_keyParameter, basket_idParameter, indexParameter, countParameter, res);
        }

        public virtual DbRawSqlQuery<users_search_stock_Result> users_search_stock(Nullable<long> user_id, string secret_key, string search_str, Nullable<int> type_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var search_strParameter = search_str != null ?
                new SqlParameter("search_str", search_str) :
                new SqlParameter("search_str", DBNull.Value);

            var type_idParameter = type_id.HasValue ?
                new SqlParameter("type_id", type_id) :
                new SqlParameter("type_id", DBNull.Value);

            return Database.SqlQuery<users_search_stock_Result>("users_search_stock @user_id, @secret_key, @search_str, @type_id, @res out", user_idParameter, secret_keyParameter, search_strParameter, type_idParameter, res);
        }

        public virtual int users_create_factor_wallet(Nullable<long> user_id, string secret_key, Nullable<int> price, Nullable<int> payment_gateway_id, string i_data, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var priceParameter = price.HasValue ?
                new SqlParameter("price", price) :
                new SqlParameter("price", DBNull.Value);

            var payment_gateway_idParameter = payment_gateway_id.HasValue ?
                new SqlParameter("payment_gateway_id", payment_gateway_id) :
                new SqlParameter("payment_gateway_id", DBNull.Value);

            var i_dataParameter = i_data != null ?
                new SqlParameter("i_data", i_data) :
                new SqlParameter("i_data", DBNull.Value);

            return Database.ExecuteSqlCommand("users_create_factor_wallet @user_id, @secret_key, @price, @payment_gateway_id, @i_data, @res out", user_idParameter, secret_keyParameter, priceParameter, payment_gateway_idParameter, i_dataParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_profile_baskets_Result> users_get_profile_baskets(Nullable<long> user_id, string secret_key, Nullable<long> profile_user_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_user_idParameter = profile_user_id.HasValue ?
                new SqlParameter("profile_user_id", profile_user_id) :
                new SqlParameter("profile_user_id", DBNull.Value);

            return Database.SqlQuery<users_get_profile_baskets_Result>("users_get_profile_baskets @user_id, @secret_key, @profile_user_id, @res out", user_idParameter, secret_keyParameter, profile_user_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_share_baskets_Result> users_get_share_baskets(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.SqlQuery<users_get_share_baskets_Result>("users_get_share_baskets @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual int Engine_evaluate_basket(Nullable<long> user_id, string secret_key, Nullable<long> basket_id, Nullable<int> total_value, Nullable<double> profit, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            var total_valueParameter = total_value.HasValue ?
                new SqlParameter("total_value", total_value) :
                new SqlParameter("total_value", DBNull.Value);

            var profitParameter = profit.HasValue ?
                new SqlParameter("profit", profit) :
                new SqlParameter("profit", typeof(double));

            return Database.ExecuteSqlCommand("Engine_evaluate_basket @user_id, @secret_key, @basket_id, @total_value, @profit, @res out", user_idParameter, secret_keyParameter, basket_idParameter, total_valueParameter, profitParameter, res);
        }

        public virtual DbRawSqlQuery<Engine_get_all_basket_orders_Result> Engine_get_all_basket_orders(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.SqlQuery<Engine_get_all_basket_orders_Result>("Engine_get_all_basket_orders @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual DbRawSqlQuery<Engine_get_basket_items_Result> Engine_get_basket_items(Nullable<long> user_id, string secret_key, Nullable<long> basket_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            return Database.SqlQuery<Engine_get_basket_items_Result>("Engine_get_basket_items @user_id, @secret_key, @basket_id, @res out", user_idParameter, secret_keyParameter, basket_idParameter, res);
        }

        public virtual DbRawSqlQuery<Engine_pick_basket_for_evaluation_Result> Engine_pick_basket_for_evaluation(Nullable<long> user_id, string secret_key, SqlParameter res, SqlParameter basket_id, SqlParameter basket_balance, SqlParameter basket_initial_balance)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.SqlQuery<Engine_pick_basket_for_evaluation_Result>("Engine_pick_basket_for_evaluation @user_id, @secret_key, @res out, @basket_id out, @basket_balance out, @basket_initial_balance out", user_idParameter, secret_keyParameter, res, basket_id, basket_balance, basket_initial_balance);
        }

        public virtual int Engine_update_basket_orders_status(Nullable<long> user_id, string secret_key, Nullable<long> order_id, Nullable<bool> confirm, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var order_idParameter = order_id.HasValue ?
                new SqlParameter("order_id", order_id) :
                new SqlParameter("order_id", DBNull.Value);

            var confirmParameter = confirm.HasValue ?
                new SqlParameter("confirm", confirm) :
                new SqlParameter("confirm", typeof(bool));

            return Database.ExecuteSqlCommand("Engine_update_basket_orders_status @user_id, @secret_key, @order_id, @confirm, @res out", user_idParameter, secret_keyParameter, order_idParameter, confirmParameter, res);
        }

        public virtual int users_add_item_to_private_basket(Nullable<long> user_id, string secret_key, Nullable<long> namad_id, Nullable<int> order_price, Nullable<int> count, Nullable<long> basket_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var namad_idParameter = namad_id.HasValue ?
                new SqlParameter("namad_id", namad_id) :
                new SqlParameter("namad_id", DBNull.Value);

            var order_priceParameter = order_price.HasValue ?
                new SqlParameter("order_price", order_price) :
                new SqlParameter("order_price", DBNull.Value);

            var countParameter = count.HasValue ?
                new SqlParameter("count", count) :
                new SqlParameter("count", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_add_item_to_private_basket @user_id, @secret_key, @namad_id, @order_price, @count, @basket_id, @res out", user_idParameter, secret_keyParameter, namad_idParameter, order_priceParameter, countParameter, basket_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_Private_baskets_Result> users_get_Private_baskets(Nullable<long> user_id, string secret_key, Nullable<long> profile_user_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var profile_user_idParameter = profile_user_id.HasValue ?
                new SqlParameter("profile_user_id", profile_user_id) :
                new SqlParameter("profile_user_id", DBNull.Value);

            return Database.SqlQuery<users_get_Private_baskets_Result>("users_get_Private_baskets @user_id, @secret_key, @profile_user_id, @res out", user_idParameter, secret_keyParameter, profile_user_idParameter, res);
        }

        public virtual int users_buy_basket_share_packages(Nullable<long> user_id, string secret_key, Nullable<long> basket_share_package_basket_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_share_package_basket_idParameter = basket_share_package_basket_id.HasValue ?
                new SqlParameter("basket_share_package_basket_id", basket_share_package_basket_id) :
                new SqlParameter("basket_share_package_basket_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_buy_basket_share_packages @user_id, @secret_key, @basket_share_package_basket_id, @res out", user_idParameter, secret_keyParameter, basket_share_package_basket_idParameter, res);
        }

        public virtual int users_delete_basket_share_package(Nullable<long> user_id, string secret_key, Nullable<long> basket_share_package_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_share_package_idParameter = basket_share_package_id.HasValue ?
                new SqlParameter("basket_share_package_id", basket_share_package_id) :
                new SqlParameter("basket_share_package_id", DBNull.Value);

            return Database.ExecuteSqlCommand("users_delete_basket_share_package @user_id, @secret_key, @basket_share_package_id, @res out", user_idParameter, secret_keyParameter, basket_share_package_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_basket_share_packages_Result> users_get_basket_share_packages(Nullable<long> user_id, string secret_key, Nullable<long> basket_id, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            return Database.SqlQuery<users_get_basket_share_packages_Result>("users_get_basket_share_packages @user_id, @secret_key, @basket_id, @res out", user_idParameter, secret_keyParameter, basket_idParameter, res);
        }

        public virtual DbRawSqlQuery<users_get_get_my_balance_Result> users_get_get_my_balance(Nullable<long> user_id, string secret_key, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            return Database.SqlQuery<users_get_get_my_balance_Result>("users_get_get_my_balance @user_id, @secret_key, @res out", user_idParameter, secret_keyParameter, res);
        }

        public virtual int users_share_my_basket(Nullable<long> user_id, string secret_key, Nullable<long> basket_id, string name, Nullable<int> duration, Nullable<int> price, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_idParameter = basket_id.HasValue ?
                new SqlParameter("basket_id", basket_id) :
                new SqlParameter("basket_id", DBNull.Value);

            var nameParameter = name != null ?
                new SqlParameter("name", name) :
                new SqlParameter("name", DBNull.Value);

            var durationParameter = duration.HasValue ?
                new SqlParameter("duration", duration) :
                new SqlParameter("duration", DBNull.Value);

            var priceParameter = price.HasValue ?
                new SqlParameter("price", price) :
                new SqlParameter("price", DBNull.Value);

            return Database.ExecuteSqlCommand("users_share_my_basket @user_id, @secret_key, @basket_id, @name, @duration, @price, @res out", user_idParameter, secret_keyParameter, basket_idParameter, nameParameter, durationParameter, priceParameter, res);
        }

        public virtual int users_set_basket_item_price(Nullable<long> user_id, string secret_key, Nullable<long> basket_item_id, Nullable<double> current_price, SqlParameter res)
        {
            var user_idParameter = user_id.HasValue ?
                new SqlParameter("user_id", user_id) :
                new SqlParameter("user_id", DBNull.Value);

            var secret_keyParameter = secret_key != null ?
                new SqlParameter("secret_key", secret_key) :
                new SqlParameter("secret_key", DBNull.Value);

            var basket_item_idParameter = basket_item_id.HasValue ?
                new SqlParameter("basket_item_id", basket_item_id) :
                new SqlParameter("basket_item_id", DBNull.Value);

            var current_priceParameter = current_price.HasValue ?
                new SqlParameter("current_price", current_price) :
                new SqlParameter("current_price", typeof(double));

            return Database.ExecuteSqlCommand("users_set_basket_item_price @user_id, @secret_key, @basket_item_id, @current_price, @res out", user_idParameter, secret_keyParameter, basket_item_idParameter, current_priceParameter, res);
        }

        public virtual int admin_confirm_factor_wallet(Nullable<int> factor_id, Nullable<int> payment_gateway_id, Nullable<int> price, string ref1, string ref2, string ref3, string ref4, SqlParameter res)
        {
            var factor_idParameter = factor_id.HasValue ?
                new SqlParameter("factor_id", factor_id) :
                new SqlParameter("factor_id", DBNull.Value);

            var payment_gateway_idParameter = payment_gateway_id.HasValue ?
                new SqlParameter("payment_gateway_id", payment_gateway_id) :
                new SqlParameter("payment_gateway_id", DBNull.Value);

            var priceParameter = price.HasValue ?
                new SqlParameter("price", price) :
                new SqlParameter("price", DBNull.Value);

            var ref1Parameter = ref1 != null ?
                new SqlParameter("ref1", ref1) :
                new SqlParameter("ref1", DBNull.Value);

            var ref2Parameter = ref2 != null ?
                new SqlParameter("ref2", ref2) :
                new SqlParameter("ref2", DBNull.Value);

            var ref3Parameter = ref3 != null ?
                new SqlParameter("ref3", ref3) :
                new SqlParameter("ref3", DBNull.Value);

            var ref4Parameter = ref4 != null ?
                new SqlParameter("ref4", ref4) :
                new SqlParameter("ref4", DBNull.Value);

            return Database.ExecuteSqlCommand("admin_confirm_factor_wallet @factor_id, @payment_gateway_id, @price, @ref1, @ref2, @ref3, @ref4, @res out", factor_idParameter, payment_gateway_idParameter, priceParameter, ref1Parameter, ref2Parameter, ref3Parameter, ref4Parameter, res);
        }

        public virtual int admin_get_factor_wallet_data(Nullable<int> factor_id, Nullable<int> payment_gateway_id, SqlParameter price, SqlParameter i_data)
        {
            var factor_idParameter = factor_id.HasValue ?
                new SqlParameter("factor_id", factor_id) :
                new SqlParameter("factor_id", DBNull.Value);

            var payment_gateway_idParameter = payment_gateway_id.HasValue ?
                new SqlParameter("payment_gateway_id", payment_gateway_id) :
                new SqlParameter("payment_gateway_id", DBNull.Value);

            return Database.ExecuteSqlCommand("admin_get_factor_wallet_data @factor_id, @payment_gateway_id, @price out, @i_data out", factor_idParameter, payment_gateway_idParameter, price, i_data);
        }
    }
}
