﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Post
{
    public class get_post_view_model : get_base_data_model
    {
        public long? post_id { get; set; } = 0;

        public int? index { get; set; } = 0;

        public int? count { get; set; } = 0;

        public string caption { get; set; } = "";
    }
}