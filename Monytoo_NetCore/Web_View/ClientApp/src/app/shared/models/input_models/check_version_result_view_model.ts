import { result_model } from "./result_model.interface";

export interface check_version_result_view_model extends result_model {
  status_id: number;
  message: string;
  update_url: string;
}

