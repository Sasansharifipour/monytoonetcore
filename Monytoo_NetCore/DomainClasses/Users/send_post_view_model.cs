﻿
using Microsoft.AspNetCore.Http;

namespace DomainClasses.Users
{
    public class send_post_view_model
    {
        //public HttpPostedFile image { get; set; }
        public IFormFile image { get; set; }

        public string API_Key { get; set; }

        public string caption { get; set; }

        public long? from_user_id { get; set; }
    }
}