﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Users
{
    public class update_information_field_value_model
    {
        public string API_Key { get; set; }

        public List<information_field_value> information_fields { get; set; }
    }

    public class information_field_value
    {
        public string field_key { get; set; }

        public string field_value { get; set; }
    }
}