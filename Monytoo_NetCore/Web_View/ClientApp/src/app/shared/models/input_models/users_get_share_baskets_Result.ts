export interface users_get_share_baskets_Result {
  iD: number;
  name: string;
  total_value: number;
  create_time: Date;
  last_evaluation_time: Date;
  is_shareable: boolean;
  item_count: number;
  expiration_date: Date;
}
