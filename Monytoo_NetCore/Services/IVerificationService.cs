﻿using IPE.SmsIrRestful.TPL.NetCore;
using System.Collections.Generic;

namespace Services
{

    public interface IVerificationService
    {
        bool send_verification_code(string phone_number, string verification_code);
    }

    public class VerificationService : IVerificationService
    {
        static string userApikey = "db9ad4d816740b44cb8ee115";
        static string secretKey = "124dea583931d301220e282";
        public bool send_verification_code(string phone_number, string verification_code)
        {
            bool result = false;
            try
            {
                long mobile_number = long.Parse(phone_number.Replace("+98", "0"));

                var token = new Token().GetToken(userApikey, secretKey);
                var ultraFastSend = new UltraFastSend()
                {
                    Mobile = mobile_number,
                    TemplateId = 55061,
                    ParameterArray = new List<UltraFastParameters>()
                        {
                            new UltraFastParameters()
                            {
                                Parameter = "VerificationCode" , ParameterValue = verification_code
                            }
                        }.ToArray()

                };

                UltraFastSendRespone ultraFastSendRespone = new UltraFast().Send(token, ultraFastSend);

                if (ultraFastSendRespone.IsSuccessful)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch
            {
                result = false;
            }
            return result;
        }
    }
}