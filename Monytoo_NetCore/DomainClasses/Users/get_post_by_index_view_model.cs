﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Users
{
    public class get_post_by_index_view_model : get_base_data_model
    {
        public int? index { get; set; }

        public int? count { get; set; }
    }
}