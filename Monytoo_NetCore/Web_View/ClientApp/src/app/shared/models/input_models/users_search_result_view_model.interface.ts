import { result_model } from "./result_model.interface";
import { users_search_Result } from "./users_search_Result.interface";

export interface users_search_result_view_model extends result_model {
  information: users_search_Result[];
}

