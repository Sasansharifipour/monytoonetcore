import { result_model } from "./result_model.interface";
import { users_get_basket_order_history_Result } from "./users_get_basket_order_history_Result";

export interface users_get_basket_order_history_result_view_model extends result_model {
  informations: users_get_basket_order_history_Result[];
}

