﻿using DomainClasses.Instrument;
using System.Collections.Generic;

namespace Services
{
    public static class ExtensionMethods
    {
        public static List<Instrument_Update_Information_Field_View_Model>
            Convert_To_Update_Model(this Instrument_Price_View_Model item)
        {
            if (item == null || item.InsCode == 0)
                return new List<Instrument_Update_Information_Field_View_Model>();

            List<Instrument_Update_Information_Field_View_Model> result = new
                List<Instrument_Update_Information_Field_View_Model>();

            foreach (var prop in item.GetType().GetProperties())
            {
                string prop_name = prop.Name;
                string prop_value = prop.GetValue(item).ToString().Trim();

                if (prop_name.StartsWith("ENGINE") && !string.IsNullOrWhiteSpace(prop_value) && !string.IsNullOrEmpty(prop_value))
                {
                    result.Add(new Instrument_Update_Information_Field_View_Model()
                    {
                        Instrument_Id = item.InsCode,
                        Namad_Id = item.namad_id,
                        Information_Field_Name = prop_name,
                        Information_Field_Value = prop_value
                    });
                }
            }

            return result;
        }

    }
}
