﻿using System;

namespace DataLayer.Models
{
    public class Engine_get_namad_info_Result
    {
        public long namad_id { get; set; }
        public string InsCode { get; set; }
        public string LVal18 { get; set; }
        public string LVal18AFC { get; set; }
        public string LVal30 { get; set; }
        public Nullable<int> data_provider_id { get; set; }
        public Nullable<double> strength { get; set; }
    }
}
