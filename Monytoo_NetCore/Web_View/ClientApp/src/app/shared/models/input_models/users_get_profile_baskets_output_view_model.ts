import { users_get_Private_baskets_Result } from "./users_get_Private_baskets_Result";
import { users_get_basket_history_Result } from "./users_get_basket_history_Result";

export interface users_get_profile_baskets_output_view_model {
  info: users_get_Private_baskets_Result;
  one_week_profit: number;
  one_month_profit: number;
  three_months_profit : number;
  total_value_USD : number;
  total_value_CHF : number;
  total_value : number;
  history: users_get_basket_history_Result[];
}
