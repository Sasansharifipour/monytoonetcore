import { get_base_data_model } from "./get_base_data_model.interface";

export interface get_post_by_index_view_model extends get_base_data_model {
  index: number;
  count: number;
}
