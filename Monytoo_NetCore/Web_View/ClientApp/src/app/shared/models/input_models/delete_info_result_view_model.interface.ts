import { result_model } from "./result_model.interface";

export interface delete_info_result_view_model extends result_model {
  is_deleted: boolean;
}
