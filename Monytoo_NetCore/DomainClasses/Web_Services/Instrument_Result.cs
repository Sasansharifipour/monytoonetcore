﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Web_Services
{
    public class Instrument_Result
    {
        public int DEVen { get; set; } = 0;

        public long InsCode { get; set; } = 0;

        public string InstrumentID { get; set; } = "";

        public string CValMne { get; set; } = "";

        public string LVal18 { get; set; } = "";

        public string CSocCSAC { get; set; } = "";

        public string LSoc30 { get; set; } = "";

        public string LVal18AFC { get; set; } = "";

        public string LVal30 { get; set; } = "";

        public string CIsin { get; set; } = "";

        public decimal QNmVlo { get; set; } = 0;

        public decimal ZTitad { get; set; } = 0;

        public int DESop { get; set; } = 0;

        public byte YOPSJ { get; set; } = 0;

        public string CGdSVal { get; set; } = "";

        public string CGrValCot { get; set; } = "";

        public int DInMar { get; set; } = 0;

        public byte YUniExpP { get; set; } = 0;

        public string YMarNSC { get; set; } = "";

        public string CComVal { get; set; } = "";

        public string CSecVal { get; set; } = "";

        public string CSoSecVal { get; set; } = "";

        public byte YDeComp { get; set; } = 0;

        public decimal PSaiSMaxOkValMdv { get; set; } = 0;

        public decimal PSaiSMinOkValMdv { get; set; } = 0;

        public long BaseVol { get; set; } = 0;

        public int YVal { get; set; } = 0;

        public int QPasCotFxeVal { get; set; } = 0;

        public int QQtTranMarVal { get; set; } = 0;

        public byte Flow { get; set; } = 0;

        public long QtitMinSaiOmProd { get; set; } = 0;

        public long QtitMaxSaiOmProd { get; set; } = 0;

        public short Valid { get; set; } = 0;
    }
}