﻿using System;

namespace DataLayer.Models
{
    public partial class Engine_pick_basket_for_evaluation_Result
    {
        public Nullable<double> order_price { get; set; }
        public Nullable<int> count { get; set; }
        public string current_price { get; set; }
    }
}
