﻿using System;

namespace DataLayer.Models
{
    public class Engine_get_basket_items_Result
    {
        public long item_id { get; set; }
        public Nullable<int> count { get; set; }
        public Nullable<double> order_price { get; set; }
        public string current_price { get; set; }
    }
}
