import { result_model } from "./result_model.interface";

export interface update_profile_data_result_model extends result_model {
  is_updated: boolean;
  field_key: string;
}
