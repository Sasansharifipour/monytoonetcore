﻿using System;

namespace DataLayer.Models
{
    public class Engine_get_all_basket_orders_Result
    {
        public long order_id { get; set; }
        public Nullable<long> basket_id { get; set; }
        public Nullable<int> count { get; set; }
        public Nullable<System.DateTime> create_time { get; set; }
        public Nullable<int> difficulty { get; set; }
        public Nullable<int> initial_Trade_Volume { get; set; }
        public Nullable<long> namad_id { get; set; }
        public Nullable<double> offer_price { get; set; }
        public string current_trade_volume { get; set; }
        public string last_price { get; set; }
    }
}
