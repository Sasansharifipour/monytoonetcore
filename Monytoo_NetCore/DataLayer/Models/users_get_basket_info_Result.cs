﻿using System;

namespace DataLayer.Models
{
    public class users_get_basket_info_Result
    {
        public Nullable<System.DateTime> create_time { get; set; }
        public Nullable<int> balance { get; set; }
        public Nullable<int> total_value { get; set; }
        public Nullable<System.DateTime> last_evaluation_time { get; set; }
        public Nullable<double> last_profit_change { get; set; }
        public string Name { get; set; }
        public Nullable<bool> is_shareable { get; set; }
        public Nullable<int> stock_count { get; set; }
    }
}
