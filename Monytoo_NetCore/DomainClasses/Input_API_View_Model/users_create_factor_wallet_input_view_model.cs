﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class users_create_factor_wallet_input_view_model : get_base_data_model
    {
        public int price { get; set; } = 0;

        public int payment_gateway_id { get; set; } = 0;

        public string i_data { get; set; } = "";
    }
}
