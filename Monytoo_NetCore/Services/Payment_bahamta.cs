﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace Services
{
    public static class Payment_bahamta
    {
        private static string api_key = "webpay:87fef14a-0442-4cab-87c3-6ac69ddce2d4:a7feca74-d22f-4d95-a01c-a1927bab3af6";
        private static string callbackURL = "http://bg1.linkpey.com/bahamta_payment/confirm"; //"https://localhost:44310/bahamta_payment/confirm";// "http://bg1.linkpey.com/bahamta_payment/confirm";

        public struct results_create_req
        {
            public string payment_url;
        }

        public class create_request_result
        {
            public string ok;
            public results_create_req result;
        }

        public static string create_request(string user_id, string factor_id, string price)
        {
            string result = "";
            try
            {
                string URL = "https://webpay.bahamta.com/api/create_request?api_key=" + api_key + "&reference=" + user_id + "-" + factor_id + "&amount_irr=" + price + "&callback_url=" + callbackURL;

                result = SendRequest_Get(URL);

                if (result != "")
                {
                    create_request_result deserializedResponse = JsonConvert.DeserializeObject<create_request_result>(result);
                    if (deserializedResponse.ok.ToLower() == "true")
                        return deserializedResponse.result.payment_url;
                }
            }
            catch
            {

            }

            return "";
        }

        public struct results_confirm_req
        {
            public string state;
            public string total;
            public string wage;
            public string gateway;
            public string terminal;
            public string pay_ref;
            public string pay_trace;
            public string pay_pan;
            public string pay_cid;
            public string pay_time;
        }

        public class confirm_request_result
        {
            public string ok;
            public results_confirm_req result;
        }

        public static string To_String(this results_confirm_req value)
        {
            var props = value.GetType().GetFields();
            var sb = new StringBuilder();

            foreach (var info in props)
            {
                var prop_value = info.GetValue(value) ?? "(null)";
                sb.Append(" " + info.Name + " : " + prop_value.ToString());
            }

            return sb.ToString();
        }

        public static confirm_request_result confirm_payment(long user_id, long factor_id, int price)
        {

            //confirm_request_result result = new confirm_request_result() { ok = "false" };
            try
            {
                string URL = "https://webpay.bahamta.com/api/confirm_payment?api_key=" + api_key + "&reference=" + user_id + "-" + factor_id + "&amount_irr=" + price;

                string data = SendRequest_Get(URL);

                if (data != "")
                {
                    confirm_request_result deserializedResponse = JsonConvert.DeserializeObject<confirm_request_result>(data);
                    if (deserializedResponse.ok.ToLower() == "true")
                        return deserializedResponse;
                }
            }
            catch
            {

            }

            return null;
        }

        private static string SendRequest_Get(string URL)
        {
            string responseString = "";
            try
            {
                HttpWebRequest Myrequest = (HttpWebRequest)WebRequest.Create(URL);

                Myrequest.KeepAlive = true;

                var response = (HttpWebResponse)Myrequest.GetResponse();


                responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                response.Close();

            }
            catch (Exception ex)
            {

            }

            return responseString;
        }
    }
}