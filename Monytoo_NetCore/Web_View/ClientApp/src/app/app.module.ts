import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClientJsonpModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { LoginComponent } from './login/login-form.component';


import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy-form.component';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material/dialog';
import { ErrorDialog } from './error-dialoge/error-dialoge';
import { LoginApproveComponent } from './login-approve/login-approve';
import { TermsOfConditionsComponent } from './terms-of-conditions/terms-of-conditions-form.component';
import { UserRegisterComponent } from './register-user/register-user';
import { HomePageComponent } from './home-page/home-page.component';
import { ProfileMainPageComponent } from './profile-main/profile-main.component';
import { FollowersPageComponent } from './followers-list/followers-list.component';
import { FollowingPageComponent } from './following-list/following-list.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ShowPostPageComponent } from './show-post/show-post.component';
import { ShowNotificationsComponent } from './show-notifications/show-notifications.component';
import { ExplorerPageComponent } from './explorer-page/explorer-page.component';
import { MatMenuModule } from '@angular/material/menu';

import * as echarts from 'echarts';
import { NgxEchartsModule } from 'ngx-echarts';
import { BasketDetailsComponent } from './basket-details/basket-details.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    BasketDetailsComponent,
    ErrorDialog,
    LoginApproveComponent,
    CounterComponent,
    FetchDataComponent,
    LoginComponent,
    HomePageComponent,
    ProfileMainPageComponent,
    ShowNotificationsComponent,
    FollowersPageComponent,
    FollowingPageComponent,
    PrivacyPolicyComponent,
    ExplorerPageComponent,
    ShowPostPageComponent,
    TermsOfConditionsComponent,
    UserRegisterComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    NgxEchartsModule.forRoot({ 
      echarts: () => import('echarts')
    }),
    AngularFontAwesomeModule,
    MatMenuModule,
    HttpClientJsonpModule,
    HttpClientModule,
    MatInputModule,
    MatDialogModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    MatCheckboxModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'error', component: ErrorDialog },
      { path: 'counter', component: CounterComponent },
      { path: 'login-approve', component: LoginApproveComponent },
      { path: 'show-post', component: ShowPostPageComponent },
      { path: 'show-notifications', component: ShowNotificationsComponent },
      { path: 'terms-of-conditions', component: TermsOfConditionsComponent },
      { path: 'fetch-data', component: FetchDataComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', component: UserRegisterComponent },
      { path: 'profile', component: ProfileMainPageComponent },
      { path: 'followers', component: FollowersPageComponent },
      { path: 'following', component: FollowingPageComponent },
      { path: 'explore', component: ExplorerPageComponent },
      { path: 'home-page', component: HomePageComponent },
      { path: 'privacy-policy', component: PrivacyPolicyComponent },
    ]),
    BrowserAnimationsModule
  ],
  providers: [{ provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }],
  bootstrap: [AppComponent]
})
export class AppModule { }
