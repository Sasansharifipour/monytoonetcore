﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Users
{
    public class user_profile_information_view_model
    {
        public string key_name { get; set; }

        public string name { get; set; }

        public string value { get; set; }

        public string parent_url { get; set; }
    }
}