﻿
namespace DomainClasses.Output_API_View_Model
{
    public class add_data_result_view_model: result_model
    {
        public bool is_added { get; set; } = false;
    }
}