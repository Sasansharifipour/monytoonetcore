﻿using System;

namespace DataLayer.Models
{
    public class users_get_basket_history_Result
    {
        public long ID { get; set; }
        public Nullable<long> balance { get; set; }
        public Nullable<double> daily_profit { get; set; }
        public Nullable<System.DateTime> insert_time { get; set; }
        public Nullable<long> total_value { get; set; }
        public Nullable<int> total_value_CHF { get; set; }
        public Nullable<int> total_value_USD { get; set; }
    }
}
