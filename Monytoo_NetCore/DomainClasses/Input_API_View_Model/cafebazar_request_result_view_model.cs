﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class cafebazar_request_result_view_model
    {
        public int consumptionState { get; set; } = 0;

        public int purchaseState { get; set; } = 0;

        public string kind { get; set; } = "";

        public string developerPayload { get; set; } = "";

        public long purchaseTime { get; set; } = 0;
    }
}
