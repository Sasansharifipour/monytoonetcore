﻿using NetTrader.Indicator;
using DomainClasses.Instrument;
using DomainClasses.RSI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using static CommonCode.CommonCodes;
using CommonCode;
using System.IO;

namespace Services
{
    public interface IRSIService
    {
        void find_and_post__rsi_for_all_namads(long user_id, string secret_key);
    }

    public class RSIService : IRSIService
    {
        private IInstrumentService _instrumentService;
        private ICreator<IDataProvider> _creator;
        private IImageService _imageService;
        private IuserServices _userService;
        private IPostService _postService;
        private ILogger _logger;
        private string secret_key = "";
        private long user_id = 0;

        public RSIService(IInstrumentService instrumentService, ICreator<IDataProvider> creator, IImageService imageService, IuserServices userService, IPostService postService, ILogger logger)
        {
            _instrumentService = instrumentService;
            _creator = creator;
            _logger = logger;
            _imageService = imageService;
            _userService = userService;
            _postService = postService;
            (user_id, secret_key) = Config.get_user_info_from_config("rsi");
        }

        private void post_for_namad(long namad_id, string namad_name, decimal namad_price, double rsi, RSI_POST_enum post_type)
        {
            try
            {
                var data = _userService.users_get_profile_information_fields(user_id, secret_key, namad_id);

                if (data != null && data.data != null)
                    foreach (var item in data.data)
                    {
                        if (item == null || item.key_name == null || item.value == null)
                            continue;
                        if (item.key_name.Trim() == "ENGINE_RSI_Last_Status" && item.value.Trim() == post_type.ToString().Trim())
                            return;
                    }

                rsi = Math.Round(rsi, 2);
                RSI_Posting_View_Model model = new RSI_Posting_View_Model() { namad_name = namad_name, post_type = post_type, rsi = rsi, namad_price = namad_price };

                Bitmap image = ImageUtil.generate_rsi_image_for_namads(model);
                string file_path = _imageService.save_image(image, saving_image_location.post_image);

                string caption = "نماد " + namad_name + " با قیمت " + namad_price.ToString("#,##0") + " و RSI " + rsi.ToString() + " " + model.post_type.GetDesciption();

                if (!file_path.IsNullOrEmptyOrWhiteSpace())
                {
                    string image_path = Config.get_root_path() + file_path;
                    string API_Key = String.Format("{0}-{1}", user_id, secret_key);

                    string file_name = Path.GetFileName(image_path);

                    var file_data = File.ReadAllBytes(image_path);

                    var result = _postService.post("http://Api.moneytoo.ir/api/users/send_post", file_name, file_data,
                        API_Key, caption, namad_id.ToString());

                    int result_update_db = result.Post_id;

                    if (result_update_db <= 0)
                    {
                        _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} - InsCode : {3}", "RSIServices",
                            "post_for_namad", "Can not post for RSI", namad_id));
                    }
                    else
                    {
                        bool updated = false;
                        do
                        {
                            updated = _instrumentService.update_instrument_info(user_id, secret_key, namad_id, "ENGINE_RSI_Last_Status", post_type.ToString());
                        } while (!updated);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} - InsCode : {3}", "RSIServices",
                    "post_for_namad", ex.Message, namad_id));
            }
        }

        public void find_and_post__rsi_for_all_namads(long user_id, string secret_key)
        {
            try
            {
                var all_namads = _instrumentService.get_all_namads(user_id, secret_key);

                foreach (var item in all_namads)
                {
                    DateTime start = DateTime.Now;

                    try
                    {
                        long Instrument_Code = 0;
                        long.TryParse(item.InsCode, out Instrument_Code);

                        double? rsi_last_value = 0;
                        double? rsi_yesterday_value = 0;
                        decimal? last_price = 0;
                        (last_price, rsi_last_value, rsi_yesterday_value) = find_rsi_for_namad(Instrument_Code, item.data_provider_id);

                        if (rsi_last_value.HasValue && rsi_last_value.Value > 0)
                        {
                            bool updated = false;
                            do
                            {
                                updated = _instrumentService.update_instrument_info(user_id, secret_key, item.namad_id, "ENGINE_RSI_Data", rsi_last_value.Value.ToString());
                            } while (!updated);
                        }

                        if (rsi_last_value.HasValue && rsi_last_value.Value > 0 && rsi_yesterday_value.HasValue && rsi_yesterday_value.Value > 0 && last_price.HasValue)
                        {
                            if (rsi_yesterday_value < 30 && rsi_last_value > 30)
                                post_for_namad(item.namad_id, item.LVal18AFC, last_price.Value, rsi_last_value.Value, RSI_POST_enum.Exit_Sales);

                            if (rsi_yesterday_value < 70 && rsi_last_value > 70)
                                post_for_namad(item.namad_id, item.LVal18AFC, last_price.Value, rsi_last_value.Value, RSI_POST_enum.Enter_Bought);

                            if (rsi_yesterday_value > 30 && rsi_last_value < 30)
                                post_for_namad(item.namad_id, item.LVal18AFC, last_price.Value, rsi_last_value.Value, RSI_POST_enum.Enter_Sales);

                            if (rsi_yesterday_value > 70 && rsi_last_value < 70)
                                post_for_namad(item.namad_id, item.LVal18AFC, last_price.Value, rsi_last_value.Value, RSI_POST_enum.Exit_Bought);

                        }
                    }
                    catch (Exception ex)
                    {

                        string ins_code = "";

                        if (item != null && item.InsCode != null)
                            ins_code = item.InsCode;

                        _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} - InsCode : {3}", "RSIervices", "find_and_post__rsi_for_all_namads", ex.Message, ins_code));
                    }
                    finally
                    {
                        var run_time = DateTime.Now - start;

                        Thread.Sleep(5000 - Math.Min(5000, run_time.Milliseconds));
                    }
                }

            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2}", "RSIervices", "find_and_post__rsi_for_all_namads", e.Message));
            }
        }

        public (decimal?, double?, double?) find_rsi_for_namad(long Instrument_Code, int? data_provider_id)
        {
            try
            {
                RSI_Config_View_Model config = load_rsi_config_for_namad(Instrument_Code);

                if (!data_provider_id.HasValue)
                    return (null, null, null);

                IDataProvider dataProvider = _creator.FactoryMethod(data_provider_id.Value);

                var ins_trades = dataProvider.get_Trade_From_Date_To_Date(Instrument_Code, DateTime.Now.AddDays(-1 * config.get_day_counts), DateTime.Now).OrderBy(s => s.DEven).ToList();
                ins_trades = ins_trades.Skip(Math.Max(0, ins_trades.Count() - config.calculate_day_counts - 2)).ToList();
                var last_price = ins_trades.Last().PClosing;
                var rsi_calculated = calculate_rsi(config, ins_trades);

                return (last_price, rsi_calculated.Item1, rsi_calculated.Item2);
            }
            catch (Exception e)
            {
                return (null, null, null);
            }
        }

        private (double?, double?) calculate_rsi(RSI_Config_View_Model config, List<Instrument_Trade_View_Model> data)
        {
            try
            {
                RSI rsi = new RSI(config.calculate_day_counts);
                List<Ohlc> ohlcList = new List<Ohlc>();

                foreach (var item in data)
                {
                    Ohlc ohlc = new Ohlc()
                    {
                        Date = item.DEven,
                        Low = (double)item.PriceMin,
                        High = (double)item.PriceMax,
                        AdjClose = (double)item.PClosing,
                        Volume = (double)item.QTotCap,
                        Close = (double)item.PDrCotVal,
                        Open = (double)item.PriceFirst
                    };
                    ohlcList.Add(ohlc);
                }

                rsi.Load(ohlcList);
                RSISerie serie = rsi.Calculate();

                return (serie.RSI[serie.RSI.Count - 1], serie.RSI[serie.RSI.Count - 2]);
            }
            catch (Exception e)
            {
                return (null, null);
            }

        }

        public RSI_Config_View_Model load_rsi_config_for_namad(long Instrument_Code)
        {
            return new RSI_Config_View_Model() { calculate_day_counts = 14 };
        }
    }
}