﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Instrument
{
    public class Static_Thresholds_Result_View_Model
    {
        public long InsCode { get; set; }
        public decimal PSGelStaMax { get; set; }
        public decimal PSGelStaMin { get; set; }
    }
}
