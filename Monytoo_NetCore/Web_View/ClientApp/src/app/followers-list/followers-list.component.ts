import { Component, Inject, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../shared/services/users.services';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router, ActivatedRoute } from '@angular/router';
import { get_profile_data_model } from '../shared/models/output_models/get_profile_data_model.interface';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { get_user_follow_view_model } from '../shared/models/output_models/get_user_follow_view_model.interface';
import { users_get_profile_followers_result_view_model } from '../shared/models/input_models/users_get_profile_followers_result_view_model.interface';
import { users_get_profile_followers_Result } from '../shared/models/input_models/users_get_profile_followers_Result.interface';
import { get_profile_data_result_model } from '../shared/models/input_models/get_profile_data_result_model.interface';
import { user_profile_data_view_model } from '../shared/models/input_models/user_profile_data_view_model.interface';
import { update_profile_data_result_model } from '../shared/models/input_models/update_profile_data_result_model.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'app-followers-list-page',
  templateUrl: './followers-list.component.html'
})
export class FollowersPageComponent implements OnInit {
  private pg_idx: number = -1;
  private _user_id;
  private profile_user_id;
  isLoading = false;

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      if (params['user_id'] != undefined)
        this._user_id = params['user_id'];
      else
        this._user_id = this.profile_user_id;

      this.loadProfileInfo();
      this.loadInfo();
    });

  }

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private router: Router, private route: ActivatedRoute, private dialogService: MatDialog, private sanitizer: DomSanitizer,
    private _location: Location) {

    this.APIKEY = localStorage.getItem('API_KEY');
    this.profile_user_id = localStorage.getItem('USER_ID');

    if (this.APIKEY == null) {
      this.router.navigate(['']);
    }
  }

  private APIKEY: string = '';
  private data: users_get_profile_followers_Result[] = [];
  private profile_data: user_profile_data_view_model;

  @HostListener("window:scroll", ["$event"])
  getScrollHeight(): void {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight && !this.isLoading) {
      this.loadInfo();
    }
  }

  loadProfileInfo() {
    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: this._user_id, t: '' };

    this.userService.get_profile_data(model).subscribe((result: get_profile_data_result_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.profile_data = result.data;
      }

    });

  }

  loadInfo() {
    this.isLoading = true;
    var user_id = this.APIKEY.split('-')[0];

    var model: get_user_follow_view_model = { count: 10, index: this.pg_idx + 1, API_Key: this.APIKEY, user_profile_id: this._user_id };

    this.userService.users_get_profile_followers(model).subscribe((result: users_get_profile_followers_result_view_model) => {

      console.log(result);

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.pg_idx = this.pg_idx + 1;
        let t_data: users_get_profile_followers_Result[];
        t_data = result.informations;
        
        t_data.forEach(item => {
          this.get_profile_image(item.id, item.profile_image_t).subscribe(result => {
            let objectURL = URL.createObjectURL(result);
              item.profile_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
          });
        });

        t_data.forEach(item => {
          this.data.push(item);
        });
      }

    });

    this.isLoading = false;
  }

  backClicked() {
    this._location.back();
  }

  unfollow_user(user_id) {

    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_id.toString(), t:'' };

    this.userService.unfollow(model).subscribe((result: update_profile_data_result_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else if (result.is_updated)
      {
        this.data.forEach(item => {
          if (item.id == user_id) {
            item.is_followed = '0';
          }
        });
      }

    });

  }

  follow_user(user_id) {
    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_id.toString(), t:'' };

    this.userService.follow(model).subscribe((result: update_profile_data_result_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else if (result.is_updated) {
        this.data.forEach(item => {
          if (item.id == user_id) {
            item.is_followed = '1';
          }
        });
      }

    });

  }

  get_profile_image(user_profile_id, t): Observable<string> {

    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_profile_id, t: t };

    return this.userService.get_profile_image(model);
  }
}

