﻿using System;

namespace DataLayer.Models
{
    public class users_get_profile_baskets_Result
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public Nullable<int> total_value { get; set; }
        public Nullable<System.DateTime> create_time { get; set; }
        public Nullable<System.DateTime> last_evaluation_time { get; set; }
        public Nullable<bool> is_shareable { get; set; }
        public Nullable<int> item_count { get; set; }
        public Nullable<System.DateTime> expiration_date { get; set; }
    }
}
