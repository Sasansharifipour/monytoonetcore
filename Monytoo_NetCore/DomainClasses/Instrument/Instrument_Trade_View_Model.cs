﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Instrument
{
    public class Instrument_Trade_View_Model
    {
        public long InsCode { get; set; } = 0;

        public DateTime DEven { get; set; } = new DateTime(1, 1, 1);

        public decimal ZTotTran { get; set; } = 0;

        public decimal QTotTran5J { get; set; } = 0;

        public decimal QTotCap { get; set; } = 0;

        public decimal PClosing { get; set; } = 0;

        public decimal PDrCotVal { get; set; } = 0;

        public decimal PriceChange { get; set; } = 0;

        public decimal PriceMin { get; set; } = 0;

        public decimal PriceMax { get; set; } = 0;

        public decimal PriceFirst { get; set; } = 0;

        public decimal PriceYesterday { get; set; } = 0;
    }
}