﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Output_API_View_Model
{
    public class users_get_profile_baskets_output_view_model
    {
        public object info { get; set; }

        public double? one_week_profit { get; set; }
        public double? one_month_profit { get; set; }
        public double? three_months_profit { get; set; }
        public double? total_value_USD { get; set; }
        public double? total_value_CHF { get; set; }
        public double? total_value { get; set; }

        public object history { get; set; }
    }
}
