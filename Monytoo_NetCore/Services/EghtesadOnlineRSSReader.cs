﻿using DomainClasses.RSS;
using System;
using System.Collections.Generic;
using System.Globalization;
using static CommonCode.CommonCodes;

namespace Services
{
    public class EghtesadOnlineRSSReader : IRSSProvider
    {
        private IRSSReaderService _rssReaderService;

        public EghtesadOnlineRSSReader(IRSSReaderService rssReaderService)
        {
            _rssReaderService = rssReaderService;
        }

        public IEnumerable<RssNews> Read_Feeds(string rss_url)
        {
            List<RssNews> results = new List<RssNews>();

            try
            {
                DateTimeFormatInfo info = new DateTimeFormatInfo()
                { FullDateTimePattern = "ddd, dd MMM yyyy h:mm tt zzz" };

                var data = _rssReaderService.Read_RSS_From_URL(rss_url, "item");
                var rss_data = Convert_RSS_Feed_To_Object<RSS_eghtesadonline>(data);

                foreach (var item in rss_data)
                {
                    string image_path = "";
                    string description = "";

                    var str = item.description.Split(new string[] { "<div>" }, StringSplitOptions.RemoveEmptyEntries);

                    if (str.Length > 1)
                    {
                        image_path = str[0].Replace("  ", " ").Replace("\" >", "\">").Replace("<img src=\"", "").Replace("\">", "");
                        description = str[1].Replace("</div>", "");
                    }
                    else
                        description = item.description;

                    RssNews rss = new RssNews()
                    {
                        Rss_URL = rss_url,
                        Title = item.title,
                        Description = description,
                        Link = item.link,
                        Date = Convert.ToDateTime(item.pubDate, info),
                        Image_path = image_path,
                    };

                    results.Add(rss);
                }
            }
            catch (Exception e)
            {
                return new List<RssNews>();
            }

            return results;
        }
    }
}