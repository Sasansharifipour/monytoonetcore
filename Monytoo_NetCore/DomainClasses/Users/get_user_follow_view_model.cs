﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Users
{
    public class get_user_follow_view_model : get_base_data_model
    {
        public string user_profile_id { get; set; } = "";

        public int? count { get; set; } = 0;

        public int? index { get; set; } = 0;
    }
}