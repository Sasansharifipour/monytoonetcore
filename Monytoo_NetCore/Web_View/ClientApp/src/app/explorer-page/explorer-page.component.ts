import { Component, Inject, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { UserService } from '../shared/services/users.services';
import { check_user_name_available_model } from '../shared/models/output_models/check_user_name_available_model.interface';
import { UserRegistration } from '../shared/models/inline_models/user.registration.interface';
import { register_model } from '../shared/models/output_models/register_model.interface';
import { register_result_model } from '../shared/models/input_models/register_result_model.interface';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router } from '@angular/router';
import { get_post_by_index_view_model } from '../shared/models/output_models/get_post_by_index_view_model.interface';
import { users_home_posts_result_view_model } from '../shared/models/input_models/users_home_posts_result_view_model.interface';
import { users_home_posts_Result } from '../shared/models/input_models/users_home_posts_Result.interface';
import { get_profile_data_model } from '../shared/models/output_models/get_profile_data_model.interface';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { get_post_image_view_model } from '../shared/models/output_models/get_post_image_view_model';
import { users_explore_posts_result_view_model } from '../shared/models/input_models/users_explore_posts_result_view_model.interface';
import { users_Explore_posts_Result } from '../shared/models/input_models/users_Explore_posts_Result.interface';

@Component({
  selector: 'app-explorer-page',
  templateUrl: './explorer-page.component.html'
})
export class ExplorerPageComponent  {
  private pg_idx: number = -1;
  private cnt: number = 15;
  isLoading = false;
  split_data: any[];

  splitArr(arr, size) {
    let newArr = [];
    for (let i = 0; i < arr.length; i += size) {
      newArr.push(arr.slice(i, i + size));
    }
    return newArr;
  }

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private router: Router, private dialogService: MatDialog, private sanitizer: DomSanitizer) {
    this.APIKEY = localStorage.getItem('API_KEY');

    if (this.APIKEY == null) {
      this.router.navigate(['']);
    }

    this.loadInfo();
  }
  private APIKEY: string = '';
  private data: users_Explore_posts_Result[] = [];

  @HostListener("window:scroll", ["$event"])
  getScrollHeight(): void {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight && !this.isLoading) {
      this.loadInfo();
    }
  }

  loadInfo() {
    this.isLoading = true;
    var model: get_post_by_index_view_model = { count: this.cnt, index: this.pg_idx + 1, API_Key: this.APIKEY };

    this.userService.users_explore_posts(model).subscribe((result: users_explore_posts_result_view_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.pg_idx = this.pg_idx + 1;
        let t_data: users_Explore_posts_Result[];
        t_data = result.informations;
        
        t_data.forEach(item => {
          
          this.get_post_image(item.post_id, item.image_id).subscribe(result => {
            let objectURL = URL.createObjectURL(result);
            item.post_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
          });
        });

        t_data.forEach(item => {
          this.data.push(item);
        });

        this.split_data = this.splitArr(this.data, 3);
      }

    });

    this.isLoading = false;
  }

  get_profile_image(user_profile_id, t): Observable<string> {

    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_profile_id, t: t };

    return this.userService.get_profile_image(model);
  }

  get_post_image(post_id, image_id): Observable<string> {

    var model: get_post_image_view_model = { API_Key: this.APIKEY, post_id: post_id, image_id: image_id };

    return this.userService.users_get_image(model);
  }

}

