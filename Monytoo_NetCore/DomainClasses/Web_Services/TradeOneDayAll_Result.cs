﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Web_Services
{
    public class TradeOneDayAll_Result
    {
        public string LVal18AFC { get; set; } = "";

        public int DEven { get; set; } = 0;

        public decimal ZTotTran { get; set; } = 0;

        public decimal QTotTran5J { get; set; } = 0;

        public decimal QTotCap { get; set; } = 0;

        public long InsCode { get; set; } = 0;

        public string LVal30 { get; set; } = "";

        public decimal PClosing { get; set; } = 0;

        public decimal PDrCotVal { get; set; } = 0;

        public decimal PriceChange { get; set; } = 0;

        public decimal PriceMin { get; set; } = 0;

        public decimal PriceMax { get; set; } = 0;

        public decimal PriceFirst { get; set; } = 0;

        public decimal PriceYesterday { get; set; } = 0;
    }
}