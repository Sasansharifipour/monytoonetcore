﻿using DomainClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Output_API_View_Model
{
    public class check_version_result_view_model : result_model
    {
        public long status_id { get; set; } = 0;

        public string message { get; set; } = "";

        public string update_url { get; set; } = "";
    }
}
