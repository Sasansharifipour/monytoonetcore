﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace Services
{
    public class TsePublicV2
    {
        public DataSet StaticThresholds(string tsem_username, string tsem_password, byte flow)
        {
            throw new NotImplementedException();
        }

        public DataSet InstTrade(string tsem_username, string tsem_password, long instrument_Code, int date_From, int date_To)
        {
            throw new NotImplementedException();
        }

        public DataSet TradeLastDay(string tsem_username, string tsem_password, byte flow)
        {
            throw new NotImplementedException();
        }

        public Task<DataSet> InstrumentsStateChange(string tsem_username, string tsem_password, int instrument_Code, int date)
        {
            throw new NotImplementedException();
        }

        public DataSet InstrumentsState(string tsem_username, string tsem_password, byte flow)
        {
            throw new NotImplementedException();
        }

        public DataSet Instrument(string tsem_username, string tsem_password, byte flow)
        {
            throw new NotImplementedException();
        }

        public DataSet BestLimitOneIns(string tsem_username, string tsem_password, long instrument_Code)
        {
            throw new NotImplementedException();
        }

        public DataTable ClientType(string tsem_username, string tsem_password)
        {
            throw new NotImplementedException();
        }

        public DataSet TradeOneDay(string tsem_username, string tsem_password, int date, byte flow)
        {
            throw new NotImplementedException();
        }

        public DataSet TradeOneDayAll(string tsem_username, string tsem_password, int date, byte flow)
        {
            throw new NotImplementedException();
        }
    }
}