﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Output_API_View_Model
{
    public class users_get_basket_data_output_view_model
    {
        public object result_basket_items { get; set; }

        public object result_basket_order_history { get; set; }

        public object result_basket_info { get; set; }
    }
}
