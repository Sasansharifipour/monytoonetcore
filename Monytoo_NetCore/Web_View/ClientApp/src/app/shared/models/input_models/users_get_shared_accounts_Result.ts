export interface users_get_shared_accounts_Result {
  iD: number;
  secret_key: string;
  type_id: number;
  t: number;
  username: string;
  name: string;
  lastname: string;
}
