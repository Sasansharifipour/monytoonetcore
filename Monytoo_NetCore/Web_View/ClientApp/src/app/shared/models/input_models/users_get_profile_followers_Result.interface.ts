import { SafeUrl } from "@angular/platform-browser";

export interface users_get_profile_followers_Result {
  id: number;
  name: string;
  lastname: string;
  username: string;
  is_followed: string;
  r_number: number;
  user_type: number;
  profile_image_t: number;
  profile_image_data: SafeUrl;
}
