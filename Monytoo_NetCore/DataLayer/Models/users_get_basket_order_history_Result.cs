﻿using System;

namespace DataLayer.Models
{
    public class users_get_basket_order_history_Result
    {
        public long order_id { get; set; }
        public Nullable<System.DateTime> create_time { get; set; }
        public string namad_name { get; set; }
        public Nullable<int> count { get; set; }
        public Nullable<double> offer_price { get; set; }
        public Nullable<int> status_id { get; set; }
        public Nullable<long> r_number { get; set; }
    }
}
