﻿using CommonCode;
using DomainClasses.Instrument;
using DomainClasses.RSI;
using DomainClasses.Users;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using static CommonCode.CommonCodes;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace Services
{
    public interface INamadService
    {
        List<Namad_View_Model> get_all_namads(get_base_data_model model);

        List<Trades_View_Model> get_namad_price(Namad_Get_Price_View_Model model);

        Engine_get_namad_info_Result get_namad_info(Namad_Get_View_Model model);

        Engine_get_namad_info_Result get_namad_info(long user_id, string secret_key, long? namad_id);

        update_profile_data_result_model set_impact_on_Namad(RSI_Set_Impact_View_Model model);

        update_profile_data_result_model update_namad_information_fields(Instrument_Update_Information_Field_API_View_Model model);

        bool Save_namad_infoAsync(string id, string username, string name, string family);
    }

    public class NamadService : INamadService
    {
        private IInstrumentService _instrumentService;
        private IHashingService _hashingService;
        private ICreator<IDataProvider> _creator;
        private IuserServices _userService;
        private IDbContextFactory<connectionString> _contextFactory;

        public NamadService(IInstrumentService instrumentService, IHashingService hashingService, connectionString db
            , ICreator<IDataProvider> creator, IuserServices userService, IDbContextFactory<connectionString> contextFactory)
        {
            _instrumentService = instrumentService;
            _hashingService = hashingService;
            _contextFactory = contextFactory;
            _creator = creator;
            _userService = userService;
        }

        public List<Namad_View_Model> get_all_namads(get_base_data_model model)
        {
            try
            {
                string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                string User_id = "";
                string secret_key = "";

                (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                long user_id = 0;
                long.TryParse(User_id, out user_id);

                var result = _instrumentService.get_all_namads(user_id, secret_key).Select(s => new Namad_View_Model
                {
                    namad_id = s.namad_id,
                    name = s.LVal18AFC
                }).ToList();

                return result;
            }
            catch (Exception ex)
            {
                return new List<Namad_View_Model>();
            }

            return new List<Namad_View_Model>();
        }

        public Engine_get_namad_info_Result get_namad_info(long user_id, string secret_key, long? namad_id)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", 0);
                    var results = _db.Engine_get_namad_info(user_id, secret_key, namad_id, res).FirstOrDefault();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        return new Engine_get_namad_info_Result();
                    }

                    return results;
                }
            }
            catch (Exception ex)
            {
                return new Engine_get_namad_info_Result();
            }
        }

        public Engine_get_namad_info_Result get_namad_info(Namad_Get_View_Model model)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0);
                    var results = _db.Engine_get_namad_info(user_id, secret_key, model.namad_id, res).FirstOrDefault();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        return new Engine_get_namad_info_Result();
                    }

                    return results;
                }
            }
            catch (Exception ex)
            {
                return new Engine_get_namad_info_Result();
            }
        }

        public List<Trades_View_Model> get_namad_price(Namad_Get_Price_View_Model model)
        {
            try
            {
                string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                string User_id = "";
                string secret_key = "";

                (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                long user_id = 0;
                long.TryParse(User_id, out user_id);

                var namad = get_namad_info(new Namad_Get_View_Model() { API_Key = model.API_Key, namad_id = model.namad_id });
                long Instrument_Code = 0;
                long.TryParse(namad.InsCode, out Instrument_Code);

                if (namad.namad_id <= 0 || Instrument_Code <= 0 || !namad.data_provider_id.HasValue)
                    return new List<Trades_View_Model>();

                IDataProvider dataProvider = _creator.FactoryMethod(namad.data_provider_id.Value);

                var result = dataProvider.get_Trade_From_Date_To_Date(Instrument_Code, model.from_date, model.to_date).Select(s => new Trades_View_Model
                {
                    date = s.DEven,
                    price = s.PClosing
                }).ToList();

                var today_price = result.FirstOrDefault(s => s.date.Date == DateTime.Now.Date);

                if (today_price == null)
                {
                    var info = _userService.users_get_profile_information_fields(user_id, secret_key, namad.namad_id);

                    if (info != null && info.data != null)
                    {
                        var last_price = info.data.FirstOrDefault(s => s.key_name == "ENGINE_Price_Reader_Last_Price");

                        if (last_price != null && last_price.value != null && !last_price.value.Trim().IsNullOrEmptyOrWhiteSpace())
                        {
                            double price = 0;

                            double.TryParse(last_price.value.Trim(), out price);

                            result.Add(new Trades_View_Model() { date = DateTime.Now, price = (int)price });
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return new List<Trades_View_Model>();
            }

            return new List<Trades_View_Model>();
        }

        public update_profile_data_result_model set_impact_on_Namad(RSI_Set_Impact_View_Model model)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model();
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);


                    SqlParameter res = new SqlParameter("res", 0);
                    var results = _db.Engine_set_impact_on_Namad(user_id, secret_key, model.namad_id, model.strength, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_updated = true;
                    }

                    return result_model;
                }
            }
            catch (Exception ex)
            {
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
            }

            return result_model;
        }

        public update_profile_data_result_model update_namad_information_fields(Instrument_Update_Information_Field_API_View_Model model)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model();
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);


                    SqlParameter res = new SqlParameter("res", 0);
                    var results = _db.Engine_update_namad_information_fields(user_id, secret_key, model.namad_Id, model.information_Field_Name, model.information_Field_Value, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_updated = true;
                    }

                    return result_model;
                }
            }
            catch (Exception ex)
            {
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
            }

            return result_model;
        }

        public bool Save_namad_infoAsync(string id, string username, string name, string family)
        {
            try
            {
                return _userService.Save_user_infoAsync(id, username, name, family);
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }
    }
}