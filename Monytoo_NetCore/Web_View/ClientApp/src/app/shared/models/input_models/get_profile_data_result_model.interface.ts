import { result_model } from "./result_model.interface";
import { user_profile_data_view_model } from "./user_profile_data_view_model.interface";

export interface get_profile_data_result_model extends result_model {
  data: user_profile_data_view_model;
}
