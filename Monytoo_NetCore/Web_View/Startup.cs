using DataLayer.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Services;
using System.Data.Entity.Infrastructure;
using Unity;
using Unity.AspNet.Mvc;

namespace Web_View
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders =
                    ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.AddControllersWithViews();

            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist";
            });
        }

        public void ConfigureContainer(IUnityContainer container)
        {
            container.RegisterType<connectionString, connectionString>();
            container.RegisterType<IDbContextFactory<connectionString>, DbContextFactory>();
            container.RegisterType<ITGJUService, TGJUService>();
            container.RegisterType<IuserServices, userServices>();
            container.RegisterType<IVerificationService, VerificationService>();
            container.RegisterType<ICodeGeneratorService, CodeGeneratorService>();
            container.RegisterType<IHashingService, HashingService>();
            container.RegisterType<ITsetmcService, TsetmcService>();
            container.RegisterType<TsePublicV2, TsePublicV2>();
            container.RegisterType<IImageService, ImageService>();
            container.RegisterType<IRssService, RssService>();
            container.RegisterType<IInstrumentService, InstrumentService>();
            container.RegisterType<IRSIService, RSIService>();
            container.RegisterType<IPostService, PostService>();
            container.RegisterType<IMACDService, MACDService>();
            container.RegisterType<INamadService, NamadService>();
            container.RegisterType<IBasketService, BasketService>();
            container.RegisterType<Services.ILogger, Logger>();
            container.RegisterType<IRSSReaderService, RSSReaderService>();
            container.RegisterType<IBoardReaderService, BoardReaderService>();
            container.RegisterType<ICreator<IDataProvider>, DataProviderFactoryCreator>();
            container.RegisterType<ICodalService, CodalService>();
            container.RegisterType<ICreator<IRSSProvider>, RSSProviderFactoryCreator>();
            container.RegisterType<IDataProvider, TGJUDataProvider>("TGJU", new PerRequestLifetimeManager());
            container.RegisterType<IDataProvider, BourseDataProvider>("Bourse", new PerRequestLifetimeManager());
            container.RegisterType<IDataProvider, NullDataProvider>("Null", new PerRequestLifetimeManager());
            container.RegisterType<IRSSProvider, NabzeBourseRSSReader>("NabzeBourse", new PerRequestLifetimeManager());
            container.RegisterType<IRSSProvider, EghtesadOnlineRSSReader>("EghtesadOnline", new PerRequestLifetimeManager());
            container.RegisterType<IRSSProvider, NullRSSProvider>("Null", new PerRequestLifetimeManager());
            //container.RegisterSingleton<ICassandraDBContext, CassandraDBContext>();
            //container.RegisterSingleton<IMessagingService, MessagingService>();

        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseForwardedHeaders();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseAngularCliServer(npmScript: "start");
                }
            });
        }
    }
}
