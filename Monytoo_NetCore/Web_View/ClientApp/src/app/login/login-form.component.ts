import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { UserService } from '../shared/services/users.services';
import { check_user_name_available_model } from '../shared/models/output_models/check_user_name_available_model.interface';
import { UserRegistration } from '../shared/models/inline_models/user.registration.interface';
import { register_model } from '../shared/models/output_models/register_model.interface';
import { register_result_model } from '../shared/models/input_models/register_result_model.interface';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html'
})
export class LoginComponent {
  private result_model: register_result_model;
  private mobile_number: string = '';
  private readyToSend: boolean = true;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private router: Router, private dialogService: MatDialog) {
  }

  register({ value }: { value: UserRegistration }) {
    this.readyToSend = false;
    let model: register_model = {
      mobile_number: value.mobile, firebase_id: null, invitation_code: null,
      lastname: null, name: null, username: null
    };

    this.mobile_number = value.mobile;

    this.userService.register(model).subscribe(result => {
      this.result_model = result;

      if (this.result_model.error_Id != "0") {
        this.readyToSend = true;
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        if (this.result_model.is_new_user) {
          this.router.navigate(['register']);
        }
        else {
          this.router.navigate(['login-approve', { mobile_number: this.mobile_number, user_session_ID: this.result_model.user_session_ID }]);
        }
      }

    });
  }
}

