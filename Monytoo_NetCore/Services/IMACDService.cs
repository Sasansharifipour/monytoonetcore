﻿using NetTrader.Indicator;
using DomainClasses.Common;
using DomainClasses.Instrument;
using DomainClasses.MACD;
using DomainClasses.RSI;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using static CommonCode.CommonCodes;
using CommonCode;
using System.Data.Entity.Infrastructure;
using System.IO;

namespace Services
{
    public interface IMACDService
    {
        void find_and_post__macd_for_all_namads(long user_id, string secret_key);
    }

    public class MACDService : IMACDService
    {
        private IInstrumentService _instrumentService;
        private ICreator<IDataProvider> _creator;
        private IImageService _imageService;
        private IuserServices _userService;
        private IPostService _postService;
        private ILogger _logger;

        public MACDService(IInstrumentService instrumentService, ICreator<IDataProvider> creator, IImageService imageService,
            IuserServices userService, ILogger logger, IPostService postService)
        {
            _instrumentService = instrumentService;
            _imageService = imageService;
            _userService = userService;
            _postService = postService;
            _logger = logger;
            _creator = creator;
        }

        public void find_and_post__macd_for_all_namads(long user_id, string secret_key)
        {
            try
            {
                var all_namads = _instrumentService.get_all_namads(user_id, secret_key);

                foreach (var item in all_namads)
                {
                    DateTime start = DateTime.Now;

                    try
                    {
                        long Instrument_Code = 0;
                        long.TryParse(item.InsCode, out Instrument_Code);

                        double? macd_line = null;
                        double? macd_histogram = null;
                        double? macd_signal = null;

                        List<double?> macd_line_list;
                        List<double?> macd_histogram_list;
                        List<double?> macd_signal_list;
                        List<Instrument_Trade_View_Model> data;

                        ((macd_line_list, macd_histogram_list, macd_signal_list), data) = find_macd_for_namad(Instrument_Code);

                        if (macd_line_list != null && macd_line_list.Count > 0)
                            macd_line = macd_line_list.Last();

                        if (macd_histogram_list != null && macd_histogram_list.Count > 0)
                            macd_histogram = macd_histogram_list.Last();

                        if (macd_signal_list != null && macd_signal_list.Count > 0)
                            macd_signal = macd_signal_list.Last();

                        bool? nullable_crossing = null;
                        bool? nullable_is_sell = null;

                        int status = 0;

                        (nullable_crossing, nullable_is_sell) = find_crossing(macd_signal_list, macd_histogram_list);

                        if (nullable_crossing.HasValue && nullable_crossing.Value)
                        {
                            bool is_diverge = is_divergent(nullable_is_sell.Value, data, macd_histogram_list);

                            if (is_diverge && macd_histogram.HasValue)
                            {
                                string image_path = Config.get_image_path_for_macd_post(nullable_is_sell.Value, macd_histogram.Value);

                                var text_on_images = preparing_post(item.LVal18AFC, data.Last().PClosing);
                                var caption = preparing_post_caption(nullable_is_sell.Value, item.LVal18AFC, data.Last().PClosing);

                                var image = ImageUtil.generate_image_with_text(image_path, text_on_images);
                                if (image != null)
                                {
                                    string image_path_saved = _imageService.save_image(image, saving_image_location.post_image);

                                    if (!image_path_saved.IsNullOrEmptyOrWhiteSpace())
                                    {
                                        post(user_id, secret_key, caption, image_path_saved, nullable_is_sell.Value, is_diverge, item.namad_id);

                                        if (nullable_is_sell.Value)
                                            status = -1;
                                        else
                                            status = 1;
                                    }
                                }
                            }
                            else if (!is_diverge && macd_histogram.HasValue)
                            {
                                string image_path = Config.get_image_path_for_special_macd_post(nullable_is_sell.Value, macd_histogram.Value);

                                var text_on_images = preparing_post(item.LVal18AFC, data.Last().PClosing);
                                var caption = preparing_post_caption(nullable_is_sell.Value, item.LVal18AFC, data.Last().PClosing);

                                var image = ImageUtil.generate_image_with_text(image_path, text_on_images);
                                if (image != null)
                                {
                                    string image_path_saved = _imageService.save_image(image, saving_image_location.post_image);

                                    if (!image_path_saved.IsNullOrEmptyOrWhiteSpace())
                                    {
                                        post(user_id, secret_key, caption, image_path_saved, nullable_is_sell.Value, is_diverge, item.namad_id);

                                        if (nullable_is_sell.Value)
                                            status = -1;
                                        else
                                            status = 1;
                                    }
                                }
                            }
                        }

                        if (macd_line.HasValue && macd_histogram.HasValue && macd_signal.HasValue)
                        {
                            bool updated = false;
                            int cnt = 0;
                            string status_string = "عادی";

                            if (status == -1)
                                status_string = "فروش";
                            else if (status == 1)
                                status_string = "خرید";

                            do
                            {
                                cnt++;
                                updated = _instrumentService.update_instrument_info(user_id, secret_key, item.namad_id, "ENGINE_MACD_Status", status_string);
                            } while (!updated && cnt < 10);

                            updated = false;
                            cnt = 0;
                            do
                            {
                                cnt++;
                                updated = _instrumentService.update_instrument_info(user_id, secret_key, item.namad_id, "ENGINE_MACD_Line", macd_line.Value.ToString());
                            } while (!updated && cnt < 10);

                            updated = false;
                            cnt = 0;
                            do
                            {
                                cnt++;
                                updated = _instrumentService.update_instrument_info(user_id, secret_key, item.namad_id, "ENGINE_MACD_Histogram", macd_histogram.Value.ToString());
                            } while (!updated && cnt < 10);

                            updated = false;
                            cnt = 0;
                            do
                            {
                                cnt++;
                                updated = _instrumentService.update_instrument_info(user_id, secret_key, item.namad_id, "ENGINE_MACD_Signal", macd_signal.Value.ToString());
                            } while (!updated && cnt < 10);
                        }
                        else
                        {
                            bool updated = false;
                            int cnt = 0;
                            do
                            {
                                cnt++;
                                updated = _instrumentService.update_instrument_info(user_id, secret_key, item.namad_id, "ENGINE_MACD_Status", "عادی");
                            } while (!updated && cnt < 10);
                        }

                    }
                    catch (Exception ex)
                    {
                        string ins_code = "";

                        if (item != null && item.InsCode != null)
                            ins_code = item.InsCode;

                        _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} - InsCode : {3}", "MACDServices", "find_and_post__macd_for_all_namads", ex.Message, ins_code));
                    }
                    finally
                    {
                        var run_time = DateTime.Now - start;

                        Thread.Sleep(5000 - Math.Min(5000, run_time.Milliseconds));
                    }
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2}", "MACDServices", "find_and_post__macd_for_all_namads", e.Message));
            }
        }

        private void post(long user_id, string secret_key, string caption, string image_path_saved, bool is_sell, bool is_diverge, long namad_id)
        {
            string macd_status = is_sell ? "Sell_" : "Bought_";
            macd_status += is_diverge ? "Regular" : "Special";

            var data = _userService.users_get_profile_information_fields(user_id, secret_key, namad_id);

            if (data != null && data.data != null)
                foreach (var item in data.data)
                    if (item.key_name.Trim() == "ENGINE_MACD_Last_Status" && item.value != null && item.value.Trim() == macd_status.Trim())
                        return;

            if (user_id > 0 && !secret_key.IsNullOrEmptyOrWhiteSpace())
            {

                if (!image_path_saved.IsNullOrEmptyOrWhiteSpace())
                {
                    string image_path = Config.get_root_path() + image_path_saved;
                    string API_Key = String.Format("{0}-{1}", user_id, secret_key);

                    string file_name = Path.GetFileName(image_path);

                    var file_data = File.ReadAllBytes(image_path);

                    var result = _postService.post("http://Api.moneytoo.ir/api/users/send_post", file_name, file_data,
                        API_Key, caption, namad_id.ToString());

                    int result_update_db = result.Post_id;

                    if (result_update_db <= 0)
                    {
                        _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} - InsCode : {3}", "RSIServices",
                            "post_for_namad", "Can not post for MACD", namad_id));
                    }
                }

                bool updated = false;
                int cnt = 0;
                do
                {
                    cnt++;
                    updated = _instrumentService.update_instrument_info(user_id, secret_key, namad_id, "ENGINE_MACD_Last_Status", macd_status);
                } while (!updated && cnt < 10);
            }
        }

        public string preparing_post_caption(bool is_sell, string namad_name, decimal namad_price)
        {
            string text = "";

            if (is_sell)
                text = "موقعیت فروش " + namad_name + " با قیمت " + namad_price.ToString("#,##0");
            else
                text = "موقعیت خرید " + namad_name + " با قیمت " + namad_price.ToString("#,##0");

            return text;
        }

        public List<PreparingText> preparing_post(string namad_name, decimal namad_price)
        {
            List<PreparingText> result = new List<PreparingText>();

            PreparingText namad_name_label = new PreparingText()
            {
                Title = namad_name,
                Font_Color = "24255a",
                Font_Style = FontStyle.Bold,
                Font_Size = 11,
                Font_Name = "B mitra",
                X = 820,
                Y = 860
            };

            result.Add(namad_name_label);

            PreparingText namad_price_label = new PreparingText()
            {
                Title = namad_price.ToString("#,##0"),
                Font_Color = "24255a",
                Font_Style = FontStyle.Bold,
                Font_Size = 11,
                Font_Name = "B mitra",
                X = 820,
                Y = 950
            };

            result.Add(namad_price_label);

            DateTime d = DateTime.Now;
            PersianCalendar pc = new PersianCalendar();
            string persian_date = string.Format("{0}/{1}/{2}", pc.GetYear(d), pc.GetMonth(d), pc.GetDayOfMonth(d));

            result.Add(new PreparingText()
            {
                Title = persian_date,
                Font_Color = "24255a",
                Font_Style = FontStyle.Bold,
                Font_Size = 8,
                Font_Name = "IRANSans",
                X = 80,
                Y = 970
            });

            return result;
        }

        private (bool?, bool?) find_crossing(List<double?> signal, List<double?> histogram)
        {
            try
            {
                int len = signal.Count;
                double yesterday_status = 0;
                double today_status = 0;
                bool sell = false;
                bool crossing = false;

                if (signal[len - 2].HasValue && histogram[len - 2].HasValue)
                    yesterday_status = signal[len - 2].Value - histogram[len - 2].Value;

                if (signal[len - 1].HasValue && histogram[len - 1].HasValue)
                    today_status = signal[len - 1].Value - histogram[len - 1].Value;

                if (yesterday_status < 0 && today_status > 0)
                {
                    crossing = true;
                    sell = true;
                }
                else if (yesterday_status > 0 && today_status < 0)
                {
                    crossing = true;
                    sell = false;
                }

                return (crossing, sell);
            }
            catch (Exception e)
            {
                return (null, null);
            }
        }

        private bool is_divergent(bool is_sell, List<Instrument_Trade_View_Model> data, List<double?> histogram)
        {
            try
            {
                List<decimal> price_data = new List<decimal>();
                List<decimal> price_min = new List<decimal>();
                List<decimal> price_max = new List<decimal>();
                List<decimal> histogram_min = new List<decimal>();
                List<decimal> histogram_max = new List<decimal>();
                List<double> histogram_data = new List<double>();
                List<decimal> histogram_normal_data = new List<decimal>();

                decimal price_difference = 0;
                decimal histogram_difference = 0;

                foreach (var item in histogram)
                    if (item.HasValue)
                        histogram_data.Add(item.Value);

                for (int i = 1; i < histogram_data.Count; i++)
                    histogram_normal_data.Add((decimal)(histogram_data[i - 1] + histogram_data[i]) / 2);

                if (is_sell)
                    for (int i = 1; i < data.Count; i++)
                        price_data.Add((data[i - 1].PriceMin + data[i].PriceMin) / 2);
                else
                    for (int i = 1; i < data.Count; i++)
                        price_data.Add((data[i - 1].PriceMax + data[i].PriceMax) / 2);

                (price_min, price_max) = find_minimal_maximal(price_data);
                (histogram_min, histogram_max) = find_minimal_maximal(histogram_normal_data);

                if (is_sell)
                {
                    price_difference = price_max[price_max.Count - 2] - price_max[price_max.Count - 1];
                    histogram_difference = histogram_max[histogram_max.Count - 2] - histogram_max[histogram_max.Count - 1];
                }
                else
                {
                    price_difference = price_min[price_min.Count - 2] - price_min[price_min.Count - 1];
                    histogram_difference = histogram_min[histogram_min.Count - 2] - histogram_min[histogram_min.Count - 1];
                }


                if (price_difference > 0 && histogram_difference < 0)
                    return true;

                if (price_difference < 0 && histogram_difference > 0)
                    return true;
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        private (List<decimal>, List<decimal>) find_minimal_maximal(List<decimal> arr)
        {
            int n = arr.Count;
            List<decimal> mx = new List<decimal>();
            List<decimal> mn = new List<decimal>();

            for (int i = 1; i < n - 1; i++)
            {
                if ((arr[i - 1] > arr[i]) &&
                    (arr[i] < arr[i + 1]))
                    mn.Add(arr[i]);

                else if ((arr[i - 1] < arr[i]) &&
                         (arr[i] > arr[i + 1]))
                    mx.Add(arr[i]);
            }

            return (mn, mx);
        }

        public ((List<double?>, List<double?>, List<double?>), List<Instrument_Trade_View_Model>) find_macd_for_namad(long Instrument_Code)
        {
            MACD_Config_View_Model config = load_macd_config_for_namad(Instrument_Code);

            IDataProvider dataProvider = _creator.FactoryMethod(1);

            var ins_trades = dataProvider.get_Trade_From_Date_To_Date(Instrument_Code, DateTime.Now.AddDays(-1 * config.get_day_counts), DateTime.Now).OrderBy(s => s.DEven).ToList();
            ins_trades = ins_trades.Skip(Math.Max(0, ins_trades.Count() - config.calculate_day_counts)).ToList();
            return (calculate_macd(config, ins_trades), ins_trades);
        }

        private (List<double?>, List<double?>, List<double?>) calculate_macd(MACD_Config_View_Model config, List<Instrument_Trade_View_Model> data)
        {
            try
            {
                List<Ohlc> ohlcList = new List<Ohlc>();

                foreach (var item in data)
                {
                    Ohlc ohlc = new Ohlc()
                    {
                        Date = item.DEven,
                        Low = (double)item.PriceMin,
                        High = (double)item.PriceMax,
                        AdjClose = (double)item.PClosing,
                        Volume = (double)item.QTotCap,
                        Close = (double)item.PDrCotVal,
                        Open = (double)item.PriceFirst
                    };
                    ohlcList.Add(ohlc);
                }

                MACD macd = new MACD();
                macd.Load(ohlcList);
                MACDSerie macd_series = macd.Calculate();

                return (macd_series.MACDLine, macd_series.MACDHistogram, macd_series.Signal);
            }
            catch (Exception e)
            {
                return (null, null, null);
            }

        }

        public MACD_Config_View_Model load_macd_config_for_namad(long Instrument_Code)
        {
            return new MACD_Config_View_Model() { calculate_day_counts = 100 };
        }
    }
}