﻿using System;

namespace DataLayer.Models
{
    public class users_get_follow_suggestion_list_Result
    {
        public long ID { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string username { get; set; }
        public Nullable<long> profile_image_t { get; set; }
        public Nullable<int> user_type { get; set; }
    }
}
