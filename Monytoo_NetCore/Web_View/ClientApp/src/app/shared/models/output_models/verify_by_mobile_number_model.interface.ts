export interface verify_by_mobile_number_model {
  user_session_ID: string;
  verification_code: string;
}
