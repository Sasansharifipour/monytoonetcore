﻿
namespace DomainClasses.Users
{
    public class get_profile_image_result_view_model : result_model
    {
        public byte[] image_data { get; set; }

        public string image_extention { get; set; }
    }
}