﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class users_add_item_to_private_basket_input_view_model : get_base_data_model
    {
        public long namad_id { get; set; } = 0;

        public int order_price { get; set; } = 0;

        public int count { get; set; } = 0;

        public long basket_id { get; set; } = 0;
    }
}
