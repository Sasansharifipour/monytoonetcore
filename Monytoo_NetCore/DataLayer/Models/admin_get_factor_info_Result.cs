﻿using System;

namespace DataLayer.Models
{
    public class admin_get_factor_info_Result
    {
        public long ID { get; set; }
        public Nullable<int> price { get; set; }
        public Nullable<int> duration { get; set; }
        public Nullable<long> coupon_id { get; set; }
    }
}
