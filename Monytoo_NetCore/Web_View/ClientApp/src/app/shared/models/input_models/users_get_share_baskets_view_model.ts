import { result_model } from "./result_model.interface";
import { users_get_share_baskets_Result } from "./users_get_share_baskets_Result";

export interface users_get_share_baskets_view_model extends result_model {
  informations: users_get_share_baskets_Result[];
}

