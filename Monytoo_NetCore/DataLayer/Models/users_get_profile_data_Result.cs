﻿using System;

namespace DataLayer.Models
{
    public class users_get_profile_data_Result
    {
        public string username { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public Nullable<double> strength { get; set; }
        public Nullable<int> post_count { get; set; }
        public Nullable<int> follower_count { get; set; }
        public Nullable<int> following_count { get; set; }
        public Nullable<int> user_type { get; set; }
        public Nullable<bool> is_followed { get; set; }
        public string invitation_code { get; set; }
        public Nullable<long> profile_image_t { get; set; }
    }
}
