﻿using CommonCode;
using Microsoft.AspNetCore.Http;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using static CommonCode.CommonCodes;

namespace Services
{
    public interface IImageService
    {
        string save_image(IFormFile file, saving_image_location location_type);

        string save_image(string file_url, saving_image_location location_type);

        string save_image(string file_url, saving_image_location location_type, int max_width_or_height);

        string save_image(Bitmap file, saving_image_location location_type);

        (byte[], string) load_image(string file_path);
    }

    public class ImageService : IImageService
    {
        public string save_image(IFormFile file, saving_image_location location_type)
        {
            try
            {
                string image_path = Config.image_saving_path(location_type) + "\\" + DateTime.Now.ToString("yy-MM-dd") + "\\";

                string full_path = Config.get_root_path() + image_path;

                if (!Directory.Exists(full_path))
                    Directory.CreateDirectory(full_path);

                string file_name = Config.get_unique_name(full_path, (new FileInfo(file.FileName)).Extension);
                
                using (Stream fileStream = new FileStream(full_path + file_name, FileMode.Create))
                    file.CopyTo(fileStream);

                return image_path + file_name;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string save_image(Bitmap file, saving_image_location location_type)
        {
            try
            {
                if (file == null)
                    return "";

                string image_path = Config.image_saving_path(location_type) + "\\" + DateTime.Now.ToString("yy-MM-dd") + "\\";

                string full_path = Config.get_root_path() + image_path;

                if (!Directory.Exists(full_path))
                    Directory.CreateDirectory(full_path);

                string file_name = Config.get_unique_name(full_path, ".jpg");

                file.Save(full_path + file_name);
                return image_path + file_name;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string save_image(string file_url, saving_image_location location_type, int max_width_or_height)
        {
            try
            {
                string image_path = Config.image_saving_path(location_type) + "\\" + DateTime.Now.ToString("yy-MM-dd") + "\\";

                string full_path = Config.get_root_path() + image_path;

                if (!Directory.Exists(full_path))
                    Directory.CreateDirectory(full_path);

                string file_name = Config.get_unique_name(full_path, Path.GetExtension(file_url));

                Download_And_Save_Image(file_url, full_path + file_name);

                var image = Image.FromFile(full_path + file_name);

                if (image.Width < max_width_or_height && image.Height < max_width_or_height)
                {
                    Size size = new Size();
                    if (image.Width < image.Height)
                    {
                        size.Height = 512;
                        size.Width = (image.Width * 512) / image.Height;
                    }
                    else
                    {
                        size.Width = 512;
                        size.Height = (image.Height * 512) / image.Width;
                    }

                    var new_image = new Bitmap(image, size);

                    string resized_file_name = Config.get_unique_name(full_path, Path.GetExtension(file_url));
                    new_image.Save(full_path + resized_file_name);
                    new_image.Dispose();
                    image.Dispose();

                    File.Delete(full_path + file_name);

                    return image_path + resized_file_name;
                }

                return image_path + file_name;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string save_image(string file_url, saving_image_location location_type)
        {
            try
            {
                string image_path = Config.image_saving_path(location_type) + "\\" + DateTime.Now.ToString("yy-MM-dd") + "\\";

                string full_path = Config.get_root_path() + image_path;

                if (!Directory.Exists(full_path))
                    Directory.CreateDirectory(full_path);

                string file_name = Config.get_unique_name(full_path, Path.GetExtension(file_url));

                Download_And_Save_Image(file_url, full_path + file_name);
                return image_path + file_name;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public void Download_And_Save_ImageAsync(string image_url, string image_path)
        {
            using (WebClient _wc = new WebClient())
                _wc.DownloadFileAsync(new Uri(image_url), image_path);
        }

        public void Download_And_Save_Image(string image_url, string image_path)
        {
            using (WebClient _wc = new WebClient())
                _wc.DownloadFile(new Uri(image_url), image_path);
        }

        public (byte[], string) load_image(string file_path)
        {
            string image_path = Config.get_root_path() + file_path;
            var image_data = File.ReadAllBytes(image_path);
            FileInfo fileInfo = new FileInfo(image_path);
            var image_extention = fileInfo.Extension;

            return (image_data, image_extention);
        }
    }
}