﻿using System;
using System.Text;

namespace Services
{
    public interface ICodeGeneratorService
    {
        string generate_register_code();

        string generate_secret_key();

        string generate_invitation_code();

    }
    public class CodeGeneratorService : ICodeGeneratorService
    {
        private readonly Random _random = new Random();

        public string generate_register_code()
        {
            return _random.Next(100000, 999999).ToString();
        }

        public string generate_secret_key()
        {
            int size = 50;
            var builder = new StringBuilder(size);

            char offset = 'a';
            const int lettersOffset = 26;

            for (var i = 0; i < size; i++)
            {
                int rnd = _random.Next(2);

                offset = (rnd == 0) ? 'a' : 'A';

                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return builder.ToString();
        }

        public string generate_invitation_code()
        {
            int size = 4;
            var builder = new StringBuilder(size);

            char offset = 'a';
            const int lettersOffset = 26;

            for (var i = 0; i < size; i++)
            {
                int rnd = _random.Next(2);

                offset = (rnd == 0) ? 'a' : 'A';

                var @char = (char)_random.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return builder.ToString();
        }
    }
}