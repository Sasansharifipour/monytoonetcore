﻿using DomainClasses.Input_API_View_Model;
using DomainClasses.Output_API_View_Model;
using DomainClasses.Post;
using DomainClasses.Users;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using static CommonCode.CommonCodes;
using CommonCode;
using DomainClasses.Common;
using System.Dynamic;
using System.Data.Entity.Infrastructure;
using System.Net.Http;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Data;

namespace Services
{
    public interface IuserServices
    {
        privacy_result_model get_system_messages();

        privacy_result_model get_terms_of_conditions();

        privacy_result_model get_privacy_policy();

        check_user_name_available_result_model check_username_availability(string username);

        register_result_model register(register_model model);

        verify_by_mobile_number_result_model users_verify_user_session(verify_by_mobile_number_model model);

        update_profile_data_result_model update_profile_data(update_profile_data_model model);

        List<update_profile_data_result_model> update_information_field_value(update_information_field_value_model model);

        get_profile_data_result_model get_profile_data(get_profile_data_model model);

        update_profile_data_result_model update_profile_image(update_profile_image_view_model model);

        get_profile_image_result_view_model get_profile_image(get_profile_data_model model);

        send_post_result_view_model send_post(send_post_view_model model);

        send_post_result_view_model send_post(long user_id, string secret_key, string caption, string img_path, long? from_user_id);

        get_user_profile_information_result_view_model users_get_profile_information_fields(long user_id, string secret_key, long? user_profile_id);

        update_profile_data_result_model follow_user(get_profile_data_model model);

        update_profile_data_result_model unfollow_user(get_profile_data_model model);

        user_get_profile_post_result_view_model users_get_profile_posts(get_user_follow_view_model model);

        user_get_profile_post_result_view_model users_search(search_profile_data_model model);

        user_get_profile_post_result_view_model users_home_posts(get_post_by_index_view_model model);

        user_get_profile_post_result_view_model users_explore_posts(get_post_by_index_view_model model);

        get_profile_image_result_view_model users_get_image(get_post_image_view_model model);

        user_get_profile_post_result_view_model users_get_single_post(get_post_view_model model);

        user_get_profile_post_result_view_model users_like_post(get_post_view_model model);

        user_get_profile_post_result_view_model users_unlike_post(get_post_view_model model);

        user_get_profile_post_result_view_model users_get_profile_followers(get_user_follow_view_model model);

        user_get_profile_post_result_view_model users_get_profile_followings(get_user_follow_view_model model);

        user_get_profile_post_result_view_model users_get_likes_list(get_post_view_model model);

        update_profile_data_result_model users_edit_post(get_post_view_model model);

        delete_info_result_view_model users_delete_post(get_post_view_model model);

        delete_info_result_view_model users_delete_profile_picture(get_base_data_model model);

        user_get_profile_post_result_view_model users_get_price_packages(get_base_data_model model);

        user_get_profile_post_result_view_model users_create_factor(users_create_factor_API_View_Model model);

        user_get_profile_post_result_view_model users_get_notification_list(user_get_notification_API_View_Model model);

        update_profile_data_result_model users_seen_notification_list(get_base_data_model model);

        add_comment_result_view_model users_send_comment(UserSendCommentViewModel model);

        delete_info_result_view_model users_delete_comment(CommentBaseViewModel model);

        doing_job_result_view_model users_report_post(PostBaseViewModel model);

        user_get_profile_post_result_view_model users_get_comment_reply_list(UserGetCommentReplyListViewModel model);

        user_get_profile_post_result_view_model users_get_post_comments_list(UserGetPostCommentsListViewModel model);

        check_version_result_view_model users_check_version(get_version_post_input_view_model model);

        get_info_post_result_view_model users_get_shared_accounts(get_base_data_model model);

        get_info_post_result_view_model users_get_follow_suggestion_list(get_base_data_model model);

        doing_job_result_view_model users_like_comment(CommentBaseViewModel model);

        doing_job_result_view_model users_unlike_comment(CommentBaseViewModel model);

        get_info_post_result_view_model users_get_comment_likes_list(UserGetCommentReplyListViewModel model);

        doing_job_result_view_model users_create_basket(user_create_basket_input_view_model model);

        doing_job_result_view_model users_create_basket_order(user_create_basket_order_input_view_model model);

        user_get_profile_post_result_view_model users_search_stock(search_profile_data_model model);

        doing_job_result_view_model users_basket_cancel_order(users_basket_cancel_order_input_view_model model);

        user_get_profile_post_result_view_model users_get_basket_order_history(users_get_basket_order_history_input_view_model model);

        user_get_profile_post_result_view_model users_get_basket_data(users_get_basket_order_history_input_view_model model);

        user_get_profile_post_result_view_model users_get_profile_baskets(users_get_profile_baskets_view_model model);

        user_get_profile_post_result_view_model users_get_share_baskets(users_get_profile_baskets_view_model model);

        doing_job_return_info_result_view_model users_create_factor_wallet(users_create_factor_wallet_input_view_model model);

        doing_job_result_view_model users_add_item_to_private_basket(users_add_item_to_private_basket_input_view_model model);

        user_get_profile_post_result_view_model users_get_Private_baskets(users_get_profile_baskets_view_model model);

        user_get_profile_post_result_view_model users_get_my_balance(get_base_data_model model);

        user_get_profile_post_result_view_model users_share_my_basket(users_share_my_basket_view_model model);

        doing_job_result_view_model users_delete_basket_share_package(basket_share_package_input_view_model model);

        user_get_profile_post_result_view_model users_get_basket_share_packages(get_basket_share_package_input_view_model model);

        doing_job_result_view_model users_buy_basket_share_packages(basket_share_package_input_view_model model);

        doing_job_result_view_model users_set_basket_item_price(user_set_basket_item_price_view_model model);

        void user_bahamta_confirm_payment(long user_id, long factor_id);

        doing_job_result_view_model user_cafebazar_confirm_payment(cafebazar_confirm_view_model model);

        bool Save_user_infoAsync(string id, string username, string name, string family);
    }

    public class userServices : IuserServices
    {
        private IDbContextFactory<connectionString> _contextFactory;
        private IVerificationService _verificationService;
        private ICodeGeneratorService _codeGeneratorService;
        private IHashingService _hashingService;
        private IImageService _imageService;
        private ILogger _logger;

        public userServices(IVerificationService verificationService,
            ICodeGeneratorService codeGeneratorService, IHashingService hashingService,
            IImageService imageService, ILogger logger,
            IDbContextFactory<connectionString> contextFactory)
        {
            _contextFactory = contextFactory;
            _verificationService = verificationService;
            _codeGeneratorService = codeGeneratorService;
            _hashingService = hashingService;
            _imageService = imageService;
            _logger = logger;
        }

        public privacy_result_model get_system_messages()
        {
            privacy_result_model result_model = new privacy_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    var result = _db.users_get_system_messages().FirstOrDefault();
                    if (result == null)
                    {
                        result_model.text = "";
                        return result_model;
                    }

                    if (result.first_popup == null)
                    {
                        result_model.text = "";
                        return result_model;
                    }

                    result_model.text = result.first_popup;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "privacy_result_model", e.Message));
                result_model.text = "";
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public privacy_result_model get_terms_of_conditions()
        {
            privacy_result_model result_model = new privacy_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    var result = _db.users_get_system_messages().FirstOrDefault();
                    if (result == null)
                    {
                        result_model.text = "";
                        return result_model;
                    }

                    if (result.terms_of_condition == null)
                    {
                        result_model.text = "";
                        return result_model;
                    }

                    result_model.text = result.terms_of_condition;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "privacy_result_model", e.Message));
                result_model.text = "";
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public privacy_result_model get_privacy_policy()
        {
            privacy_result_model result_model = new privacy_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    var result = _db.users_get_system_messages().FirstOrDefault();
                    if (result == null)
                    {
                        result_model.text = "";
                        return result_model;
                    }

                    if (result.privacy_policy == null)
                    {
                        result_model.text = "";
                        return result_model;
                    }

                    result_model.text = result.privacy_policy;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "get_privacy_policy", e.Message));
                result_model.text = "";
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public check_user_name_available_result_model check_username_availability(string username)
        {
            check_user_name_available_result_model result_model = new check_user_name_available_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output };
                    var results = _db.users_check_username_availability(0, username, res).FirstOrDefault();

                    result_model.is_available = Convert.ToInt32(res.Value);

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "check_username_availability", e.Message));
                result_model.is_available = 0;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public register_result_model register(register_model model)
        {
            register_result_model register_Result_Model = new register_result_model();

            try
            {
                Regex rgx = new Regex(@"^[a-zA-Z0-9.-]+$");

                if (model.username != null)
                    if (!rgx.IsMatch(model.username))
                        return new register_result_model()
                        {
                            Error_Id = error_enum.Username_is_not_available.GetCode(),
                            Error_Text = error_enum.Username_is_not_available.GetDescription()
                        };

                string verification_code = _codeGeneratorService.generate_register_code();
                string invitation_code = _codeGeneratorService.generate_invitation_code();

                long? inviter_id = null;
                string inviter_code = "";

                (inviter_id, inviter_code) = CommonCodes
                    .convert_inviter_id_code_To_inviter_id_and_inviter_code(model.invitation_code);

                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    SqlParameter is_new_user = new SqlParameter("is_new_user", 0) { Direction = ParameterDirection.Output };

                    var results = _db.users_register(model.mobile_number, verification_code, model.username, model.name, model.lastname, inviter_id, inviter_code,
                        model.firebase_id, invitation_code, 1, is_new_user, res);
                    int result_insert_to_db = -1;

                    int.TryParse(res.Value.ToString(), out result_insert_to_db);

                    bool is_new_user_result = false;
                    bool.TryParse(is_new_user.Value.ToString(), out is_new_user_result);

                    if (result_insert_to_db < 0)
                    {
                        register_Result_Model.Error_Id = result_insert_to_db.ToString();
                        register_Result_Model.Error_Text = ((error_enum)result_insert_to_db).GetDescription();

                        return register_Result_Model;
                    }
                    else if (result_insert_to_db > 0)
                    {
                        register_Result_Model.user_session_ID = result_insert_to_db;
                        register_Result_Model.is_new_user = is_new_user_result;
                    }

                    bool send_code = _verificationService.send_verification_code(model.mobile_number, verification_code);

                    if (!send_code)
                    {
                        register_Result_Model.Error_Id = error_enum.Sending_verification_code_failed.GetCode();
                        register_Result_Model.Error_Text = error_enum.Sending_verification_code_failed.GetDescription();
                    }
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "register", e.Message));
                register_Result_Model.Error_Id = error_enum.unknown_error.GetCode();
                register_Result_Model.Error_Text = error_enum.unknown_error.GetDescription();
            }

            return register_Result_Model;
        }

        public verify_by_mobile_number_result_model users_verify_user_session(verify_by_mobile_number_model model)
        {
            verify_by_mobile_number_result_model result_model = new verify_by_mobile_number_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string secret_key = _codeGeneratorService.generate_secret_key();

                    long user_session_id = 0;
                    long.TryParse(model.user_session_ID, out user_session_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output };
                    SqlParameter user_id = new SqlParameter("user_id", 0) { Direction = ParameterDirection.Output };

                    _db.users_verify_user_session(user_session_id, model.verification_code, secret_key, user_id, res);

                    int result_verify = Convert.ToInt32(res.Value);

                    if (result_verify < 0)
                    {
                        result_model.API_KEY = "";
                        result_model.Error_Id = result_verify.ToString();
                        result_model.Error_Text = ((error_enum)result_verify).GetDescription();

                        return result_model;
                    }
                    else if (result_verify == 0)
                    {
                        result_model.API_KEY = _hashingService.hash_string(Convert.ToInt32(user_session_id) + "-" + secret_key);
                        int usr_id = 0;
                        int.TryParse(user_id.Value.ToString(), out usr_id);
                        result_model.user_id = usr_id;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "verify_By_Mobile_Number", e.Message));
                result_model.API_KEY = "";
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public update_profile_data_result_model update_profile_data(update_profile_data_model model)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output };
                    var results = _db.users_update_profile_data(user_id, secret_key, model.name, model.lastname, model.username, res);
                    int result_update_db = Convert.ToInt32(res.Value);

                    if (result_update_db < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = result_update_db.ToString();
                        result_model.Error_Text = ((error_enum)result_update_db).GetDescription();

                        return result_model;
                    }
                    else if (result_update_db == 0)
                    {
                        result_model.is_updated = true;
                        return result_model;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "update_profile_data", e.Message));
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
            }

            return result_model;
        }

        public get_profile_data_result_model get_profile_data(get_profile_data_model model)
        {
            get_profile_data_result_model result_model = new get_profile_data_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    long temp_user_profile_id = 0;
                    long.TryParse(model.user_profile_id, out temp_user_profile_id);

                    long? user_profile_id = temp_user_profile_id;

                    if (user_id == temp_user_profile_id)
                        user_profile_id = null;

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    users_get_profile_data_Result results = _db.users_get_profile_data(user_id, secret_key, user_profile_id, res).FirstOrDefault();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.data = null;
                        result_model.Error_Id = (result_get_profile_info).ToString();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        get_user_profile_information_result_view_model information_fileds = users_get_profile_information_fields(user_id,
                            secret_key, user_profile_id);

                        if (information_fileds.Error_Id == "-1" || information_fileds.Error_Id == "-2")
                        {
                            result_model.data = null;
                            result_model.Error_Id = information_fileds.Error_Id;
                            result_model.Error_Text = information_fileds.Error_Text;
                        }
                        else
                        {
                            user_profile_data_view_model data = new user_profile_data_view_model()
                            {
                                follower_count = results.follower_count,
                                following_count = results.following_count,
                                lastname = results.lastname,
                                profile_image_t = results.profile_image_t,
                                name = results.name,
                                invitation_code = results.invitation_code,
                                is_followed = results.is_followed,
                                post_count = results.post_count,
                                strength = results.strength,
                                username = results.username,
                                user_type = results.user_type
                            };

                            if (information_fileds.Error_Id == "0")
                                data.information_fields = information_fileds.data;
                            else
                                data.information_fields = null;

                            result_model.data = data;
                        }
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "get_profile_data", e.Message));
                result_model.data = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public get_user_profile_information_result_view_model users_get_profile_information_fields(long user_id, string secret_key, long? user_profile_id)
        {
            get_user_profile_information_result_view_model result_model = new get_user_profile_information_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_profile_information_fields(user_id, secret_key, user_profile_id, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.data = null;
                        result_model.Error_Id = (result_get_profile_info).ToString();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        List<user_profile_information_view_model> data = new List<user_profile_information_view_model>();

                        foreach (var item in results)
                            data.Add(new user_profile_information_view_model() { key_name = item.key_name, name = item.name, value = item.value, parent_url = item.parent_url });

                        result_model.data = data;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_profile_information_fields", e.Message));
                result_model.data = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public List<update_profile_data_result_model> update_information_field_value(update_information_field_value_model model)
        {
            List<update_profile_data_result_model> result_model = new List<update_profile_data_result_model>();

            try
            {
                string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                string User_id = "";
                string secret_key = "";

                (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                long user_id = 0;
                long.TryParse(User_id, out user_id);

                foreach (var item in model.information_fields)
                    result_model.Add(update_information_field_value(user_id, secret_key, item.field_key, item.field_value));
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "update_information_field_value", e.Message));
                result_model.Add(new update_profile_data_result_model()
                {
                    is_updated = false,
                    Error_Id = error_enum.unknown_error.GetCode(),
                    Error_Text = error_enum.unknown_error.GetDescription()
                });

                return result_model;
            }

            return result_model;
        }

        private update_profile_data_result_model update_information_field_value(long user_id, string secret_key, string field_key, string field_value)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model() { field_key = field_key };
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_update_information_field_value(user_id, secret_key, field_key, field_value, res);
                    int result_update_db = Convert.ToInt32(res.Value);

                    if (result_update_db < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = result_update_db.ToString();
                        result_model.Error_Text = ((error_enum)result_update_db).GetDescription();

                        return result_model;
                    }
                    else if (result_update_db == 0)
                    {
                        result_model.is_updated = true;
                        return result_model;
                    }
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "update_information_field_value", e.Message));
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription(); ;
            }

            return result_model;
        }

        public update_profile_data_result_model update_profile_image(update_profile_image_view_model model)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    string img_path = _imageService.save_image(model.image, saving_image_location.profile_image);

                    if (img_path == "")
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = error_enum.unknown_error.GetCode();
                        result_model.Error_Text = error_enum.unknown_error.GetDescription();
                        return result_model;
                    }

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_update_profile_picture(user_id, secret_key, img_path, res);
                    int result_update_db = Convert.ToInt32(res.Value);

                    if (result_update_db < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = result_update_db.ToString();
                        result_model.Error_Text = ((error_enum)result_update_db).GetDescription();

                    }
                    else if (result_update_db == 0)
                    {
                        result_model.is_updated = true;
                    }
                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "update_profile_image", e.Message));
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }

        }

        public get_profile_image_result_view_model get_profile_image(get_profile_data_model model)
        {
            get_profile_image_result_view_model result_model = new get_profile_image_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    long temp_user_profile_id = 0;
                    long.TryParse(model.user_profile_id, out temp_user_profile_id);

                    long? user_profile_id = temp_user_profile_id;

                    SqlParameter profile_image_address = new SqlParameter("profile_image_address", SqlDbType.NVarChar, 250) { Direction = ParameterDirection.Output };
                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_profile_image(user_id, secret_key, user_profile_id, res, profile_image_address);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.image_data = null;
                        result_model.image_extention = "";
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        byte[] image_data;
                        string image_extention = "";

                        (image_data, image_extention) = _imageService.load_image(profile_image_address.Value.ToString());
                        result_model.image_data = image_data;
                        result_model.image_extention = image_extention;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "get_profile_image", e.Message));
                result_model.image_data = null;
                result_model.image_extention = "";
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public update_profile_data_result_model follow_user(get_profile_data_model model)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    long temp_user_profile_id = 0;
                    long.TryParse(model.user_profile_id, out temp_user_profile_id);

                    long? user_profile_id = temp_user_profile_id;

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output };
                    var results = _db.users_follow(user_id, secret_key, user_profile_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode(); ;
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_updated = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "follow_user", e.Message));
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public update_profile_data_result_model unfollow_user(get_profile_data_model model)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    long temp_user_profile_id = 0;
                    long.TryParse(model.user_profile_id, out temp_user_profile_id);

                    long? user_profile_id = temp_user_profile_id;

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_unfollow(user_id, secret_key, user_profile_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_updated = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "unfollow_user", e.Message));
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public send_post_result_view_model send_post(send_post_view_model model)
        {
            send_post_result_view_model result_model = new send_post_result_view_model();
            string img_path = "";
            try
            {

                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    img_path = _imageService.save_image(model.image, saving_image_location.post_image);

                    if (img_path == "")
                    {
                        result_model.Post_id = 0;
                        result_model.Error_Id = error_enum.unknown_error.GetCode();
                        result_model.Error_Text = error_enum.unknown_error.GetDescription();
                        return result_model;
                    }

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_send_post(user_id, secret_key, img_path, model.caption, model.from_user_id, res);
                    int result_update_db = Convert.ToInt32(res.Value);

                    if (result_update_db < 0)
                    {
                        File.Delete(Config.get_root_path() + img_path);
                        result_model.Post_id = 0;
                        result_model.Error_Id = result_update_db.ToString();
                        result_model.Error_Text = ((error_enum)result_update_db).GetDescription();

                    }
                    else if (result_update_db > 0)
                    {
                        result_model.Post_id = result_update_db;
                    }
                    return result_model;
                }
            }
            catch (Exception e)
            {
                if (!img_path.IsNullOrEmptyOrWhiteSpace())
                    File.Delete(Config.get_root_path() + img_path);

                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "send_post", e.Message));
                result_model.Post_id = 0;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }

        }

        public send_post_result_view_model send_post(long user_id, string secret_key, string caption, string img_path, long? from_user_id)
        {
            send_post_result_view_model result_model = new send_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    if (img_path == "")
                    {
                        result_model.Post_id = 0;
                        result_model.Error_Id = error_enum.unknown_error.GetCode();
                        result_model.Error_Text = error_enum.unknown_error.GetDescription();
                        return result_model;
                    }

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_send_post(user_id, secret_key, img_path, caption, from_user_id, res);
                    int result_update_db = Convert.ToInt32(res.Value);

                    if (result_update_db < 0)
                    {
                        result_model.Post_id = 0;
                        result_model.Error_Id = result_update_db.ToString();
                        result_model.Error_Text = ((error_enum)result_update_db).GetDescription();

                    }
                    else if (result_update_db > 0)
                    {
                        result_model.Post_id = result_update_db;
                    }
                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "send_post", e.Message));
                result_model.Post_id = 0;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }

        }

        public user_get_profile_post_result_view_model users_get_profile_posts(get_user_follow_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    long temp_user_profile_id = 0;
                    long.TryParse(model.user_profile_id, out temp_user_profile_id);

                    long? user_profile_id = temp_user_profile_id;

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_profile_posts(user_id, secret_key, user_profile_id, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_profile_posts", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_search(search_profile_data_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_search(user_id, secret_key, model.search_str, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_home_posts(get_post_by_index_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_home_posts(user_id, secret_key, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_home_posts", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_explore_posts(get_post_by_index_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_Explore_posts(user_id, secret_key, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_explore_posts", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public get_profile_image_result_view_model users_get_image(get_post_image_view_model model)
        {
            get_profile_image_result_view_model result_model = new get_profile_image_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter image_address = new SqlParameter("image_address", typeof(global::System.String));
                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_image(user_id, secret_key, model.post_id, model.image_id, image_address, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.image_data = null;
                        result_model.image_extention = "";
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        byte[] image_data;
                        string image_extention = "";

                        (image_data, image_extention) = _imageService.load_image(image_address.Value.ToString());
                        result_model.image_data = image_data;
                        result_model.image_extention = image_extention;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_image", e.Message));
                result_model.image_data = null;
                result_model.image_extention = "";
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_single_post(get_post_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_single_post(user_id, secret_key, model.post_id, res).FirstOrDefault();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_single_post", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_like_post(get_post_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_like_post(user_id, secret_key, model.post_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_like_post", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_unlike_post(get_post_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_unlike_post(user_id, secret_key, model.post_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_unlike_post", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_profile_followers(get_user_follow_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    long temp_user_profile_id = 0;
                    long.TryParse(model.user_profile_id, out temp_user_profile_id);

                    long? user_profile_id = temp_user_profile_id;

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_profile_followers(user_id, secret_key, user_profile_id, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_profile_followers", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_profile_followings(get_user_follow_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    long temp_user_profile_id = 0;
                    long.TryParse(model.user_profile_id, out temp_user_profile_id);

                    long? user_profile_id = temp_user_profile_id;

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_profile_followings(user_id, secret_key, user_profile_id, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_profile_followings", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_likes_list(get_post_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_post_likes_list(user_id, secret_key, model.post_id, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_likes_list", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public update_profile_data_result_model users_edit_post(get_post_view_model model)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_edit_post(user_id, secret_key, model.post_id, model.caption, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_updated = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_edit_post", e.Message));
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public delete_info_result_view_model users_delete_post(get_post_view_model model)
        {
            delete_info_result_view_model result_model = new delete_info_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_delete_post(user_id, secret_key, model.post_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_deleted = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_deleted = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_post", e.Message));
                result_model.is_deleted = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public delete_info_result_view_model users_delete_profile_picture(get_base_data_model model)
        {
            delete_info_result_view_model result_model = new delete_info_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_delete_profile_picture(user_id, secret_key, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_deleted = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_deleted = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_profile_picture", e.Message));
                result_model.is_deleted = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_price_packages(get_base_data_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_price_packages(user_id, secret_key, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_price_packages", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_create_factor(users_create_factor_API_View_Model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    SqlParameter price = new SqlParameter("price", 0);

                    var result = _db.users_create_factor(user_id, secret_key, model.package_id, model.coupon_id, res, price);
                    int result_get_profile_info = Convert.ToInt32(res.Value);
                    int output_price = Convert.ToInt32(price.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        string payment_link = Payment_bahamta.create_request(user_id.ToString(), result_get_profile_info.ToString(), output_price.ToString());

                        result_model.Informations = new { Factor_Id = result_get_profile_info, Payment_Link = payment_link };
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_create_factor", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_notification_list(user_get_notification_API_View_Model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_get_notification_list(user_id, secret_key, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = result;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_notification_list", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public update_profile_data_result_model users_seen_notification_list(get_base_data_model model)
        {
            update_profile_data_result_model result_model = new update_profile_data_result_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_seen_notification_list(user_id, secret_key, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_updated = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_updated = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_seen_notification_list", e.Message));
                result_model.is_updated = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public add_comment_result_view_model users_send_comment(UserSendCommentViewModel model)
        {
            add_comment_result_view_model result_model = new add_comment_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_send_comment(user_id, secret_key, model.Post_id, model.Text, model.Reply_to_comment_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_added = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_added = true;
                        result_model.Information = result_get_profile_info;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_send_comment", e.Message));
                result_model.is_added = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public delete_info_result_view_model users_delete_comment(CommentBaseViewModel model)
        {
            delete_info_result_view_model result_model = new delete_info_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_delete_comment(user_id, secret_key, model.Comment_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_deleted = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_deleted = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_comment", e.Message));
                result_model.is_deleted = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_report_post(PostBaseViewModel model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_report_post(user_id, secret_key, model.Post_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_report_post", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_comment_reply_list(UserGetCommentReplyListViewModel model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_get_comment_reply_list(user_id, secret_key, model.Comment_id, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = result;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_comment_reply_list", e.Message));
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_post_comments_list(UserGetPostCommentsListViewModel model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_get_post_comments_list(user_id, secret_key, model.Post_id, model.index, model.count, res).ToList().Select(
                        s => new
                        {
                            comment_id = s.comment_id,
                            r_number = s.r_number,
                            reply_count = s.reply_count,
                            comment_user_type_id = s.comment_user_type_id,
                            comment_user_id = s.comment_user_id,
                            comment_username = s.comment_username,
                            comment_time = s.comment_time,
                            comment_text = s.comment_text,
                            comment_is_liked = s.comment_is_liked,
                            comment_like_count = s.comment_like_count,
                            profile_image_t = s.comment_user_profile_image_t,
                            reply = new
                            {
                                reply_id = s.reply_id,
                                reply_is_liked = s.reply_is_liked,
                                reply_like_count = s.reply_like_count,
                                reply_text = s.reply_text,
                                reply_user_profile_image_t = s.reply_user_profile_image_t,
                                reply_time = s.reply_time,
                                reply_username = s.reply_username,
                                replay_user_id = s.replay_user_id,
                                reply_user_type_id = s.reply_user_type_id
                            }
                        }).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = result;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_post_comments_list", e.Message));
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public check_version_result_view_model users_check_version(get_version_post_input_view_model model)
        {
            check_version_result_view_model result_model = new check_version_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    var result = _db.users_check_version(model.Version_ID).FirstOrDefault();

                    if (result == null)
                    {
                        result_model.Error_Id = (error_enum.unknown_error).GetCode();
                        result_model.Error_Text = (error_enum.unknown_error).GetDescription();
                    }
                    else
                    {
                        result_model.status_id = result.status_id;
                        result_model.message = result.message;
                        result_model.update_url = result.update_url;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2}", "userServices", "users_check_version", e.Message));
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public get_info_post_result_view_model users_get_shared_accounts(get_base_data_model model)
        {
            get_info_post_result_view_model result_model = new get_info_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_get_shared_accounts(user_id, secret_key, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = result;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_shared_accounts", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public get_info_post_result_view_model users_get_follow_suggestion_list(get_base_data_model model)
        {
            get_info_post_result_view_model result_model = new get_info_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_get_follow_suggestion_list(user_id, secret_key, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        List<object> all_data = new List<object>();

                        foreach (var item in result)
                        {
                            dynamic data = item.ToDynamic();

                            try
                            {
                                var result_3_posts = _db.users_get_last_3_posts(user_id, secret_key, item.ID, res).ToList();
                                int result_3_posts_info = Convert.ToInt32(res.Value);

                                if (result_3_posts_info < 0)
                                    AddProperty(data, "posts", null);
                                else
                                    AddProperty(data, "posts", result_3_posts.Select(s => new { s.image_id, s.post_id }));
                            }
                            catch (Exception ex)
                            {
                                AddProperty(data, "posts", null);
                            }
                            finally
                            {
                                all_data.Add(data);
                            }
                        }

                        result_model.Informations = all_data;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_shared_accounts", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        private void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            var exDict = expando as IDictionary<string, object>;
            if (exDict.ContainsKey(propertyName))
                exDict[propertyName] = propertyValue;
            else
                exDict.Add(propertyName, propertyValue);
        }

        public void user_bahamta_confirm_payment(long user_id, long factor_id)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    int price = 0;

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.admin_get_factor_info(factor_id).ToList();

                    if (result != null && result.Count() > 0)
                        int.TryParse(result.First().price.ToString(), out price);

                    var result_confirm = Payment_bahamta.confirm_payment(user_id, factor_id, price);

                    if (result_confirm != null && Convert.ToBoolean(result_confirm.ok))
                    {
                        try
                        {
                            _db.admin_Confirm_payment_factor((int)user_id, factor_id, result_confirm.result.state, result_confirm.result.total, result_confirm.result.wage
                                , result_confirm.result.gateway, result_confirm.result.terminal, result_confirm.result.pay_ref, result_confirm.result.pay_trace
                                , result_confirm.result.pay_pan, result_confirm.result.pay_cid, result_confirm.result.pay_time, res);

                            int result_insert = Convert.ToInt32(res.Value);

                            if (result_insert < 0)
                                log_factor("factor_id : " + factor_id.ToString() + result_confirm.result.To_String());
                        }
                        catch (Exception ex)
                        {
                            log_factor("factor_id : " + factor_id.ToString() + result_confirm.result.To_String());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "user_confirm_payment", e.Message));

            }

        }

        public doing_job_result_view_model user_cafebazar_confirm_payment(cafebazar_confirm_view_model model)
        {
            try
            {
                doing_job_result_view_model result_model = new doing_job_result_view_model()
                {
                    is_done = false,
                    Error_Id = error_enum.unknown_error.GetCode(),
                    Error_Text = error_enum.unknown_error.GetDescription()
                };

                if (model.payment_gateway_id == 1)
                    return result_model;

                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    SqlParameter price = new SqlParameter("price", 0);
                    SqlParameter i_data = new SqlParameter("i_data", typeof(global::System.String));

                    var result = _db.admin_get_factor_wallet_data(model.factor_id, model.payment_gateway_id, price, i_data);

                    if (price == null || (int)price.Value < 0)
                        return result_model;

                    string cafebazar_payment_confirm_url = string.Format
                        ("https://pardakht.cafebazaar.ir/devapi/v2/api/validate/{0}/inapp/{1}/purchases/{2}/", model.package_name,
                        model.product_id, model.purchase_token);

                    cafebazar_request_result_view_model request_result = new cafebazar_request_result_view_model() { developerPayload = i_data.Value.ToString() };

                    using (var httpClient = new HttpClient())
                    {
                        httpClient.DefaultRequestHeaders.Add("Authorization",
                            "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJleHAiOjE2Mjg1NjQyODUsInVzZXJfaWQiOjE5MjAyNzU3LCJzY29wZXMiOlsiYW5kcm9pZHB1Ymxpc2hlciJdfQ.AQ2_nAjciXsr3wDXXO8XhYATz9CmcLXbT7letDQPeeHzy3HFcIz8z3LoIM0M4tnE9l45j1A4uAy-D88D8PBbSiA9Umjd8PS0tSL70h6bFFUvlHPuJ4spWj5-76BOg9r_pqnHRe0VKxN3F9G-6HrmgAfz8U-aG0qBWD4x7yk-hu3KoXntKcMTaN37xKhcv0p6U6ufhC1PhKE6tihN6yFQ19uSPcN0GJasQihkRlI5W0FXkQ4ypYUx-n1W6Y_Ouhp9Y9xY8lBJesRboJaLMvoHa2_tTYkNQqLd9II_ixcAChz-vewVLANd02VQrH-e2f1yyKn1dXRUDPvc7OOPbwromAUkmwk1mJjX0DFmEmk6BR3yUPMPaaWtvROIMG3fFgd_xbrU7FgxtdcI1w8ft6bEVedztImKlWHwm76isrKatQmNqVnHzLcjcMALkXZrIk1PQtEBgDjd2Ippstu3h8G7DBuGrw0ny4FFgShpzkZrGTI7jxCLGn9nyC5s6pr5r3XHZYvvDYMkrWoLhQyOc_SZge9_J4t2zXmKbFMqygmWVyNHtGGrwFMuPt9xsstoCyQCXu_gITKVAsMJzRDRqDosfq0SdCkA1hYn-YGXxG2NmjDG-maIB_xGgFeRf5isDnlkCu4fpOvyNHGMONubWls_AMBX_-XhzMnlzniOEreQMEc");

                        HttpResponseMessage response = httpClient.GetAsync(cafebazar_payment_confirm_url).Result;


                        if (response.IsSuccessStatusCode)
                        {
                            string result_data = response.Content.ReadAsStringAsync().Result;
                            request_result = JsonConvert.DeserializeObject<cafebazar_request_result_view_model>(result_data);
                        }
                        else
                            return result_model;
                    }

                    if (request_result == null || request_result.developerPayload != i_data.Value.ToString() || request_result.purchaseState == 1)
                        return result_model;
                    else
                    {
                        try
                        {
                            _db.admin_confirm_factor_wallet(model.factor_id, model.payment_gateway_id, (int)price.Value, request_result.purchaseState.ToString(),
                                request_result.purchaseTime.ToString(), request_result.consumptionState.ToString(), request_result.developerPayload, res);

                            int result_insert = Convert.ToInt32(res.Value);


                            if (result_insert < 0)
                            {
                                log_factor("factor_id : " + model.factor_id.ToString());
                                return result_model;
                            }

                            result_model = new doing_job_result_view_model()
                            {
                                is_done = true
                            };

                            return result_model;
                        }
                        catch (Exception ex)
                        {
                            log_factor("factor_id : " + model.factor_id.ToString());
                            return result_model;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "user_confirm_payment", e.Message));
                return new doing_job_result_view_model()
                {
                    is_done = false,
                    Error_Id = error_enum.unknown_error.GetCode(),
                    Error_Text = error_enum.unknown_error.GetDescription()
                };
            }

        }

        private void log_factor(string factor_id)
        {
            string path = Config.get_default_log_file_for_factors();

            if (!File.Exists(path))
                using (StreamWriter sw = File.CreateText(path))
                    sw.WriteLine(factor_id);
            else
                using (StreamWriter sw = File.AppendText(path))
                    sw.WriteLine(factor_id);
        }

        public bool Save_user_infoAsync(string id, string username, string name, string family)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string secret_key = _codeGeneratorService.generate_secret_key();

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var results = _db.admin_register_Namad(id, username, name, family
                        , secret_key, provider_enum.Bourse.GetCode(), res);

                    int result_add = Convert.ToInt32(res.Value);

                    if (result_add == (int)error_enum.Namad_exists || result_add > 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "Save_user_infoAsync", e.Message));
                return false;
            }

            return false;
        }

        public doing_job_result_view_model users_like_comment(CommentBaseViewModel model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_like_comment(user_id, secret_key, model.Comment_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_comment", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_unlike_comment(CommentBaseViewModel model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_unlike_comment(user_id, secret_key, model.Comment_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_comment", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public get_info_post_result_view_model users_get_comment_likes_list(UserGetCommentReplyListViewModel model)
        {
            get_info_post_result_view_model result_model = new get_info_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_get_comment_likes_list(user_id, secret_key, model.Comment_id, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = result;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_comment", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_create_basket(user_create_basket_input_view_model model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_create_basket(user_id, secret_key, model.name, model.basket_balance, model.type_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_comment", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_create_basket_order(user_create_basket_order_input_view_model model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_create_basket_order(user_id, secret_key, model.namad_id, model.order_price, model.count, model.basket_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_comment", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_search_stock(search_profile_data_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_search_stock(user_id, secret_key, model.search_str, model.type_id, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_basket_cancel_order(users_basket_cancel_order_input_view_model model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_basket_cancel_order(user_id, secret_key, model.order_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_comment", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_basket_order_history(users_get_basket_order_history_input_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_basket_order_history(user_id, secret_key, model.basket_id, model.index, model.count, res).ToList();
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.Informations = results;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_basket_data(users_get_basket_order_history_input_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    if (model.count == 0)
                        model.count = 10;

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_basket_info = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_basket_info = _db.users_get_basket_info(user_id, secret_key, model.basket_id, res_basket_info).ToList();
                    int result_basket_info_get_profile_info = Convert.ToInt32(res_basket_info.Value);

                    if (result_basket_info_get_profile_info < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_basket_info_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_basket_info_get_profile_info).GetDescription();
                        return result_model;
                    }

                    SqlParameter res_basket_order_history = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_basket_order_history = _db.users_get_basket_order_history(user_id, secret_key, model.basket_id, model.index, model.count, res_basket_order_history).ToList();
                    int result_basket_order_history = Convert.ToInt32(res_basket_order_history.Value);

                    if (result_basket_order_history < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_basket_order_history).GetCode();
                        result_model.Error_Text = ((error_enum)result_basket_order_history).GetDescription();
                        return result_model;
                    }

                    SqlParameter res_basket_items = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_basket_items = _db.users_get_basket_items(user_id, secret_key, model.basket_id, res_basket_items).ToList();
                    int result_basket_items = Convert.ToInt32(res_basket_items.Value);

                    if (result_basket_items < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_basket_order_history).GetCode();
                        result_model.Error_Text = ((error_enum)result_basket_order_history).GetDescription();
                        return result_model;
                    }

                    result_model.Informations = new users_get_basket_data_output_view_model()
                    {
                        result_basket_items = results_basket_items,
                        result_basket_order_history = results_basket_order_history,
                        result_basket_info = results_basket_info.First()
                    };

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_get_basket_data", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_profile_baskets(users_get_profile_baskets_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();
            List<users_get_profile_baskets_output_view_model> informations = new List<users_get_profile_baskets_output_view_model>();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_get_profile_baskets = _db.users_get_profile_baskets(user_id, secret_key, model.profile_user_id, res_get_profile_baskets).ToList();
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (result_get_profile_baskets < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_baskets).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_baskets).GetDescription();
                        return result_model;
                    }

                    if (results_get_profile_baskets == null)
                    {
                        result_model.Error_Id = (error_enum.unknown_error).GetCode();
                        result_model.Error_Text = (error_enum.unknown_error).GetDescription();
                        return result_model;
                    }

                    foreach (var item in results_get_profile_baskets)
                    {
                        if (item == null)
                            continue;
                        users_get_profile_baskets_output_view_model data = new users_get_profile_baskets_output_view_model() { info = item };

                        SqlParameter res_basket_history = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                        var results_basket_history = _db.users_get_basket_history(user_id, secret_key, item.ID, res_basket_history).ToList();
                        int result_basket_history = Convert.ToInt32(res_basket_history.Value);

                        if (result_basket_history < 0)
                        {
                            result_model.Informations = null;
                            result_model.Error_Id = ((error_enum)result_basket_history).GetCode();
                            result_model.Error_Text = ((error_enum)result_basket_history).GetDescription();
                            return result_model;
                        }

                        data.history = results_basket_history;

                        int history_count = results_basket_history.Count();

                        if (history_count >= 7)
                        {
                            data.one_week_profit = results_basket_history[Math.Max(0, history_count - 1)].daily_profit - results_basket_history[Math.Max(0, history_count - 1 - 7)].daily_profit;
                        }

                        if (history_count >= 30)
                        {
                            data.one_month_profit = results_basket_history[Math.Max(0, history_count - 1)].daily_profit - results_basket_history[Math.Max(0, history_count - 1 - 30)].daily_profit;
                        }

                        if (history_count >= 90)
                        {
                            data.three_months_profit = results_basket_history[Math.Max(0, history_count - 1)].daily_profit - results_basket_history[Math.Max(0, history_count - 1 - 90)].daily_profit;
                        }

                        informations.Add(data);
                    }

                    result_model.Informations = informations;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }
        
        public user_get_profile_post_result_view_model users_get_share_baskets(users_get_profile_baskets_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();
            List<users_get_profile_baskets_output_view_model> informations = new List<users_get_profile_baskets_output_view_model>();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_get_profile_baskets = _db.users_get_share_baskets(user_id, secret_key, res_get_profile_baskets).ToList();
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (result_get_profile_baskets < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_baskets).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_baskets).GetDescription();
                        return result_model;
                    }

                    if (results_get_profile_baskets == null)
                    {
                        result_model.Error_Id = (error_enum.unknown_error).GetCode();
                        result_model.Error_Text = (error_enum.unknown_error).GetDescription();
                        return result_model;
                    }

                    foreach (var item in results_get_profile_baskets)
                    {
                        if (item == null)
                            continue;
                        users_get_profile_baskets_output_view_model data = new users_get_profile_baskets_output_view_model() { info = item };

                        SqlParameter res_basket_history = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                        var results_basket_history = _db.users_get_basket_history(user_id, secret_key, item.ID, res_basket_history).ToList();
                        int result_basket_history = Convert.ToInt32(res_basket_history.Value);

                        if (result_basket_history < 0)
                        {
                            result_model.Informations = null;
                            result_model.Error_Id = ((error_enum)result_basket_history).GetCode();
                            result_model.Error_Text = ((error_enum)result_basket_history).GetDescription();
                            return result_model;
                        }

                        data.history = results_basket_history;

                        int history_count = results_basket_history.Count();

                        if (history_count >= 7)
                        {
                            data.one_week_profit = results_basket_history[Math.Max(0, history_count - 1)].daily_profit - results_basket_history[Math.Max(0, history_count - 1 - 7)].daily_profit;
                        }

                        if (history_count >= 30)
                        {
                            data.one_month_profit = results_basket_history[Math.Max(0, history_count - 1)].daily_profit - results_basket_history[Math.Max(0, history_count - 1 - 30)].daily_profit;
                        }

                        if (history_count >= 90)
                        {
                            data.three_months_profit = results_basket_history[Math.Max(0, history_count - 1)].daily_profit - results_basket_history[Math.Max(0, history_count - 1 - 90)].daily_profit;
                        }

                        informations.Add(data);
                    }

                    result_model.Informations = informations;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_return_info_result_view_model users_create_factor_wallet(users_create_factor_wallet_input_view_model model)
        {
            doing_job_return_info_result_view_model result_model = new doing_job_return_info_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);
                    string i_data = model.i_data;

                    if (model.payment_gateway_id == 1)
                        i_data = "";

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_create_factor_wallet(user_id, secret_key, model.price, model.payment_gateway_id, i_data, res);
                    int output_price = Convert.ToInt32(model.price);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_done = false;
                        result_model.info = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        create_factor_view_model create_Factor_View_Model = new create_factor_view_model();
                        create_Factor_View_Model.factor_id = result_get_profile_info;

                        if (model.payment_gateway_id == 1)
                        {
                            string payment_link = Payment_bahamta.create_request(user_id.ToString(), result_get_profile_info.ToString(), output_price.ToString());
                            create_Factor_View_Model.bahamta_payment_url = payment_link;
                        }

                        result_model.is_done = true;
                        result_model.info = create_Factor_View_Model;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_delete_comment", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_add_item_to_private_basket(users_add_item_to_private_basket_input_view_model model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};

                    var result = _db.users_add_item_to_private_basket(user_id, secret_key, model.namad_id, model.order_price, model.count, model.basket_id, res);
                    int result_get_profile_info = Convert.ToInt32(res.Value);

                    if (result_get_profile_info < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_info).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_info).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_add_item_to_private_basket", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_Private_baskets(users_get_profile_baskets_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();
            users_get_profile_baskets_output_view_model informations = new users_get_profile_baskets_output_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_get_profile_baskets = _db.users_get_Private_baskets(user_id, secret_key, model.profile_user_id, res_get_profile_baskets).FirstOrDefault();
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (result_get_profile_baskets < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = ((error_enum)result_get_profile_baskets).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_baskets).GetDescription();
                        return result_model;
                    }

                    if (results_get_profile_baskets == null)
                    {
                        result_model.Error_Id = (error_enum.unknown_error).GetCode();
                        result_model.Error_Text = (error_enum.unknown_error).GetDescription();
                        return result_model;
                    }

                    if (results_get_profile_baskets != null)
                    {
                        users_get_profile_baskets_output_view_model data = new users_get_profile_baskets_output_view_model() { info = results_get_profile_baskets };

                        SqlParameter res_basket_history = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                        var results_basket_history = _db.users_get_basket_history(user_id, secret_key, results_get_profile_baskets.ID, res_basket_history).ToList();
                        int result_basket_history = Convert.ToInt32(res_basket_history.Value);

                        if (result_basket_history < 0)
                        {
                            result_model.Informations = null;
                            result_model.Error_Id = ((error_enum)result_basket_history).GetCode();
                            result_model.Error_Text = ((error_enum)result_basket_history).GetDescription();
                            return result_model;
                        }

                        data.history = results_basket_history;

                        int history_count = results_basket_history.Count();

                        if (history_count > 0)
                        {
                            data.total_value_USD = results_basket_history[history_count - 1].total_value_USD;
                            data.total_value_CHF = results_basket_history[history_count - 1].total_value_CHF;
                            data.total_value = results_basket_history[history_count - 1].total_value;
                        }

                        informations = data;
                    }

                    result_model.Informations = informations;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_my_balance(get_base_data_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_get_profile_baskets = _db.users_get_get_my_balance(user_id, secret_key, res_get_profile_baskets).FirstOrDefault();
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (results_get_profile_baskets == null)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = (error_enum.unknown_error).GetCode();
                        result_model.Error_Text = (error_enum.unknown_error).GetDescription();
                        return result_model;
                    }

                    result_model.Informations = results_get_profile_baskets;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_share_my_basket(users_share_my_basket_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_get_profile_baskets = _db.users_share_my_basket(user_id, secret_key, model.basket_id, model.name, model.duration, model.price,
                        res_get_profile_baskets);
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (result_get_profile_baskets < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = (error_enum.unknown_error).GetCode();
                        result_model.Error_Text = (error_enum.unknown_error).GetDescription();
                        return result_model;
                    }

                    result_model.Informations = result_get_profile_baskets;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_delete_basket_share_package(basket_share_package_input_view_model model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_get_profile_baskets = _db.users_delete_basket_share_package(user_id, secret_key, model.basket_share_package_id,
                        res_get_profile_baskets);
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (result_get_profile_baskets < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_baskets).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_baskets).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public user_get_profile_post_result_view_model users_get_basket_share_packages(get_basket_share_package_input_view_model model)
        {
            user_get_profile_post_result_view_model result_model = new user_get_profile_post_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results = _db.users_get_basket_share_packages(user_id, secret_key, model.basket_id,
                        res_get_profile_baskets).ToList()
                        ;
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (result_get_profile_baskets < 0)
                    {
                        result_model.Informations = null;
                        result_model.Error_Id = (error_enum.unknown_error).GetCode();
                        result_model.Error_Text = (error_enum.unknown_error).GetDescription();
                        return result_model;
                    }

                    result_model.Informations = results;

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.Informations = null;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_buy_basket_share_packages(basket_share_package_input_view_model model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_get_profile_baskets = _db.users_buy_basket_share_packages(user_id, secret_key, model.basket_share_package_id,
                        res_get_profile_baskets);
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (result_get_profile_baskets < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_baskets).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_baskets).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }

        public doing_job_result_view_model users_set_basket_item_price(user_set_basket_item_price_view_model model)
        {
            doing_job_result_view_model result_model = new doing_job_result_view_model();

            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string unhash_API_Key = _hashingService.unhash_string(model.API_Key);
                    string User_id = "";
                    string secret_key = "";

                    (User_id, secret_key) = CommonCodes.convert_API_Key_To_User_id_and_secret_key(unhash_API_Key);

                    long user_id = 0;
                    long.TryParse(User_id, out user_id);

                    SqlParameter res_get_profile_baskets = new SqlParameter("res", 0) { Direction = ParameterDirection.Output};
                    var results_get_profile_baskets = _db.users_set_basket_item_price(user_id, secret_key, model.basket_item_id, model.current_price,
                        res_get_profile_baskets);
                    int result_get_profile_baskets = Convert.ToInt32(res_get_profile_baskets.Value);

                    if (result_get_profile_baskets < 0)
                    {
                        result_model.is_done = false;
                        result_model.Error_Id = ((error_enum)result_get_profile_baskets).GetCode();
                        result_model.Error_Text = ((error_enum)result_get_profile_baskets).GetDescription();
                    }
                    else
                    {
                        result_model.is_done = true;
                    }

                    return result_model;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "userServices", "users_search", e.Message));
                result_model.is_done = false;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }
    }
}