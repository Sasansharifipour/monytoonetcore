﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Instrument
{
    public class Namad_Get_Price_View_Model : Namad_Get_View_Model
    {
        public DateTime from_date { get; set; }

        public DateTime to_date { get; set; }
    }
}