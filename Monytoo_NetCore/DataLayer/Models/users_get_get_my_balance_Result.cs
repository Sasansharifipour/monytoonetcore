﻿using System;

namespace DataLayer.Models
{
    public class users_get_get_my_balance_Result
    {
        public Nullable<int> real_balance { get; set; }
        public Nullable<int> virtual_balance { get; set; }
    }
}
