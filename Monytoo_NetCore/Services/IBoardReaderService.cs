﻿using CommonCode;
using DomainClasses.Common;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using static CommonCode.CommonCodes;
using System.Data.Entity.Infrastructure;
using System.IO;

namespace Services
{
    public interface IBoardReaderService
    {
        void add_post();
    }

    public class BoardReaderService : IBoardReaderService
    {
        private IDbContextFactory<connectionString> _contextFactory;
        private IImageService _imageService;
        private IuserServices _userService;
        private ILogger _logger;
        private PostService _postService;

        public BoardReaderService(IImageService imageService, IuserServices userService, ILogger logger
            , IDbContextFactory<connectionString> contextFactory, PostService postService)
        {
            _contextFactory = contextFactory;
            _imageService = imageService;
            _userService = userService;
            _postService = postService;
            _logger = logger;
        }

        public void add_post()
        {
            DateTime date = DateTime.Now.Date;

            DateTime d = DateTime.Now;
            PersianCalendar pc = new PersianCalendar();
            string persian_date = string.Format("{0}/{1}/{2}", pc.GetYear(d), pc.GetMonth(d), pc.GetDayOfMonth(d));

            foreach (Board_Type_Element_enum board_Type in (Board_Type_Element_enum[])Enum.GetValues(typeof(Board_Type_Element_enum)))
            {
                bool can_add = can_add_board_elements(board_Type, date);

                if (!can_add)
                    continue;

                var data = get_board_element(board_Type, date);

                var date_position = get_date_position(board_Type);

                var first_element_position = get_first_element_position(board_Type);
                var seed = get_seed(board_Type);

                if (data == null || data.Count <= 0)
                    continue;

                int number_of_templates = get_number_of_templates(board_Type);

                var text_on_images = preparing_post(data, first_element_position, seed);
                var caption = preparing_post_caption(data);

                text_on_images.Add(new PreparingText()
                {
                    Title = persian_date,
                    Font_Color = "24255a",
                    Font_Style = FontStyle.Bold,
                    Font_Size = 8,
                    Font_Name = "B mitra",
                    X = date_position.X,
                    Y = date_position.Y
                });

                var image = ImageUtil.generate_board_image_for_post(board_Type, text_on_images, number_of_templates);
                if (image != null)
                {
                    string image_path = _imageService.save_image(image, saving_image_location.post_image);

                    if (!image_path.IsNullOrEmptyOrWhiteSpace())
                    {
                        long user_id = 0;
                        string secret_key = "";

                        (user_id, secret_key) = Config.get_board_user_info(board_Type);

                        string image_path_complete = Config.get_root_path() + image_path;
                        string API_Key = String.Format("{0}-{1}", user_id, secret_key);

                        string file_name = Path.GetFileName(image_path_complete);

                        var file_data = File.ReadAllBytes(image_path_complete);

                        var result = _postService.post("http://Api.moneytoo.ir/api/users/send_post", file_name, file_data,
                            API_Key, caption, user_id.ToString());

                        int result_update_db = result.Post_id;

                        if (result_update_db > 0)
                            save_add_board_elements(board_Type, date, result_update_db);
                    }
                }
            }
        }

        private int get_number_of_templates(Board_Type_Element_enum board_Type)
        {
            switch (board_Type)
            {
                case Board_Type_Element_enum.Probable_Sales_Queue_Tomorrow:
                case Board_Type_Element_enum.Probable_Purchases_Queue_Tomorrow:
                case Board_Type_Element_enum.Suspicious_Volumes:
                    return 6;
                case Board_Type_Element_enum.Smart_Money_Outflow:
                case Board_Type_Element_enum.Smart_Money_Entry:
                    return 3;
                default:
                    return 0;
            }
        }

        private Point get_first_element_position(Board_Type_Element_enum board_Type)
        {
            switch (board_Type)
            {
                case Board_Type_Element_enum.Probable_Sales_Queue_Tomorrow:
                case Board_Type_Element_enum.Probable_Purchases_Queue_Tomorrow:
                    return new Point(670, 280);
                case Board_Type_Element_enum.Suspicious_Volumes:
                    return new Point(655, 530);
                case Board_Type_Element_enum.Smart_Money_Outflow:
                case Board_Type_Element_enum.Smart_Money_Entry:
                    return new Point(675, 490);
                default:
                    return new Point();
            }
        }

        private Size get_seed(Board_Type_Element_enum board_Type)
        {
            switch (board_Type)
            {
                case Board_Type_Element_enum.Probable_Sales_Queue_Tomorrow:
                case Board_Type_Element_enum.Probable_Purchases_Queue_Tomorrow:
                    return new Size(125, 125);
                default:
                    return new Size(120, 120);
            }
        }

        private Point get_date_position(Board_Type_Element_enum board_Type)
        {
            switch (board_Type)
            {
                case Board_Type_Element_enum.Probable_Sales_Queue_Tomorrow:
                    return new Point(110, 970);
                case Board_Type_Element_enum.Probable_Purchases_Queue_Tomorrow:
                    return new Point(830, 970);
                case Board_Type_Element_enum.Suspicious_Volumes:
                    return new Point(80, 970);
                case Board_Type_Element_enum.Smart_Money_Outflow:
                    return new Point(870, 970);
                case Board_Type_Element_enum.Smart_Money_Entry:
                    return new Point(110, 970);
                default:
                    return new Point();
            }
        }

        public string preparing_post_caption(List<Engine_Board_Reader_Temp> namads)
        {
            string text = "";

            foreach (var item in namads)
                text += item.namad_name + ",";

            if (text.Length >= 1 && text[text.Length - 1] == ',')
                text = text.Substring(0, text.Length - 1);

            return text;
        }

        public List<PreparingText> preparing_post(List<Engine_Board_Reader_Temp> namads, Point first_element_position, Size seed)
        {
            List<PreparingText> result = new List<PreparingText>();

            int first_row_x = first_element_position.X;
            int first_row_y = first_element_position.Y;

            int x_seed = seed.Width;
            int y_seed = seed.Height;

            int row_cnt = 4;
            int row_counter = 0;

            foreach (var item in namads)
            {
                if (row_counter == row_cnt)
                {
                    row_counter = 0;
                    first_row_x = first_element_position.X;
                    first_row_y += y_seed;
                }

                PreparingText preparing = new PreparingText()
                {
                    Title = item.namad_name,
                    Font_Color = "24255a",
                    Font_Style = FontStyle.Bold,
                    Font_Size = 8,
                    Font_Name = "B mitra",
                    max_width = seed.Width,
                    X = first_row_x,
                    Y = first_row_y
                };

                result.Add(preparing);

                first_row_x -= x_seed;
                row_counter++;
            }

            return result;
        }

        public List<Engine_Board_Reader_Temp> get_board_element(Board_Type_Element_enum board_Type, DateTime date)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string title = board_Type.ToString();
                    List<Engine_Board_Reader_Temp> data = _db.Engine_Board_Reader_Temp.Where(s => s.insert_date == date && s.Title == title).OrderBy(s => s.idx).ToList();
                    return data;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BoardReaderService", "get_board_element", e.Message));
                return new List<Engine_Board_Reader_Temp>();
            }
        }

        public bool can_add_board_elements(Board_Type_Element_enum board_Type, DateTime date)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string title = board_Type.ToString();
                    var item = _db.Engine_Board_Reader_Posting_Temp.FirstOrDefault(s => s.insert_date == date && s.Name.Trim() == title.Trim());

                    if (item == null || item.ID <= 0 || item.post_id == null)
                        return true;

                    return false;
                }
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BoardReaderService", "can_add_board_elements", e.Message));
                return false;
            }
        }

        public void save_add_board_elements(Board_Type_Element_enum board_Type, DateTime date, int post_id)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string title = board_Type.ToString();
                    var item = _db.Engine_Board_Reader_Posting_Temp.FirstOrDefault(s => s.insert_date == date && s.Name.Trim() == title.Trim());

                    if (item == null || item.ID <= 0)
                    {
                        item = new Engine_Board_Reader_Posting_Temp() { insert_date = date, Name = title.Trim(), post_id = post_id };
                        _db.Engine_Board_Reader_Posting_Temp.Add(item);
                        _db.SaveChanges();
                    }
                    else
                    {
                        item.post_id = post_id;
                        _db.SaveChanges();
                    }
                }

            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BoardReaderService", "save_add_board_elements", e.Message));
            }
        }
    }
}
