﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static CommonCode.CommonCodes;

namespace DomainClasses.RSI
{
    public class RSI_Posting_View_Model
    {
        public string namad_name { get; set; } = "";

        public decimal namad_price { get; set; } = 0;

        public double rsi { get; set; } = 0;

        public RSI_POST_enum post_type { get; set; }
    }
}