﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class cafebazar_confirm_view_model
    {
        public string package_name { get; set; } = "";

        public string product_id { get; set; } = "";

        public string purchase_token { get; set; } = "";

        public int factor_id { get; set; } = 0;

        public int user_id { get; set; } = 0;

        public int payment_gateway_id { get; set; } = 0;
    }
}
