﻿using CommonCode;
using System.IO;

namespace Services
{
    public interface ILogger
    {
        void save_log(string exception);
    }

    public class Logger : ILogger
    {
        public void save_log(string exception)
        {
            string path = Config.get_default_log_file_for_errors();

            if (!File.Exists(path))
                using (StreamWriter sw = File.CreateText(path))
                    sw.WriteLine(exception);
            else
                using (StreamWriter sw = File.AppendText(path))
                    sw.WriteLine(exception);
        }
    }
}
