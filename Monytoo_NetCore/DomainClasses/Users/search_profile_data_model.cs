﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Users
{
    public class search_profile_data_model : get_base_data_model
    {
        public string search_str { get; set; }

        public int type_id { get; set; } = 0;
    }
}