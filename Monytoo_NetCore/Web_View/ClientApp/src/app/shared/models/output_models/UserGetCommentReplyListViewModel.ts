import { CommentBaseViewModel } from "./CommentBaseViewModel";

export interface UserGetCommentReplyListViewModel extends CommentBaseViewModel {
  index: number;
  count: number;
}
