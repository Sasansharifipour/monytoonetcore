export interface cafebazar_confirm_view_model {
  package_name: string;
  product_id: string;
  purchase_token: string;
  factor_id: number;
  user_id: number;
  payment_gateway_id: number;
}
