﻿using System;

namespace DataLayer.Models
{
    public class Engine_Board_Reader_Posting_Temp
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> insert_date { get; set; }
        public Nullable<long> post_id { get; set; }
    }
}
