﻿
namespace Services
{
    public interface IHashingService
    {
        string hash_string(string value);

        string unhash_string(string value);
    }

    public class HashingService : IHashingService
    {
        public string hash_string(string value)
        {
            return value;
        }

        public string unhash_string(string value)
        {
            return value;
        }
    }
}