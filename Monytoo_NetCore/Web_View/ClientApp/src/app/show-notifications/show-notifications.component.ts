import { Component, Inject, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { UserService } from '../shared/services/users.services';
import { check_user_name_available_model } from '../shared/models/output_models/check_user_name_available_model.interface';
import { UserRegistration } from '../shared/models/inline_models/user.registration.interface';
import { register_model } from '../shared/models/output_models/register_model.interface';
import { register_result_model } from '../shared/models/input_models/register_result_model.interface';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router } from '@angular/router';
import { get_post_by_index_view_model } from '../shared/models/output_models/get_post_by_index_view_model.interface';
import { users_home_posts_result_view_model } from '../shared/models/input_models/users_home_posts_result_view_model.interface';
import { users_home_posts_Result } from '../shared/models/input_models/users_home_posts_Result.interface';
import { get_profile_data_model } from '../shared/models/output_models/get_profile_data_model.interface';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { get_post_image_view_model } from '../shared/models/output_models/get_post_image_view_model';
import { user_get_notification_API_View_Model } from '../shared/models/output_models/user_get_notification_API_View_Model.interface';
import { users_get_notification_list_result_view_model } from '../shared/models/input_models/users_get_notification_list_result_view_model.interface';
import { users_get_notification_list_Result } from '../shared/models/input_models/users_get_notification_list_Result';
import { get_base_data_model } from '../shared/models/output_models/get_base_data_model.interface';
import { users_get_follow_suggestion_list_result_view_model } from '../shared/models/input_models/users_get_follow_suggestion_list_result_view_model.interface';
import { users_get_follow_suggestion_list_Result } from '../shared/models/input_models/users_get_follow_suggestion_list_Result';
import { update_profile_data_result_model } from '../shared/models/input_models/update_profile_data_result_model.interface';

@Component({
  selector: 'app-show-notifications',
  templateUrl: './show-notifications.component.html'
})
export class ShowNotificationsComponent  {
  private pg_idx: number = -1;
  isLoading = false;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private router: Router, private dialogService: MatDialog, private sanitizer: DomSanitizer) {

    this.APIKEY = localStorage.getItem('API_KEY');

    if (this.APIKEY == null) {
      this.router.navigate(['']);
    }

    this.loadInfo();
    this.load_suggestion_list();
  }
  private APIKEY: string = '';
  private data: users_get_notification_list_Result[] = [];
  private suggest_data: users_get_follow_suggestion_list_Result[] = [];

  @HostListener("window:scroll", ["$event"])
  getScrollHeight(): void {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight && !this.isLoading) {
      this.loadInfo();
    }
  }

  load_suggestion_list() {
    var model: get_base_data_model = { API_Key: this.APIKEY };

    this.userService.users_get_follow_suggestion_list(model).subscribe((result: users_get_follow_suggestion_list_result_view_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);
      } else {
        let t_data: users_get_follow_suggestion_list_Result[];
        t_data = result.informations;

        t_data.forEach(item => {
          this.get_profile_image(item.ID, item.profile_image_t).subscribe(result => {
            let objectURL = URL.createObjectURL(result);
            item.profile_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
          });
        });

        t_data.forEach(item => {

          if (!this.suggest_data.includes(item))
            this.suggest_data.push(item);
        });
      }
    });
  }

  follow_user(user_id) {
    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_id.toString(), t: '' };

    this.userService.follow(model).subscribe((result: update_profile_data_result_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else if (result.is_updated) {
        var idx = 0;

        this.suggest_data.forEach(item => {
          if (item.ID == user_id) {
            this.suggest_data.splice(idx, 1);
            return;
          }
          idx++;
        });
      }

    });

  }

  remove_from_suggestion_list(user_id) {
    var idx = 0;

    this.suggest_data.forEach(item => {
      if (item.ID == user_id) {
        this.suggest_data.splice(idx, 1);
        return;
      }
      idx++;
    });

  }

  loadInfo() {

    this.isLoading = true;
    var model: user_get_notification_API_View_Model = { API_Key: this.APIKEY, count: 10, index: this.pg_idx + 1 };

    this.userService.users_get_notification_list(model).subscribe((result: users_get_notification_list_result_view_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.pg_idx = this.pg_idx + 1;
        let t_data: users_get_notification_list_Result[];
        t_data = result.informations;
        
        t_data.forEach(item => {

          this.get_profile_image(item.actor_id, item.actor_profile_image_t).subscribe(result => {
            let objectURL = URL.createObjectURL(result);
              item.profile_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
          });

          item.action_text = this.get_notification_text(item);
        });

        t_data.forEach(item => {

          if (!this.data.includes(item))
            this.data.push(item);
        });
      }

    });

    this.isLoading = false;
  }

  get_notification_text(data: users_get_notification_list_Result): string {
    let result: string = '';

    if (data.notification_type_id == 2) {
      result = 'پست شما را لایک کرد.';
    } else if (data.notification_type_id == 4) {
      result = 'شما را دنبال می کند.';
    } else if (data.notification_type_id == 6) {
      let json = data.action.replace(/{/g, "{\"").replace(/,/g, ",\"").replace(/:/g, "\":").replace(/'/g, "\"");
      var json_data = JSON.parse(json);

      result = 'یک نظر جدید برای پست شما ارسال کرد: ' + json_data.comment_text;
    } else if (data.notification_type_id == 11) {
      let json = data.action.replace(/{/g, "{\"").replace(/,/g, ",\"").replace(/:/g, "\":").replace(/'/g, "\"");
      var json_data = JSON.parse(json);

      result = 'سفارش ' + json_data.namad_name + ' در سبد \"' + json_data.basket_name + '\" لغو شد.';
    }

    return result;

  }

  get_profile_image(user_profile_id, t): Observable<string> {

    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_profile_id, t: t };

    return this.userService.get_profile_image(model);
  }

  get_post_image(post_id, image_id): Observable<string> {

    var model: get_post_image_view_model = { API_Key: this.APIKEY, post_id: post_id, image_id: image_id };

    return this.userService.users_get_image(model);
  }

}

