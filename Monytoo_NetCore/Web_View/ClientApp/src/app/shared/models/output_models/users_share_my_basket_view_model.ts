import { get_base_data_model } from "./get_base_data_model.interface";

export interface users_share_my_basket_view_model extends get_base_data_model {
  name: string;
  basket_id: number;
  duration: number;
  price: number;
}
