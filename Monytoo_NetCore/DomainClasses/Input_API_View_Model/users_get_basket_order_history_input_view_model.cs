﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class users_get_basket_order_history_input_view_model : get_base_data_model
    {
        public int basket_id { get; set; } = 0;

        public int index { get; set; } = 0;

        public int count { get; set; } = 0;
    }
}
