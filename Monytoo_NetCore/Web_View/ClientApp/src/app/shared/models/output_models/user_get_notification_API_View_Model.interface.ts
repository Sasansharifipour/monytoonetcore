import { get_base_data_model } from "./get_base_data_model.interface";

export interface user_get_notification_API_View_Model extends get_base_data_model {
  index: number;
  count: number;
}
