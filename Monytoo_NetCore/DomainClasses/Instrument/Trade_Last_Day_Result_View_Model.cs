﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Instrument
{
    public class Trade_Last_Day_Result_View_Model
    {
        public string LVal18AFC { get; set; }
        public int DEven { get; set; }
        public decimal ZTotTran { get; set; }
        public decimal QTotTran5J { get; set; }
        public decimal QTotCap { get; set; }
        public long InsCode { get; set; }
        public string LVal30 { get; set; }
        public decimal PClosing { get; set; }
        public decimal PDrCotVal { get; set; }
        public decimal PriceChange { get; set; }
        public decimal PriceMin { get; set; }
        public decimal PriceMax { get; set; }
        public decimal PriceFirst { get; set; }
        public decimal PriceYesterday { get; set; }
        public int HEven { get; set; }
    }
}