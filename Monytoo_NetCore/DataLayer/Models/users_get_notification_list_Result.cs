﻿using System;

namespace DataLayer.Models
{
    public class users_get_notification_list_Result
    {
        public long ID { get; set; }
        public Nullable<System.DateTime> create_time { get; set; }
        public Nullable<bool> is_seen { get; set; }
        public Nullable<long> notification_type_id { get; set; }
        public Nullable<long> actor_id { get; set; }
        public string actor_name { get; set; }
        public string actor_lastname { get; set; }
        public string actor_username { get; set; }
        public Nullable<int> actor_type_id { get; set; }
        public Nullable<long> act_on_id { get; set; }
        public string act_on_name { get; set; }
        public string act_on_lastname { get; set; }
        public string act_on_username { get; set; }
        public Nullable<int> act_on_type_id { get; set; }
        public string notification_key_name { get; set; }
        public Nullable<long> r_number { get; set; }
        public Nullable<long> actor_profile_image_t { get; set; }
        public Nullable<long> act_on_profile_image_t { get; set; }
        public string action { get; set; }
    }
}
