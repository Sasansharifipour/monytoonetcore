import { SafeUrl } from "@angular/platform-browser";

export interface users_get_profile_posts_Result {
  post_id: number;
  caption: string;
  image_id: number;
  comment_count: number;
  from_user_id: number;
  insert_time: Date;
  post_image_data: SafeUrl;
  like_count: number;
  user_id: number;
  username: string;
  name: string;
  lastname: string;
  user_type: number;
  is_liked: string;
  comment_id: number;
  comment_text: string;
  comment_time: Date;
  comment_user_id: number;
  reply_id: number;
  reply_text: string;
  reply_time: Date;
  replay_user_id: number;
  r_number: number;
  comment_username: string;
  comment_user_type_id: number;
  reply_username: string;
  reply_user_type_id: number;
  profile_image_t: number;
  comment_user_profile_image_t: number;
  reply_user_profile_image_t: number;
}
