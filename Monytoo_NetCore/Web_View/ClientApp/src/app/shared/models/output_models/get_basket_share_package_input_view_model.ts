import { get_base_data_model } from "./get_base_data_model.interface";

export interface get_basket_share_package_input_view_model extends get_base_data_model {
  basket_id: number;
}
