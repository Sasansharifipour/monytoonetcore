import { get_base_data_model } from "./get_base_data_model.interface";

export interface get_profile_data_model extends get_base_data_model {
  user_profile_id: string;
  t: string;
}
