﻿using DomainClasses.RSS;
using System.Collections.Generic;

namespace Services
{
    public interface IRSSProvider
    {
        IEnumerable<RssNews> Read_Feeds(string rss_url);
    }

    public class NullRSSProvider : IRSSProvider
    {
        public IEnumerable<RssNews> Read_Feeds(string rss_url)
        {
            return new List<RssNews>();
        }
    }
}