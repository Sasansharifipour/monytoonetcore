﻿using CommonCode;
using DomainClasses.Instrument;
using DomainClasses.Web_Services;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using static CommonCode.CommonCodes;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace Services
{
    public interface IInstrumentService
    {
        bool update_instrument_info(long user_id, string secret_key, long namad_id, string field_key_name, string field_value);

        bool update_instrument_info_list(long user_id, string secret_key, List<Instrument_Update_Information_Field_View_Model> data);

        List<Instrument_Price_View_Model> get_TradeOneDay_For_All_Instruments(long user_id, string secret_key, DateTime datetime, byte Flow);

        List<Engine_get_all_namad_Result> get_all_namads(long user_id, string secret_key);

        Task<add_data_result_view_model> initial_instruments(byte Flow);

    }

    public class InstrumentService : IInstrumentService
    {
        private IDbContextFactory<connectionString> _contextFactory;
        private ITsetmcService _tsetmcService;
        private string secret_key = "";
        private long user_id = 0;
        private ICodeGeneratorService _codeGeneratorService;

        public InstrumentService(ITsetmcService tsetmcService,
            ICodeGeneratorService codeGeneratorService, IDbContextFactory<connectionString> contextFactory)
        {
            _tsetmcService = tsetmcService;
            _contextFactory = contextFactory;
            _codeGeneratorService = codeGeneratorService;
            (user_id, secret_key) = Config.get_user_info_from_config("rsi");
        }

        public async Task<add_data_result_view_model> initial_instruments(byte Flow)
        {
            add_data_result_view_model result = new add_data_result_view_model();

            var instruments_task = await _tsetmcService.Get_Instrument_From_Source(Flow);
            var all_namads = get_all_namads(user_id, secret_key);
            List<Instrument_Result> instruments = instruments_task.Where(s => s.Valid == 1 && all_namads.All(p => Convert.ToInt64(p.InsCode) != s.InsCode)).ToList();

            int cnt = 0;
            int added_cnt = 0;
            int error_cnt = 0;

            foreach (var item in instruments)
            {
                cnt++;

                if ("0123456789".Contains(item.LVal18AFC[item.LVal18AFC.Length - 1]))
                    continue;

                bool added = await Add_instruments_to_db(item);
                if (added) added_cnt++; else error_cnt++;
            }

            result.instrument_count = instruments.Count;
            result.added_count = added_cnt;
            result.not_added_count = error_cnt;

            if (result.not_added_count > 0)
            {
                result.Error_Id = "-1";
                result.Error_Text = "Some instrument did not add";
            }

            return result;
        }

        private async Task<bool> Add_instruments_to_db(Instrument_Result data)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    string secret_key = _codeGeneratorService.generate_secret_key();

                    SqlParameter res = new SqlParameter("res", typeof(global::System.Int32));
                    string unique_code = data.LVal18 + "-" + data.InsCode.ToString().Substring(data.InsCode.ToString().Length - 3, 3);

                    var results = await Task.Run(() => _db.admin_register_Namad(data.InsCode.ToString(),
                        unique_code, data.LVal18AFC.Change_Arabic_To_Persian().Remove_Number_From_String(), data.LVal30.Change_Arabic_To_Persian().Remove_Number_From_String()
                        , secret_key, provider_enum.Bourse.GetCode(), res));

                    int result_add = Convert.ToInt32(res.Value);

                    if (result_add == (int)error_enum.Namad_exists || result_add > 0)
                        return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return false;
        }

        public List<Instrument_Price_View_Model> get_TradeOneDay_For_All_Instruments(long user_id, string secret_key, DateTime datetime, byte Flow)
        {
            int date = 0;

            int.TryParse(datetime.ToString("yyyyMMdd", CultureInfo.InvariantCulture), out date);

            if (date <= 0)
                return null;

            var tradeOneDayAll_Results = _tsetmcService.Get_TradeOneDayAll_From_Source(date, Flow);
            var instrumentsState_Results = _tsetmcService.Get_InstrumentsState_From_Source(Flow);

            var all_namads = get_all_namads(user_id, secret_key);

            var instrument_Price_View_Models = tradeOneDayAll_Results.Result.Select(s => new Instrument_Price_View_Model()
            {
                InsCode = s.InsCode,
                ENGINE_Price_Reader_Last_Price = s.PDrCotVal.ToString(),
                ENGINE_Price_Reader_Close_Price = s.PClosing.ToString(),
                ENGINE_Price_Reader_Volume = s.QTotTran5J.ToString(),
                ENGINE_Price_Reader_Fluctuation = s.PriceChange.ToString()
            }).ToList();

            foreach (var item in instrument_Price_View_Models.ToList())
            {
                var exist = all_namads.Where(s => s.InsCode == item.InsCode.ToString()).FirstOrDefault();
                if (exist == null || exist.namad_id <= 0)
                {
                    instrument_Price_View_Models.Remove(item);
                    continue;
                }

                var instrument_state = instrumentsState_Results.Result.FirstOrDefault(s => s.InsCode == item.InsCode);
                item.namad_id = exist.namad_id;

                if (instrument_state == null || instrument_state.InsCode == 0)
                {
                    item.ENGINE_Price_Reader_Namad_Status = CommonCodes.get_instrument_status("O");
                    item.ENGINE_Price_Reader_Namad_Status_id = "O";
                }
                else
                {
                    item.ENGINE_Price_Reader_Namad_Status = CommonCodes.get_instrument_status(instrument_state.CEtaVal.Trim());
                    item.ENGINE_Price_Reader_Namad_Status_id = instrument_state.CEtaVal.Trim();
                }
            }

            if (instrument_Price_View_Models == null)
                instrument_Price_View_Models = new List<Instrument_Price_View_Model>();

            return instrument_Price_View_Models.ToList();
        }

        public bool update_instrument_info(long user_id, string secret_key, long namad_id, string field_key_name, string field_value)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", 0);
                    _db.Engine_update_namad_information_fields(user_id, secret_key, namad_id, field_key_name,
                        field_value, res);

                    int result_verify = Convert.ToInt32(res.Value);

                    if (result_verify < 0)
                        return false;

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool update_instrument_info_list(long user_id, string secret_key, List<Instrument_Update_Information_Field_View_Model> data)
        {
            try
            {
                if (data == null || data.Count == 0)
                    return true;

                var datatable = new DataTable();
                datatable.Columns.Add("namad_id");
                datatable.Columns.Add("field_key_name");
                datatable.Columns.Add("field_value");

                foreach (var item in data)
                    if (!item.Information_Field_Value.IsNullOrEmptyOrWhiteSpace() &&
                        !item.Information_Field_Name.IsNullOrEmptyOrWhiteSpace())
                        datatable.Rows.Add(item.Namad_Id, item.Information_Field_Name, item.Information_Field_Value.Replace('/', '.'));

                using (var _db = _contextFactory.Create())
                    _db.Engine_update_namad_information_fields_list(user_id, secret_key, datatable);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<Engine_get_all_namad_Result> get_all_namads(long user_id, string secret_key)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", typeof(global::System.Int32));
                    var results = _db.Engine_get_all_namad(user_id, secret_key, res).ToList();

                    return results;
                }
            }
            catch (Exception e)
            {
                return new List<Engine_get_all_namad_Result>();
            }
        }
    }
}