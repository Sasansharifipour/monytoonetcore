﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Instrument
{
    public class Instrument_Price_View_Model
    {
        public long InsCode { get; set; } = 0;

        public long namad_id { get; set; } = 0;

        public string ENGINE_Price_Reader_Namad_Status { get; set; } = "";

        public string ENGINE_Price_Reader_Namad_Status_id { get; set; } = "";

        public string ENGINE_Price_Reader_Last_Price { get; set; } = "";

        public string ENGINE_Price_Reader_Fluctuation { get; set; } = "";

        public string ENGINE_Price_Reader_Close_Price { get; set; } = "";

        public string ENGINE_Price_Reader_Volume { get; set; } = "";

        public string ENGINE_Price_Reader_Haghighi_Volume_Sell { get; set; } = "";

        public string ENGINE_Price_Reader_Haghighi_Volume_Buy { get; set; } = "";

        public string ENGINE_Price_Reader_Hoghoghi_Volume_Sell { get; set; } = "";

        public string ENGINE_Price_Reader_Hoghoghi_Volume_Buy { get; set; } = "";

        public string ENGINE_Price_Reader_Haghighi_Count_Sell { get; set; } = "";

        public string ENGINE_Price_Reader_Haghighi_Count_Buy { get; set; } = "";

        public string ENGINE_Price_Reader_Hoghoghi_Count_Sell { get; set; } = "";
        
        public string ENGINE_Price_Reader_Hoghoghi_Count_Buy { get; set; } = "";

        public string ENGINE_Price_Reader_Haghighi_Fluctuation_Sell { get; set; } = "";

        public string ENGINE_Price_Reader_Haghighi_Fluctuation_Buy { get; set; } = "";

        public string ENGINE_Price_Reader_Hoghoghi_Fluctuation_Sell { get; set; } = "";

        public string ENGINE_Price_Reader_Hoghoghi_Fluctuation_Buy { get; set; } = "";

        public string ENGINE_Price_Reader_Namad_Type { get; set; } = "";

        public string ENGINE_Price_Reader_Base_Volume { get; set; } = "";

        public string ENGINE_Price_Reader_Last_Price_Change { get; set; } = "";

        public string ENGINE_Price_Reader_Close_Price_Change { get; set; } = "";

        public string ENGINE_Price_Reader_Market_Type { get; set; } = "";

        public string ENGINE_PRICE_MAX_VALUE { get; set; } = "";

        public string ENGINE_PRICE_MIN_VALUE { get; set; } = "";

        public string ENGINE_Namad_Current_Trade_Volume { get; set; } = "";

        public string ENGINE_Namad_Queue_Difficulty_Buy { get; set; } = "";

        public string ENGINE_Namad_Queue_Difficulty_Sell { get; set; } = "";
    }
}