﻿using System;

namespace DataLayer.Models
{
    public class users_get_price_packages_Result
    {
        public long ID { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Nullable<int> duration { get; set; }
        public Nullable<int> price { get; set; }
        public string image_url { get; set; }
    }
}
