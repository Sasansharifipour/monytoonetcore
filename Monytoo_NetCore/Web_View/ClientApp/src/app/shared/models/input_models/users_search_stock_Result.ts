export interface users_search_stock_Result {
  iD: number;
  type_id: number;
  username: string;
  name: string;
  lastname: string;
  profile_image_t: number;
  max_value: string;
  min_value: string;
  namad_status: string;
  last_price: string;
}
