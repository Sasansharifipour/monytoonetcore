export interface users_get_basket_share_packages_Result {
  iD: number;
  name: string;
  duration: number;
  price: number;
}
