﻿using DomainClasses.RSS;
using System;
using System.Collections.Generic;
using System.Globalization;
using static CommonCode.CommonCodes;

namespace Services
{
    public class NabzeBourseRSSReader : IRSSProvider
    {
        private IRSSReaderService _rssReaderService;

        public NabzeBourseRSSReader(IRSSReaderService rssReaderService)
        {
            _rssReaderService = rssReaderService;
        }

        public IEnumerable<RssNews> Read_Feeds(string rss_url)
        {
            List<RssNews> results = new List<RssNews>();

            try
            {
                DateTimeFormatInfo info = new DateTimeFormatInfo()
                { FullDateTimePattern = "ddd, dd MMM yyyy h:mm tt zzz" };

                var data = _rssReaderService.Read_RSS_From_URL(rss_url, "item");
                var rss_data = Convert_RSS_Feed_To_Object<RSS_nabzebourse>(data);

                foreach (var item in rss_data)
                {
                    RssNews rss = new RssNews()
                    {
                        Rss_URL = rss_url,
                        Title = item.title,
                        Description = item.description,
                        Link = item.link,
                        Date = Convert.ToDateTime(item.pubDate, info),
                        Image_path = item.enclosure,
                    };

                    results.Add(rss);
                }
            }
            catch (Exception e)
            {
                return new List<RssNews>();
            }

            return results;
        }
    }
}