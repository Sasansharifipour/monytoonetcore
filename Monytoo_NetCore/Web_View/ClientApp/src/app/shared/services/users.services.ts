import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { privacy_result_model } from '../models/input_models/privacy_result_model.interface';
import { check_user_name_available_model } from '../models/output_models/check_user_name_available_model.interface';
import { check_user_name_available_result_model } from '../models/input_models/check_user_name_available_result_model.interface';
import { register_model } from '../models/output_models/register_model.interface';
import { register_result_model } from '../models/input_models/register_result_model.interface';
import { verify_by_mobile_number_model } from '../models/output_models/verify_by_mobile_number_model.interface';
import { verify_by_mobile_number_result_model } from '../models/input_models/verify_by_mobile_number_result_model.interface';
import { update_profile_data_result_model } from '../models/input_models/update_profile_data_result_model.interface';
import { update_profile_data_model } from '../models/output_models/update_profile_data_model.interface';
import { update_information_field_value_model } from '../models/output_models/update_information_field_value_model.interface';
import { get_profile_data_model } from '../models/output_models/get_profile_data_model.interface';
import { get_profile_data_result_model } from '../models/input_models/get_profile_data_result_model.interface';
import { get_user_follow_view_model } from '../models/output_models/get_user_follow_view_model.interface';
import { user_get_profile_post_result_view_model } from '../models/input_models/user_get_profile_post_result_view_model.interface';
import { search_profile_data_model } from '../models/output_models/search_profile_data_model.interface';
import { users_search_result_view_model } from '../models/input_models/users_search_result_view_model.interface';
import { get_post_by_index_view_model } from '../models/output_models/get_post_by_index_view_model.interface';
import { users_home_posts_result_view_model } from '../models/input_models/users_home_posts_result_view_model.interface';
import { users_explore_posts_result_view_model } from '../models/input_models/users_explore_posts_result_view_model.interface';
import { get_post_view_model } from '../models/output_models/get_post_view_model.interface';
import { users_get_single_post_result_view_model } from '../models/input_models/users_get_single_post_result_view_model.interface';
import { users_doing_works_result_int_result_view_model } from '../models/input_models/users_doing_works_result_int_result_view_model.interface';
import { users_get_profile_followers_result_view_model } from '../models/input_models/users_get_profile_followers_result_view_model.interface';
import { delete_info_result_view_model } from '../models/input_models/delete_info_result_view_model.interface';
import { get_base_data_model } from '../models/output_models/get_base_data_model.interface';
import { users_get_price_packages_result_view_model } from '../models/input_models/users_get_price_packages_result_view_model.interface';
import { users_create_factor_API_View_Model } from '../models/output_models/users_create_factor_API_View_Model.interface';
import { users_create_factor_result_view_model } from '../models/input_models/users_create_factor_result_view_model.interface';
import { users_get_notification_list_result_view_model } from '../models/input_models/users_get_notification_list_result_view_model.interface';
import { user_get_notification_API_View_Model } from '../models/output_models/user_get_notification_API_View_Model.interface';
import { UserSendCommentViewModel } from '../models/output_models/UserSendCommentViewModel';
import { add_data_result_view_model } from '../models/input_models/add_data_result_view_model';
import { CommentBaseViewModel } from '../models/output_models/CommentBaseViewModel';
import { PostBaseViewModel } from '../models/output_models/PostBaseViewModel';
import { doing_job_result_view_model } from '../models/input_models/doing_job_result_view_model';
import { UserGetCommentReplyListViewModel } from '../models/output_models/UserGetCommentReplyListViewModel';
import { UserGetPostCommentsListViewModel } from '../models/output_models/UserGetPostCommentsListViewModel';
import { get_version_post_input_view_model } from '../models/output_models/get_version_post_input_view_model';
import { check_version_result_view_model } from '../models/input_models/check_version_result_view_model';
import { get_info_post_result_view_model } from '../models/input_models/get_info_post_result_view_model';
import { users_get_follow_suggestion_list_result_view_model } from '../models/input_models/users_get_follow_suggestion_list_result_view_model.interface';
import { users_get_comment_likes_list_result_view_model } from '../models/input_models/users_get_comment_likes_list_result_view_model.interface';
import { user_create_basket_input_view_model } from '../models/output_models/user_create_basket_input_view_model';
import { user_create_basket_order_input_view_model } from '../models/output_models/user_create_basket_order_input_view_model';
import { users_basket_cancel_order_input_view_model } from '../models/output_models/users_basket_cancel_order_input_view_model';
import { users_search_stock_result_view_model } from '../models/input_models/users_search_stock_result_view_model.interface';
import { users_get_basket_order_history_input_view_model } from '../models/output_models/users_get_basket_order_history_input_view_model';
import { users_get_basket_order_history_result_view_model } from '../models/input_models/users_get_basket_order_history_result_view_model';
import { users_get_basket_data_result_view_model } from '../models/input_models/users_get_basket_data_result_view_model';
import { users_get_profile_baskets_view_model } from '../models/output_models/users_get_profile_baskets_view_model';
import { get_post_image_view_model } from '../models/output_models/get_post_image_view_model';
import { users_get_share_baskets_view_model } from '../models/input_models/users_get_share_baskets_view_model';
import { users_create_factor_wallet_input_view_model } from '../models/output_models/users_create_factor_wallet_input_view_model';
import { doing_job_return_info_result_view_model } from '../models/input_models/doing_job_return_info_result_view_model';
import { users_add_item_to_private_basket_input_view_model } from '../models/output_models/users_add_item_to_private_basket_input_view_model';
import { users_get_Private_baskets_result_view_model } from '../models/input_models/users_get_Private_baskets_result_view_model';
import { users_get_my_balance_result_view_model } from '../models/input_models/users_get_my_balance_result_view_model';
import { users_share_my_basket_view_model } from '../models/output_models/users_share_my_basket_view_model';
import { users_share_my_basket_result_view_model } from '../models/input_models/users_share_my_basket_result_view_model';
import { basket_share_package_input_view_model } from '../models/output_models/basket_share_package_input_view_model';
import { get_basket_share_package_input_view_model } from '../models/output_models/get_basket_share_package_input_view_model';
import { users_get_basket_share_packages_result_view_model } from '../models/input_models/users_get_basket_share_packages_result_view_model';
import { user_set_basket_item_price_view_model } from '../models/output_models/user_set_basket_item_price_view_model';
import { cafebazar_confirm_view_model } from '../models/output_models/cafebazar_confirm_view_model';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  private base_api_URL: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.base_api_URL = baseUrl + 'api/users';
  }

  public get_system_messages(): any {
    return this.http.get<privacy_result_model>(this.base_api_URL + '/get_system_messages/');
  }

  public get_terms_of_conditions(): any {
    return this.http.get<privacy_result_model>(this.base_api_URL + '/get_terms_of_conditions/');
  }

  public get_privacy_policy(): any {
    //return this.http.get<privacy_result_model>('https://localhost:44310/api/users//get_privacy_policy/');
    return this.http.get<privacy_result_model>(this.base_api_URL + '/get_privacy_policy/');
  }

  public check_username_availability(data: check_user_name_available_model): any {
    return this.http.post<check_user_name_available_result_model>(this.base_api_URL + '/check_username_availability/', data);
  }

  public get_profile_image(data: get_profile_data_model): any {
    return this.http.get('https://api.moneytoo.ir/api/users/get_profile_image?API_Key=' + data.API_Key + '&user_profile_id=' + data.user_profile_id + '&t=' + data.t, { responseType: 'blob' });
    //return this.http.get(this.base_api_URL + '/get_profile_image?API_Key=' + data.API_Key + '&user_profile_id=' + data.user_profile_id + '&t=' + data.t, { responseType: 'blob' });
  }

  public users_get_image(data: get_post_image_view_model): any {
    return this.http.get('https://api.moneytoo.ir/api/users/users_get_image?API_Key=' + data.API_Key + '&post_id=' + data.post_id + '&image_id=' + data.image_id, { responseType: 'blob' });
    //return this.http.get(this.base_api_URL + '/get_profile_image?API_Key=' + data.API_Key + '&user_profile_id=' + data.user_profile_id + '&t=' + data.t, { responseType: 'blob' });
  }

  public register(data: register_model): any {
    return this.http.post<register_result_model>(this.base_api_URL + '/register/', data);
  }

  public verify_by_mobile_number(data: verify_by_mobile_number_model): any {
    return this.http.post<verify_by_mobile_number_result_model>(this.base_api_URL + '/verify_user_session/', data);
  }

  public update_profile_data(data: update_profile_data_model): any {
    return this.http.post<update_profile_data_result_model>(this.base_api_URL + '/update_profile_data/', data);
  }

  public update_information_field_value(data: update_information_field_value_model): any {
    return this.http.post<update_profile_data_result_model>(this.base_api_URL + '/update_information_field_value/', data);
  }

  public follow(data: get_profile_data_model): any {
    return this.http.post<update_profile_data_result_model>(this.base_api_URL + '/follow/', data);
  }

  public unfollow(data: get_profile_data_model): any {
    return this.http.post<update_profile_data_result_model>(this.base_api_URL + '/unfollow/', data);
  }

  public get_profile_data(data: get_profile_data_model): any {
    return this.http.post<get_profile_data_result_model>(this.base_api_URL + '/get_profile_data/', data);
  }

  public users_get_profile_posts(data: get_user_follow_view_model): any {
    return this.http.post<user_get_profile_post_result_view_model>(this.base_api_URL + '/users_get_profile_posts/', data);
  }

  public users_search(data: search_profile_data_model): any {
    return this.http.post<users_search_result_view_model>(this.base_api_URL + '/users_search/', data);
  }

  public users_home_posts(data: get_post_by_index_view_model): any {
    return this.http.post<users_home_posts_result_view_model>(this.base_api_URL + '/users_home_posts/', data);
  }

  public users_explore_posts(data: get_post_by_index_view_model): any {
    return this.http.post<users_explore_posts_result_view_model>(this.base_api_URL + '/users_explore_posts/', data);
  }

  public users_get_single_post(data: get_post_view_model): any {
    return this.http.post<users_get_single_post_result_view_model>(this.base_api_URL + '/users_get_single_post/', data);
  }

  public users_like_post(data: get_post_view_model): any {
    return this.http.post<users_doing_works_result_int_result_view_model>(this.base_api_URL + '/users_like_post/', data);
  }

  public users_unlike_post(data: get_post_view_model): any {
    return this.http.post<users_doing_works_result_int_result_view_model>(this.base_api_URL + '/users_unlike_post/', data);
  }

  public users_get_profile_followers(data: get_user_follow_view_model): any {
    return this.http.post<users_get_profile_followers_result_view_model>(this.base_api_URL + '/users_get_profile_followers/', data);
  }

  public users_get_profile_followings(data: get_user_follow_view_model): any {
    return this.http.post<users_get_profile_followers_result_view_model>(this.base_api_URL + '/users_get_profile_followings/', data);
  }

  public users_get_likes_list(data: get_user_follow_view_model): any {
    return this.http.post<users_get_profile_followers_result_view_model>(this.base_api_URL + '/users_get_likes_list/', data);
  }

  public users_edit_post(data: get_post_view_model): any {
    return this.http.post<update_profile_data_result_model>(this.base_api_URL + '/users_edit_post/', data);
  }

  public users_delete_profile_picture(data: get_base_data_model): any {
    return this.http.post<delete_info_result_view_model>(this.base_api_URL + '/users_delete_profile_picture/', data);
  }

  public users_delete_post(data: get_post_view_model): any {
    return this.http.post<delete_info_result_view_model>(this.base_api_URL + '/users_delete_post/', data);
  }

  public users_get_price_packages(data: get_base_data_model): any {
    return this.http.post<users_get_price_packages_result_view_model>(this.base_api_URL + '/users_get_price_packages/', data);
  }

  public users_create_factor(data: users_create_factor_API_View_Model): any {
    return this.http.post<users_create_factor_result_view_model>(this.base_api_URL + '/users_create_factor/', data);
  }

  public users_get_notification_list(data: user_get_notification_API_View_Model): any {
    return this.http.post<users_get_notification_list_result_view_model>(this.base_api_URL + '/users_get_notification_list/', data);
  }

  public users_seen_notification_list(data: get_base_data_model): any {
    return this.http.post<update_profile_data_result_model>(this.base_api_URL + '/users_seen_notification_list/', data);
  }

  public users_send_comment(data: UserSendCommentViewModel): any {
    return this.http.post<add_data_result_view_model>(this.base_api_URL + '/users_send_comment/', data);
  }

  public users_delete_comment(data: CommentBaseViewModel): any {
    return this.http.post<delete_info_result_view_model>(this.base_api_URL + '/users_delete_comment/', data);
  }

  public users_report_post(data: PostBaseViewModel): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_report_post/', data);
  }

  public users_get_comment_reply_list(data: UserGetCommentReplyListViewModel): any {
    return this.http.post<user_get_profile_post_result_view_model>(this.base_api_URL + '/users_get_comment_reply_list/', data);
  }

  public users_get_post_comments_list(data: UserGetPostCommentsListViewModel): any {
    return this.http.post<user_get_profile_post_result_view_model>(this.base_api_URL + '/users_get_post_comments_list/', data);
  }

  public users_check_version(data: get_version_post_input_view_model): any {
    return this.http.post<check_version_result_view_model>(this.base_api_URL + '/users_check_version/', data);
  }

  public users_get_shared_accounts(data: get_base_data_model): any {
    return this.http.post<get_info_post_result_view_model>(this.base_api_URL + '/users_get_shared_accounts/', data);
  }

  public users_get_follow_suggestion_list(data: get_base_data_model): any {
    return this.http.post<users_get_follow_suggestion_list_result_view_model>(this.base_api_URL + '/users_get_follow_suggestion_list/', data);
  }

  public users_like_comment(data: CommentBaseViewModel): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_like_comment/', data);
  }

  public users_unlike_comment(data: CommentBaseViewModel): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_unlike_comment/', data);
  }

  public users_get_comment_likes_list(data: UserGetCommentReplyListViewModel): any {
    return this.http.post<users_get_comment_likes_list_result_view_model>(this.base_api_URL + '/users_get_comment_likes_list/', data);
  }

  public users_create_basket(data: user_create_basket_input_view_model): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_create_basket/', data);
  }

  public users_create_basket_order(data: user_create_basket_order_input_view_model): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_create_basket_order/', data);
  }

  public users_search_stock(data: search_profile_data_model): any {
    return this.http.post<users_search_stock_result_view_model>(this.base_api_URL + '/users_search_stock/', data);
  }

  public users_basket_cancel_order(data: users_basket_cancel_order_input_view_model): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_basket_cancel_order/', data);
  }

  public users_get_basket_order_history(data: users_get_basket_order_history_input_view_model): any {
    return this.http.post<users_get_basket_order_history_result_view_model>(this.base_api_URL + '/users_get_basket_order_history/', data);
  }

  public users_get_basket_data(data: users_get_basket_order_history_input_view_model): any {
    return this.http.post<users_get_basket_data_result_view_model>(this.base_api_URL + '/users_get_basket_data/', data);
  }

  public users_get_profile_baskets(data: users_get_profile_baskets_view_model): any {
    return this.http.post<users_get_basket_order_history_result_view_model>(this.base_api_URL + '/users_get_profile_baskets/', data);
  }

  public users_get_share_baskets(data: users_get_profile_baskets_view_model): any {
    return this.http.post<users_get_share_baskets_view_model>(this.base_api_URL + '/users_get_share_baskets/', data);
  }

  public users_create_factor_wallet(data: users_create_factor_wallet_input_view_model): any {
    return this.http.post<doing_job_return_info_result_view_model>(this.base_api_URL + '/users_create_factor_wallet/', data);
  }

  public users_add_item_to_private_basket(data: users_add_item_to_private_basket_input_view_model): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_add_item_to_private_basket/', data);
  }

  public users_get_Private_baskets(data: users_get_profile_baskets_view_model): any {
    return this.http.post<users_get_Private_baskets_result_view_model>(this.base_api_URL + '/users_get_Private_baskets/', data);
  }

  public users_get_my_balance(data: get_base_data_model): any {
    return this.http.post<users_get_my_balance_result_view_model>(this.base_api_URL + '/users_get_my_balance/', data);
  }

  public users_share_my_basket(data: users_share_my_basket_view_model): any {
    return this.http.post<users_share_my_basket_result_view_model>(this.base_api_URL + '/users_share_my_basket/', data);
  }

  public users_delete_basket_share_package(data: basket_share_package_input_view_model): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_delete_basket_share_package/', data);
  }

  public users_get_basket_share_packages(data: get_basket_share_package_input_view_model): any {
    return this.http.post<users_get_basket_share_packages_result_view_model>(this.base_api_URL + '/users_get_basket_share_packages/', data);
  }

  public users_buy_basket_share_packages(data: basket_share_package_input_view_model): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_buy_basket_share_packages/', data);
  }

  public users_set_basket_item_price(data: user_set_basket_item_price_view_model): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/users_set_basket_item_price/', data);
  }

  public cafebazar_payment_confirm(data: cafebazar_confirm_view_model): any {
    return this.http.post<doing_job_result_view_model>(this.base_api_URL + '/cafebazar_payment_confirm/', data);
  }
}
