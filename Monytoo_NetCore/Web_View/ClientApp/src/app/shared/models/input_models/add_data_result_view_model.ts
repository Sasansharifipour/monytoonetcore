import { result_model } from "./result_model.interface";

export interface add_data_result_view_model extends result_model {
  is_added: boolean;
}
