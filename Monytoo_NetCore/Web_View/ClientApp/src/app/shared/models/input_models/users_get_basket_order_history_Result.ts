export interface users_get_basket_order_history_Result {
  order_id: number;
  create_time: Date;
  namad_name: string;
  count: number;
  offer_price: number;
  status_id: number;
  r_number: number;
}
