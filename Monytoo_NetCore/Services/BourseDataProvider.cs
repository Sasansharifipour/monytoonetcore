﻿using DomainClasses.Instrument;
using DomainClasses.Web_Services;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CommonCode;

namespace Services
{
    public class BourseDataProvider : IDataProvider
    {
        private ITsetmcService _tsetmcService;
        private IInstrumentService _instrumentService;
        private ILogger _logger;

        public BourseDataProvider(ITsetmcService tsetmcService, IInstrumentService instrumentService, ILogger logger)
        {
            _tsetmcService = tsetmcService;
            _instrumentService = instrumentService;
            _logger = logger;
        }

        private async void load_min_max_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads)
        {
            List<Task<List<Static_Thresholds_Result_View_Model>>> min_max_Results = new List<Task<List<Static_Thresholds_Result_View_Model>>>();

            for (byte i = 0; i < 8; i++)
            {
                min_max_Results.Add(_tsetmcService.Get_StaticThresholds_From_Source(i));
                System.Threading.Thread.Sleep(2000);
            }

            while (min_max_Results.Any())
            {
                Task<List<Static_Thresholds_Result_View_Model>> finishedTask = await Task.WhenAny(min_max_Results);
                min_max_Results.Remove(finishedTask);
                update_min_max_for_all_instruments(user_id, secret_key, all_namads, await finishedTask);
            }

            Task.WaitAll(min_max_Results.ToArray());
        }

        private void update_min_max_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads,
            List<Static_Thresholds_Result_View_Model> data)
        {
            try
            {
                if (data == null || data.Count == 0)
                    return;

                List<Instrument_Update_Information_Field_View_Model> all_data = new List<Instrument_Update_Information_Field_View_Model>();

                foreach (var item in data)
                {
                    var namad = all_namads.FirstOrDefault(s => s.InsCode.Trim() == item.InsCode.ToString().Trim());

                    if (namad == null || namad.namad_id <= 0)
                        continue;

                    var view_model = new Instrument_Price_View_Model()
                    {
                        namad_id = namad.namad_id,
                        InsCode = item.InsCode
                    };

                    if (item != null && item.InsCode != 0)
                    {
                        view_model.ENGINE_PRICE_MAX_VALUE = item.PSGelStaMax.ToString();
                        view_model.ENGINE_PRICE_MIN_VALUE = item.PSGelStaMin.ToString();
                    }

                    all_data.AddRange(view_model.Convert_To_Update_Model());
                }

                _instrumentService.update_instrument_info_list(user_id, secret_key, all_data);
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BourseDataProvider", "update_status_for_all_instruments", e.Message));

            }
        }

        private async void load_status_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads)
        {
            List<Task<List<InstrumentsState_Result>>> state_Results = new List<Task<List<InstrumentsState_Result>>>();

            for (byte i = 0; i < 8; i++)
            {
                state_Results.Add(_tsetmcService.Get_InstrumentsState_From_Source(i));
                System.Threading.Thread.Sleep(2000);
            }

            while (state_Results.Any())
            {
                Task<List<InstrumentsState_Result>> finishedTask = await Task.WhenAny(state_Results);
                state_Results.Remove(finishedTask);
                update_status_for_all_instruments(user_id, secret_key, all_namads, await finishedTask);
            }

            Task.WaitAll(state_Results.ToArray());
        }

        private void update_status_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads,
            List<InstrumentsState_Result> data)
        {
            try
            {
                if (data == null || data.Count == 0)
                    return;

                List<Instrument_Update_Information_Field_View_Model> all_data = new List<Instrument_Update_Information_Field_View_Model>();

                foreach (var item in data)
                {
                    var namad = all_namads.FirstOrDefault(s => s.InsCode.Trim() == item.InsCode.ToString().Trim());

                    if (namad == null || namad.namad_id <= 0)
                        continue;

                    var view_model = new Instrument_Price_View_Model()
                    {
                        namad_id = namad.namad_id,
                        InsCode = item.InsCode
                    };

                    if (item == null || item.InsCode == 0)
                    {
                        view_model.ENGINE_Price_Reader_Namad_Status = CommonCodes.get_instrument_status("O");
                        view_model.ENGINE_Price_Reader_Namad_Status_id = "O";
                    }
                    else
                    {
                        view_model.ENGINE_Price_Reader_Namad_Status = CommonCodes.get_instrument_status(item.CEtaVal.Trim());
                        view_model.ENGINE_Price_Reader_Namad_Status_id = item.CEtaVal.Trim();
                    }

                    all_data.AddRange(view_model.Convert_To_Update_Model());
                }

                _instrumentService.update_instrument_info_list(user_id, secret_key, all_data);
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BourseDataProvider", "update_status_for_all_instruments", e.Message));

            }
        }

        private async void load_trade_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads)
        {
            List<Task<List<Trade_Last_Day_Result_View_Model>>> state_Results = new List<Task<List<Trade_Last_Day_Result_View_Model>>>();

            for (byte i = 0; i < 8; i++)
            {
                state_Results.Add(_tsetmcService.Get_TradeLastDay_From_Source(i));
                System.Threading.Thread.Sleep(2000);
            }

            while (state_Results.Any())
            {
                Task<List<Trade_Last_Day_Result_View_Model>> finishedTask = await Task.WhenAny(state_Results);
                state_Results.Remove(finishedTask);
                update_trade_for_all_instruments(user_id, secret_key, all_namads, await finishedTask);
            }

            Task.WaitAll(state_Results.ToArray());
        }

        private void update_trade_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads,
            List<Trade_Last_Day_Result_View_Model> data)
        {
            try
            {
                if (data == null || data.Count == 0)
                    return;

                List<Instrument_Update_Information_Field_View_Model> all_data = new List<Instrument_Update_Information_Field_View_Model>();

                foreach (var item in data)
                {
                    var namad = all_namads.FirstOrDefault(s => s.InsCode.Trim() == item.InsCode.ToString().Trim());

                    if (namad == null || namad.namad_id <= 0)
                        continue;

                    var pclosing_change = item.PClosing - item.PriceYesterday;

                    var pclosing_change_percent = Math.Round((pclosing_change / item.PriceYesterday) * 100, 2);

                    var price_change_percent = Math.Round((item.PriceChange / item.PriceYesterday) * 100, 2);

                    var view_model = new Instrument_Price_View_Model()
                    {
                        namad_id = namad.namad_id,
                        InsCode = item.InsCode,
                        ENGINE_Price_Reader_Last_Price = item.PDrCotVal.ToString(),
                        ENGINE_Price_Reader_Close_Price = item.PClosing.ToString(),
                        ENGINE_Price_Reader_Volume = item.QTotTran5J.ToString(),
                        ENGINE_Price_Reader_Fluctuation = item.PriceChange.ToString(),
                        ENGINE_Price_Reader_Last_Price_Change = price_change_percent.ToString(),
                        ENGINE_Price_Reader_Close_Price_Change = pclosing_change_percent.ToString()
                    };


                    all_data.AddRange(view_model.Convert_To_Update_Model());
                }

                _instrumentService.update_instrument_info_list(user_id, secret_key, all_data);
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BourseDataProvider", "update_trade_for_all_instruments", e.Message));
            }
        }

        private async void load_base_info_for_all_instruments(long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads)
        {
            List<Task<List<Instrument_Result>>> data_Results = new List<Task<List<Instrument_Result>>>();

            for (byte i = 0; i < 8; i++)
            {
                data_Results.Add(_tsetmcService.Get_Instrument_From_Source(i));
                System.Threading.Thread.Sleep(2000);
            }

            while (data_Results.Any())
            {
                Task<List<Instrument_Result>> finishedTask = await Task.WhenAny(data_Results);
                data_Results.Remove(finishedTask);
                update_info_for_all_instruments(user_id, secret_key, all_namads, await finishedTask);
            }

            Task.WaitAll(data_Results.ToArray());
        }

        private void update_info_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads,
            List<Instrument_Result> data)
        {
            try
            {
                if (data == null || data.Count == 0)
                    return;

                List<Instrument_Update_Information_Field_View_Model> all_data = new List<Instrument_Update_Information_Field_View_Model>();

                foreach (var item in data)
                {
                    var namad = all_namads.FirstOrDefault(s => s.InsCode.Trim() == item.InsCode.ToString().Trim());

                    if (namad == null || namad.namad_id <= 0)
                        continue;

                    var view_model = new Instrument_Price_View_Model()
                    {
                        namad_id = namad.namad_id,
                        InsCode = item.InsCode
                    };


                    view_model.ENGINE_Price_Reader_Namad_Type = CommonCodes.get_namad_noee_string(item.YVal);
                    view_model.ENGINE_Price_Reader_Base_Volume = item.BaseVol.ToString();
                    view_model.ENGINE_Price_Reader_Market_Type = CommonCodes.get_bazar_noee_string(item.Flow);

                    all_data.AddRange(view_model.Convert_To_Update_Model());
                }

                _instrumentService.update_instrument_info_list(user_id, secret_key, all_data);
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BourseDataProvider", "update_info_for_all_instruments", e.Message));
            }
        }

        private async void load_client_type_info_for_all_instruments(long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads)
        {
            List<Task<List<ClientType_Result>>> data_Results = new List<Task<List<ClientType_Result>>>();

            data_Results.Add(_tsetmcService.Get_ClientType_From_Source());
            System.Threading.Thread.Sleep(2000);

            while (data_Results.Any())
            {
                Task<List<ClientType_Result>> finishedTask = await Task.WhenAny(data_Results);
                data_Results.Remove(finishedTask);
                update_client_type_info_for_all_instruments(user_id, secret_key, all_namads, await finishedTask);
            }

            Task.WaitAll(data_Results.ToArray());
        }

        private void update_client_type_info_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads,
            List<ClientType_Result> data)
        {
            try
            {
                if (data == null || data.Count == 0)
                    return;

                List<Instrument_Update_Information_Field_View_Model> all_data = new List<Instrument_Update_Information_Field_View_Model>();

                foreach (var item in data)
                {
                    var namad = all_namads.FirstOrDefault(s => s.InsCode.Trim() == item.InsCode.ToString().Trim());

                    if (namad == null || namad.namad_id <= 0)
                        continue;

                    var view_model = new Instrument_Price_View_Model()
                    {
                        namad_id = namad.namad_id,
                        InsCode = item.InsCode,
                        ENGINE_Price_Reader_Haghighi_Volume_Buy = item.Buy_I_Volume.ToString(),
                        ENGINE_Price_Reader_Haghighi_Volume_Sell = item.Sell_I_Volume.ToString(),
                        ENGINE_Price_Reader_Haghighi_Fluctuation_Buy = item.get_Buy_I_Percentage().ToString(),
                        ENGINE_Price_Reader_Haghighi_Fluctuation_Sell = item.get_Sell_I_Percentage().ToString(),
                        ENGINE_Price_Reader_Haghighi_Count_Buy = item.Buy_CountI.ToString(),
                        ENGINE_Price_Reader_Haghighi_Count_Sell = item.Sell_CountI.ToString(),
                        ENGINE_Price_Reader_Hoghoghi_Volume_Buy = item.Buy_N_Volume.ToString(),
                        ENGINE_Price_Reader_Hoghoghi_Volume_Sell = item.Sell_N_Volume.ToString(),
                        ENGINE_Price_Reader_Hoghoghi_Fluctuation_Buy = item.get_Buy_N_Percentage().ToString(),
                        ENGINE_Price_Reader_Hoghoghi_Fluctuation_Sell = item.get_Sell_N_Percentage().ToString(),
                        ENGINE_Price_Reader_Hoghoghi_Count_Buy = item.Buy_CountN.ToString(),
                        ENGINE_Price_Reader_Hoghoghi_Count_Sell = item.Sell_CountN.ToString(),
                        ENGINE_Namad_Current_Trade_Volume = (item.Sell_I_Volume + item.Sell_N_Volume).ToString()
                    };

                    all_data.AddRange(view_model.Convert_To_Update_Model());
                }

                _instrumentService.update_instrument_info_list(user_id, secret_key, all_data);
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BourseDataProvider", "update_client_type_info_for_all_instruments", e.Message));

            }
        }

        public async Task update_info_for_all_instrumentsAsync(long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads)
        {
            var tasks = new List<Task>();

            tasks.Add(Task.Run(() => load_min_max_for_all_instruments(user_id, secret_key, all_namads)));
            tasks.Add(Task.Run(() => load_status_for_all_instruments(user_id, secret_key, all_namads)));
            tasks.Add(Task.Run(() => load_base_info_for_all_instruments(user_id, secret_key, all_namads)));
            tasks.Add(Task.Run(() => load_trade_for_all_instruments(user_id, secret_key, all_namads)));
            tasks.Add(Task.Run(() => load_client_type_info_for_all_instruments(user_id, secret_key, all_namads)));
            tasks.Add(Task.Run(() => load_BestLimitOneIns_for_all_instruments(user_id, secret_key, all_namads)));

            Task.WaitAll(tasks.ToArray());
        }

        private async void load_BestLimitOneIns_for_all_instruments(
            long user_id, string secret_key,
            List<Engine_get_all_namad_Result> all_namads)
        {
            try
            {
                List<Task<List<BestLimitOneIns_Result_View_Model>>> state_Results = new List<Task<List<BestLimitOneIns_Result_View_Model>>>();
                List<Instrument_Update_Information_Field_View_Model> all_data = new List<Instrument_Update_Information_Field_View_Model>();

                foreach (var item in all_namads)
                {
                    long ins_code = 0;
                    long.TryParse(item.InsCode, out ins_code);

                    state_Results.Add(_tsetmcService.Get_Best_Limit_One_Instrument(ins_code));
                }

                while (state_Results.Any())
                {
                    Task<List<BestLimitOneIns_Result_View_Model>> finishedTask = await Task.WhenAny(state_Results);
                    state_Results.Remove(finishedTask);

                    long sum_buy = 0;
                    long sum_sell = 0;

                    decimal min_buy = 0;
                    decimal min_sell = 0;

                    decimal max_buy = 0;
                    decimal max_sell = 0;
                    long ins_code = 0;
                    int i = 0;

                    foreach (var data in finishedTask.Result)
                    {
                        ins_code = data.InsCode;
                        sum_buy += data.QTitMeDem;
                        sum_sell += data.QTitMeOf;

                        if (i == 0)
                        {
                            min_buy = data.PMeDem;
                            min_sell = data.PMeOf;

                            max_buy = data.PMeDem;
                            max_sell = data.PMeOf;
                        }

                        if (min_buy > data.PMeDem) min_buy = data.PMeDem;
                        if (max_buy < data.PMeDem) max_buy = data.PMeDem;

                        if (min_sell > data.PMeOf) min_sell = data.PMeOf;
                        if (max_sell < data.PMeOf) max_sell = data.PMeOf;
                    }

                    var data_model = all_namads.FirstOrDefault(s => s.InsCode == ins_code.ToString());

                    if (data_model != null && data_model.InsCode != "")
                    {
                        var view_model = new Instrument_Price_View_Model()
                        {
                            namad_id = data_model.namad_id,
                            InsCode = ins_code
                        };

                        view_model.ENGINE_Namad_Queue_Difficulty_Buy = (sum_buy / (max_buy - min_buy + 1)).ToString();
                        view_model.ENGINE_Namad_Queue_Difficulty_Sell = (sum_sell / (max_sell - min_sell + 1)).ToString();

                        all_data.AddRange(view_model.Convert_To_Update_Model());
                    }
                }

                Task.WaitAll(state_Results.ToArray());

                _instrumentService.update_instrument_info_list(user_id, secret_key, all_data);
            }
            catch (Exception ex)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2}", "BourseDataProvider", "load_BestLimitOneIns_for_all_instruments", ex.Message));
            }
        }

        public Instrument_Price_View_Model get_Instruments_Price(long Instrument_Code)
        {
            return new Instrument_Price_View_Model();
        }

        public Instrument_Price_View_Model get_Instruments_Trade_One_Day(long Instrument_Code, DateTime datetime)
        {
            try
            {
                var instrumentsState_Results = _tsetmcService.Get_Instruments_State_In_Special_Date_From_Source((int)Instrument_Code, datetime);
                var tradeOneDayAll_Results = _tsetmcService.get_Trade_In_Date(Instrument_Code, datetime);

                if (tradeOneDayAll_Results == null)
                    return new Instrument_Price_View_Model();

                var instrument_Price_View_Models = new Instrument_Price_View_Model()
                {
                    InsCode = tradeOneDayAll_Results.InsCode,
                    ENGINE_Price_Reader_Last_Price = tradeOneDayAll_Results.PDrCotVal.ToString(),
                    ENGINE_Price_Reader_Close_Price = tradeOneDayAll_Results.PClosing.ToString(),
                    ENGINE_Price_Reader_Volume = tradeOneDayAll_Results.QTotTran5J.ToString(),
                    ENGINE_Price_Reader_Fluctuation = tradeOneDayAll_Results.PriceChange.ToString()
                };


                if (instrumentsState_Results.Result == null || instrumentsState_Results.Result.InsCode == 0)
                {
                    instrument_Price_View_Models.ENGINE_Price_Reader_Namad_Status = CommonCodes.get_instrument_status("O");
                    instrument_Price_View_Models.ENGINE_Price_Reader_Namad_Status_id = "O";
                }
                else
                {
                    instrument_Price_View_Models.ENGINE_Price_Reader_Namad_Status = CommonCodes.get_instrument_status(instrumentsState_Results.Result.CEtaVal.Trim());
                    instrument_Price_View_Models.ENGINE_Price_Reader_Namad_Status_id = instrumentsState_Results.Result.CEtaVal.Trim();
                }

                if (instrument_Price_View_Models == null)
                    instrument_Price_View_Models = new Instrument_Price_View_Model();

                return instrument_Price_View_Models;
            }
            catch (Exception e)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "BourseDataProvider", "get_Instruments_Trade_One_Day", e.Message));
                return null;
            }
        }

        public List<Instrument_Trade_View_Model> get_Trade_From_Date_To_Date(long Instrument_Code, DateTime from_datetime, DateTime to_datetime)
        {
            return _tsetmcService.get_Trade_From_Date_To_Date(Instrument_Code, from_datetime, to_datetime);
        }

        public async Task<List<Instrument_Result>> get_all_instruments()
        {
            List<Task<List<Instrument_Result>>> tasks = new List<Task<List<Instrument_Result>>>();
            List<Instrument_Result> data = new List<Instrument_Result>();

            for (int i = 0; i < 8; i++)
                tasks.Add(_tsetmcService.Get_Instrument_From_Source((byte)i));

            while (tasks.Any())
            {
                var finishedTask = await Task.WhenAny(tasks);
                tasks.Remove(finishedTask);
                data.AddRange(await finishedTask);
            }

            return data;
        }
    }
}