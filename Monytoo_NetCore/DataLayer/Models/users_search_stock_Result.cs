﻿using System;

namespace DataLayer.Models
{
    public class users_search_stock_Result
    {
        public long ID { get; set; }
        public Nullable<int> type_id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public Nullable<long> profile_image_t { get; set; }
        public string max_value { get; set; }
        public string min_value { get; set; }
        public string namad_status { get; set; }
        public string last_price { get; set; }
    }
}
