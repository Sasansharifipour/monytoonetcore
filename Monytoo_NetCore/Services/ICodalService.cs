﻿using CommonCode;
using DomainClasses.Common;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.IO;

namespace Services
{
    public interface ICodalService
    {
        void post_all_codal_not_posted();
    }

    public class CodalService : ICodalService
    {
        private IDbContextFactory<connectionString> _contextFactory;
        private IImageService _imageService;
        private IuserServices _userService;
        private IPostService _postService;
        private ILogger _logger;
        long user_id = 0;
        string secret_key = "";

        public CodalService(IImageService imageService, IuserServices userService
            , IDbContextFactory<connectionString> contextFactory, IPostService postService, ILogger logger)
        {
            _contextFactory = contextFactory;
            _imageService = imageService;
            _userService = userService;
            _postService = postService;
            _logger = logger;
            (user_id, secret_key) = Config.get_user_info_from_config("codal");
        }

        public void post_all_codal_not_posted()
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    var codal_posts = _db.Engine_Codal_Temp.Where(s => s.Post_Id == null).ToList();

                    foreach (var item in codal_posts)
                        post_codal(item);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void post_codal(Engine_Codal_Temp value)
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    Engine_Codal_Temp data = _db.Engine_Codal_Temp.FirstOrDefault(s => s.ID == value.ID);
                    if (data == null || data.ID <= 0 || data.Post_Id != null || data.Post_Id > 0)
                        return;

                    long from_user_id = user_id;

                    var possible_namads = _userService.users_search(new DomainClasses.Users.search_profile_data_model()
                    {
                        search_str = data.Namad_name,
                        API_Key = String.Format("{0}-{1}", user_id, secret_key)
                    });

                    if (possible_namads != null && possible_namads.Informations != null)
                    {
                        List<users_search_Result> users_Search_Results = (List<users_search_Result>)possible_namads.Informations;

                        if (users_Search_Results != null && users_Search_Results.Count > 0)
                            from_user_id = users_Search_Results[0].ID;
                    }

                    Random rand = new Random();
                    int idx = rand.Next(1, 6);

                    string template_image_path = Config.get_image_path_for_codal_post(idx);

                    var texts = preparingTexts(data, idx);
                    string caption = string.Format("{0}: {1} \n {2}", data.Namad_name, data.Title, data.URL);

                    var image = ImageUtil.generate_image_with_text(template_image_path, texts);

                    if (image != null)
                    {
                        string image_path = _imageService.save_image(image, CommonCodes.saving_image_location.temp_image);

                        if (!image_path.IsNullOrEmptyOrWhiteSpace())
                        {
                            image_path = Config.get_root_path() + image_path;
                            string API_Key = String.Format("{0}-{1}", user_id, secret_key);

                            string file_name = Path.GetFileName(image_path);

                            var file_data = File.ReadAllBytes(image_path);

                            var result = _postService.post("http://Api.moneytoo.ir/api/users/send_post", file_name, file_data,
                                API_Key, caption, from_user_id.ToString());

                            int result_update_db = result.Post_id;

                            if (result_update_db > 0)
                            {
                                data.Post_Id = result_update_db;
                                _db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.save_log(string.Format("File : {0} - Method : {1} - Exception : {2} = ", "CodalServices", "post_codal", ex.Message));
            }
        }

        private List<PreparingText> preparingTexts(Engine_Codal_Temp data, int idx)
        {
            List<string> colors = new List<string> { "071a41", "3d003a", "023502", "01604d", "540402", "180e4d" };
            List<PreparingText> texts = new List<PreparingText>();
            string font_name = "IRAN Sans";

            PreparingText preparing = new PreparingText()
            {
                Title = data.Namad_name,
                Font_Color = colors[idx - 1],
                Font_Style = FontStyle.Bold,
                Font_Size = ImageUtil.get_suitable_font_size_for_codal(data.Namad_name, font_name, new SizeF(170, 120), 32),
                Font_Name = font_name,
                X = 785,
                Y = 780
            };

            texts.Add(preparing);


            DateTime d = data.Insert_date;
            PersianCalendar pc = new PersianCalendar();
            string persian_date = string.Format("{0}/{1}/{2}", pc.GetYear(d), pc.GetMonth(d), pc.GetDayOfMonth(d));

            PreparingText date_text = new PreparingText()
            {
                Title = persian_date,
                Font_Color = "ffffff",
                Font_Style = FontStyle.Bold,
                Font_Size = 10,
                Font_Name = "B Mitra",
                X = 70,
                Y = 970
            };

            texts.Add(date_text);
            return texts;
        }
    }
}