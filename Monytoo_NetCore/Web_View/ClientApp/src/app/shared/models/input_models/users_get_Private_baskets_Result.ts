export interface users_get_Private_baskets_Result {
  iD: number;
  name: string;
  total_value: number;
  create_time: Date;
  last_evaluation_time: Date;
  is_shareable: boolean;
  item_count: number;
}
