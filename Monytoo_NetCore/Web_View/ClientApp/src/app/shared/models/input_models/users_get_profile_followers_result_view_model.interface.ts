import { result_model } from "./result_model.interface";
import { users_get_profile_followers_Result } from "./users_get_profile_followers_Result.interface";

export interface users_get_profile_followers_result_view_model extends result_model {
  informations: users_get_profile_followers_Result[];
}

