import { get_base_data_model } from "./get_base_data_model.interface";

export interface users_create_factor_wallet_input_view_model extends get_base_data_model {
  i_data: string;
  price: number;
  payment_gateway_id: number;
}
