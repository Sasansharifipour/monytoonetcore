import { get_base_data_model } from "./get_base_data_model.interface";

export interface users_get_profile_baskets_view_model extends get_base_data_model {
  profile_user_id: number;
}
