﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class user_set_basket_item_price_view_model : get_base_data_model
    {
        public int basket_item_id { get; set; } = 0;

        public float current_price { get; set; } = 0;
    }
}
