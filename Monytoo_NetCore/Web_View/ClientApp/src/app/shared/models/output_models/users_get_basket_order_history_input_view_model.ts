import { get_base_data_model } from "./get_base_data_model.interface";

export interface users_get_basket_order_history_input_view_model extends get_base_data_model {
  basket_id: number;
  index: number;
  count: number;
}
