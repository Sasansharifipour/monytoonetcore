﻿using System;

namespace DataLayer.Models
{
    public class users_search_Result
    {
        public long ID { get; set; }
        public Nullable<int> type_id { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public Nullable<long> profile_image_t { get; set; }
    }
}
