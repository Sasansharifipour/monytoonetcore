﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Output_API_View_Model
{
    public class add_comment_result_view_model : add_data_result_view_model
    {
        public object Information { get; set; }
    }
}