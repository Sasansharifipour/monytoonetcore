import { get_base_data_model } from "./get_base_data_model.interface";

export interface UserSendCommentViewModel extends get_base_data_model {
  Text: string;
  Reply_to_comment_id: number;
  Post_id: number;
}
