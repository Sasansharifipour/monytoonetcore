export interface register_model {

  mobile_number: string;

  username: string;

  name: string;

  lastname: string;

  firebase_id: string;

  invitation_code: string;

}
