﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using DomainClasses.TGJU;

namespace Services
{
    public interface ITGJUService
    {
        List<TGJU_Data_View_Model> load_data();

        void load_and_save_data();

        bool add_namad(TGJU_Data_View_Model item);
    }

    public class TGJUService : ITGJUService
    {
        IuserServices _userService;

        public TGJUService(IuserServices userService)
        {
            _userService = userService;
        }

        public bool add_namad(TGJU_Data_View_Model item)
        {
            if (item == null || item.id <= 0)
                return false;

            string unique_username = string.Format("TGJU_{0}", item.id);

            int cnt = 0;
            bool added = false;

            while (!added && ++cnt < 10)
                added = _userService.Save_user_infoAsync(item.id.ToString(), unique_username, item.title, "");

            return added;
        }

        public async Task<Request_Result_View_Model> load_currency_data()
        {
            HttpClient client = new HttpClient();
            string path = "https://gateway.accessban.com/public/web-service/list/price?format=json&limit=30000&page=1";

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZ2F0ZXdheS5hY2Nlc3NiYW4uY29tXC9wdWJsaWMiLCJzdWIiOiJhYWUyM2Y3Mi1mMjNkLTUwMTItOWNjNy03OTlkNGI0YTU4NmQiLCJpYXQiOjE2MDY3MzgzNjUsImV4cCI6MTc2NDUwNDc2NSwibmFtZSI6Ik1vc3RhZmEgS2hha2ktMTE2NzIifQ.t8hite_QXWTEZHLoePhn_AVBzEGFEWSZb-xWM4qzs0A");

            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                    return await JsonSerializer.DeserializeAsync<Request_Result_View_Model>(await response.Content.ReadAsStreamAsync());
            }
            catch (Exception e)
            {
                return new Request_Result_View_Model();
            }
            return new Request_Result_View_Model();
        }

        public async Task<Request_Result_View_Model> load_gold_coin_data()
        {
            HttpClient client = new HttpClient();
            string path = "https://gateway.accessban.com/public/web-service/list/gold?format=json&limit=30000&page=1";

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZ2F0ZXdheS5hY2Nlc3NiYW4uY29tXC9wdWJsaWMiLCJzdWIiOiJhYWUyM2Y3Mi1mMjNkLTUwMTItOWNjNy03OTlkNGI0YTU4NmQiLCJpYXQiOjE2MDY3MzgzNjksImV4cCI6MTc2NDUwNDc2OSwibmFtZSI6Ik1vc3RhZmEgS2hha2ktMTE2NzIifQ.LFzOzgn1Xuq6GCBhf5-ihHV4YKOXuYo9UaZ13Ue558c");

            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                    return await JsonSerializer.DeserializeAsync<Request_Result_View_Model>(await response.Content.ReadAsStreamAsync());
            }
            catch (Exception e)
            {
                return new Request_Result_View_Model();
            }
            return new Request_Result_View_Model();
        }

        public async Task<Request_Result_View_Model> load_base_data()
        {
            HttpClient client = new HttpClient();
            string path = "https://gateway.accessban.com/public/web-service/list/common?format=json&limit=30000&page=1";

            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvZ2F0ZXdheS5hY2Nlc3NiYW4uY29tXC9wdWJsaWMiLCJzdWIiOiJhYWUyM2Y3Mi1mMjNkLTUwMTItOWNjNy03OTlkNGI0YTU4NmQiLCJpYXQiOjE2MDY3MzgzNjEsImV4cCI6MTc2NDUwNDc2MSwibmFtZSI6Ik1vc3RhZmEgS2hha2ktMTE2NzIifQ.R0xsyMrKpmb06S3sfFCZL3OObbPDBCytJail6dEoDv4");

            try
            {
                HttpResponseMessage response = client.GetAsync(path).Result;

                if (response.IsSuccessStatusCode)
                    return await JsonSerializer.DeserializeAsync<Request_Result_View_Model>(await response.Content.ReadAsStreamAsync());
            }
            catch (Exception e)
            {
                return new Request_Result_View_Model();
            }
            return new Request_Result_View_Model();
        }

        public List<TGJU_Data_View_Model> load_data()
        {
            List<TGJU_Data_View_Model> all_data = new List<TGJU_Data_View_Model>();
            var base_data = load_base_data().Result;

            if (base_data != null && base_data.data != null && base_data.data.Count > 0)
                all_data.AddRange(base_data.data);

            var currency_data = load_currency_data().Result;

            if (currency_data != null && currency_data.data != null && currency_data.data.Count > 0)
                all_data.AddRange(currency_data.data);

            var gold_coin__data = load_gold_coin_data().Result;

            if (gold_coin__data != null && gold_coin__data.data != null && gold_coin__data.data.Count > 0)
                all_data.AddRange(gold_coin__data.data);

            return all_data;
        }

        public void load_and_save_data()
        {
            var data = load_data();

            foreach (var item in data)
                add_namad(item);
        }
    }
}
