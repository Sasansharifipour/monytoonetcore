import { Component, OnInit, Input } from '@angular/core';
import { users_get_basket_order_history_Result } from '../shared/models/input_models/users_get_basket_order_history_Result';
import { users_get_profile_baskets_output_view_model } from '../shared/models/input_models/users_get_profile_baskets_output_view_model';

@Component({
  selector: 'app-basket-details',
  templateUrl: './basket-details.component.html'
})
export class BasketDetailsComponent implements OnInit  {

  _basket?: users_get_profile_baskets_output_view_model;

  get basket(): users_get_profile_baskets_output_view_model {
    return this._basket;
  }

  @Input() set basket(value: users_get_profile_baskets_output_view_model) {
    this._basket = value;

    this.basket_options.series[0].data = [];

    this._basket.history.forEach(item => {
      this.basket_options.series[0].data.push(item.daily_profit);
    });

  }

  constructor() { }

  basket_options = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category'
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series: [
      {
        name: 'سود',
        type: 'line',
        stack: 'counts',
        areaStyle: { normal: {} },
        data: []
      }
    ]
  };

  ngOnInit(): void {
  }
}

