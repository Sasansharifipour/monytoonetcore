﻿using Microsoft.AspNetCore.Http;

namespace DomainClasses.Users
{
    public class update_profile_image_view_model
    {
        //public HttpPostedFile image { get; set; }

        public IFormFile image { get; set; }

        public string API_Key { get; set; }
    }
}