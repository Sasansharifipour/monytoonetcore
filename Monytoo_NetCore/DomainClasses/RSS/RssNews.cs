﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.RSS
{
    public class RssNews
    {
        public string Title { get; set; } = "";

        public string Description { get; set; } = "";

        public string Link { get; set; } = "";

        public DateTime Date { get; set; } = DateTime.Now;

        public string Image_path { get; set; } = "";

        public string Rss_URL { get; set; } = "";
    }
}