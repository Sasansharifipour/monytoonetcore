export interface users_create_factor_Result {
  factor_Id: number;
  payment_Link: string;
}
