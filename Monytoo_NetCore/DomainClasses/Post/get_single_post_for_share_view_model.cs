﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Post
{
    public class get_single_post_for_share_view_model
    {
        public string username { get; set; } = "";

        public string caption { get; set; } = "";

        public int like_count { get; set; } = 0;

        public string comment_text { get; set; } = "";

        public int comment_count { get; set; } = 0;

        public string reply_username { get; set; } = "";

        public string reply_text { get; set; } = "";

        public string comment_username { get; set; } = "";

        public byte[] profile_image { get; set; }

        public byte[] post_image { get; set; }

    }
}
