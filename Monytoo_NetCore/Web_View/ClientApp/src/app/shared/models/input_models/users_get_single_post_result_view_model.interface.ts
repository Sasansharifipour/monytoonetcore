import { result_model } from "./result_model.interface";
import { users_get_single_post_Result } from "./users_get_single_post_Result.interface";

export interface users_get_single_post_result_view_model extends result_model {
  informations: users_get_single_post_Result;
}

