import { result_model } from "./result_model.interface";

export interface verify_by_mobile_number_result_model extends result_model {
  apI_KEY: string;
  user_id: number;
}
