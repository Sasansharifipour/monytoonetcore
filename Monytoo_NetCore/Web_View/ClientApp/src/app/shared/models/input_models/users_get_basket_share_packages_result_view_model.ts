import { result_model } from "./result_model.interface";
import { users_get_basket_share_packages_Result } from "./users_get_basket_share_packages_Result";

export interface users_get_basket_share_packages_result_view_model extends result_model {
  informations: users_get_basket_share_packages_Result[];
}

