﻿using System;

namespace DataLayer.Models
{
    public partial class users_get_basket_share_packages_Result
    {
        public long ID { get; set; }
        public string name { get; set; }
        public Nullable<int> Duration { get; set; }
        public Nullable<int> Price { get; set; }
    }
}
