﻿using System.Drawing;

namespace DomainClasses.Common
{
    public class PreparingText
    {
        public string Title { get; set; } = "";

        public string Font_Name { get; set; } = "";

        public int Font_Size { get; set; } = 0;

        public string Font_Color { get; set; } = "";

        public FontStyle Font_Style { get; set; } = FontStyle.Regular;

        public int X { get; set; } = 0;

        public int Y { get; set; } = 0;

        public int max_width { get; set; } = 0;

        public int max_height { get; set; } = 0;

        public Font GetFont()
        {
            return new Font(Font_Name, Font_Size, Font_Style);
        }

        public Color GetColor()
        {
            return ColorTranslator.FromHtml("#" + Font_Color);
        }
    }
}
