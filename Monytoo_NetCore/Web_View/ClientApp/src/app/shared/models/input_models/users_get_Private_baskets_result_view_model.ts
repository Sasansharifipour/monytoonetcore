import { result_model } from "./result_model.interface";
import { users_get_profile_baskets_output_view_model } from "./users_get_profile_baskets_output_view_model";

export interface users_get_Private_baskets_result_view_model extends result_model {
  informations: users_get_profile_baskets_output_view_model;
}

