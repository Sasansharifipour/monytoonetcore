﻿
namespace DataLayer.Models
{
    public class users_check_version_Result
    {
        public int status_id { get; set; }
        public string message { get; set; }
        public string update_url { get; set; }
    }
}
