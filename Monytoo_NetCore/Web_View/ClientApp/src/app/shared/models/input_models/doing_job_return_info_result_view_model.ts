import { doing_job_result_view_model } from "./doing_job_result_view_model";
import { create_factor_view_model } from "./create_factor_view_model";

export interface doing_job_return_info_result_view_model extends doing_job_result_view_model {
  info: create_factor_view_model[];
}

