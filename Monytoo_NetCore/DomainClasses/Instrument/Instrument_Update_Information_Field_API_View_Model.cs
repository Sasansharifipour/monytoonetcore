﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Instrument
{
    public class Instrument_Update_Information_Field_API_View_Model : get_base_data_model
    {
        public long namad_Id { get; set; } = 0;

        public string information_Field_Name { get; set; } = "";

        public string information_Field_Value { get; set; } = "";
    }
}