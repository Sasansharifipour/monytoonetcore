export interface information_field_value {
  field_key: string;
  field_value: string;
}
