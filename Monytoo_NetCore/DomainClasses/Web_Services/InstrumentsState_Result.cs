﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Web_Services
{
    public class InstrumentsState_Result
    {
        public long InsCode { get; set; } = 0;

        public string LVal18AFC { get; set; } = "";

        public string LVal30 { get; set; } = "";

        public string LBoard { get; set; } = "";

        public string StateTypeDesc { get; set; } = "";

        public string CEtaVal { get; set; } = "";
    }
}