﻿using CommonCode;
using DomainClasses.RSS;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;
using static CommonCode.CommonCodes;
using System.Data.Entity.Infrastructure;
using System.IO;

namespace Services
{
    public interface IRssService
    {
        IEnumerable<RssNews> Read_Feeds(string rss_url);

        Task Save_FeedAsync(RssNews data);

        List<engine_rss_temp> Get_All_Rss_Donot_Posted();

        void Post_Rss_Feed(engine_rss_temp data);
    }

    public class RssService : IRssService
    {
        private IImageService _imageService;
        private IDbContextFactory<connectionString> _contextFactory;
        private ICreator<IRSSProvider> _creator;
        private IPostService _postService;

        public RssService(IImageService imageService, ICreator<IRSSProvider> creator
            , IDbContextFactory<connectionString> contextFactory, IPostService postService)
        {
            _imageService = imageService;
            _contextFactory = contextFactory;
            _creator = creator;
            _postService = postService;
        }

        public async Task Save_FeedAsync(RssNews data)
        {
            try
            {
                if (data == null || data.Image_path.IsNullOrEmptyOrWhiteSpace())
                    return;

                if (Is_Rss_Exist_In_Temp_Table(data))
                    return;

                string file_path = "";
                if (data.Image_path != null)
                    file_path = _imageService.save_image(data.Image_path, saving_image_location.temp_image, 512);

                string image_title_path = "";
                if (!string.IsNullOrEmpty(file_path) && !string.IsNullOrWhiteSpace(file_path))
                {
                    var image = ImageUtil.generate_rss_image_for_post(Config.get_root_path() + file_path, data.Title);

                    image_title_path = _imageService.save_image(image, saving_image_location.temp_image);
                    image.Dispose();

                    File.Delete(Config.get_root_path() + file_path);
                }

                try
                {
                    using (var _db = _contextFactory.Create())
                    {
                        engine_rss_temp rss_Temp = new engine_rss_temp()
                        {
                            Date = data.Date,
                            Description = data.Description,
                            Image_path = image_title_path,
                            Link = data.Link,
                            Title = data.Title,
                            rss_url = data.Rss_URL
                        };

                        _db.engine_rss_temp.Add(rss_Temp);
                        int results = _db.SaveChangesAsync().Result;

                        if (results <= 0)
                        {
                            File.Delete(Config.get_root_path() + image_title_path);
                        }
                    }
                }
                catch (Exception e)
                {
                    File.Delete(Config.get_root_path() + image_title_path);
                }
            }
            catch (Exception e)
            {

            }
        }

        public void Post_Rss_Feed(engine_rss_temp data)
        {
            try
            {
                if (data == null || data.Image_path.IsNullOrEmptyOrWhiteSpace())
                    return;

                rss_provider_enum rss_Provider_Enum = CommonCodes.Convert_URL_To_Rss_Provider_Enum(data.rss_url);
                long user_id = 0;
                string secret_key = "";
                string img_path = "";

                if (rss_Provider_Enum == rss_provider_enum.NabzeBourse)
                    (user_id, secret_key) = Config.get_user_info_from_config("rss");
                else if (rss_Provider_Enum == rss_provider_enum.BourseNews)
                    (user_id, secret_key) = Config.get_user_info_from_config("boursenews_rss");
                else if (rss_Provider_Enum == rss_provider_enum.EghtesadNews)
                    (user_id, secret_key) = Config.get_user_info_from_config("eghtesadnews_rss");
                else if (rss_Provider_Enum == rss_provider_enum.DoyaeEghtesad)
                    (user_id, secret_key) = Config.get_user_info_from_config("donyaeeghtesad_rss");


                if (data.Image_path == "")
                    img_path = Config.get_post_default_image_path();
                else
                    img_path = Config.get_root_path() + data.Image_path;

                string API_Key = String.Format("{0}-{1}", user_id, secret_key);

                string file_name = Path.GetFileName(img_path);

                var file_data = File.ReadAllBytes(img_path);

                var result = _postService.post("http://Api.moneytoo.ir/api/users/send_post", file_name, file_data,
                    API_Key, data.Title + "\n" + data.Description + "\n\n" + data.Link, user_id.ToString());

                using (var _db = _contextFactory.Create())
                {
                    int result_update_db = result.Post_id;

                    if (result_update_db > 0)
                    {
                        data.post_id = result_update_db;
                        _db.engine_rss_temp.AddOrUpdate(data);
                        _db.SaveChanges();
                    }
                }
            }
            catch (Exception e) { }
        }

        public bool Is_Rss_Exist_In_Temp_Table(RssNews data)
        {
            using (var _db = _contextFactory.Create())
            {
                var item = _db.engine_rss_temp.FirstOrDefault(s => s.Link.Trim() == data.Link.Trim());

                if (item == null || item.ID < 0)
                    return false;
                return true;
            }
        }

        public IEnumerable<RssNews> Read_Feeds(string rss_url)
        {
            IRSSProvider _provider = _creator.FactoryMethod((int)CommonCodes.Convert_URL_To_Rss_Provider_Enum(rss_url));

            return _provider.Read_Feeds(rss_url).Where(s => s.Image_path != "");
        }

        public List<engine_rss_temp> Get_All_Rss_Donot_Posted()
        {
            using (var _db = _contextFactory.Create())
            {
                return _db.engine_rss_temp.Where(s => s.post_id == null).ToList();
            }
        }
    }
}