import { get_base_data_model } from "./get_base_data_model.interface";
import { information_field_value } from "./information_field_value.interface";

export interface update_information_field_value_model extends get_base_data_model {
  information_fields: information_field_value[];
}
