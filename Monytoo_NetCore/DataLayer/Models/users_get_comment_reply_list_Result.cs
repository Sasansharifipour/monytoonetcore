﻿using System;

namespace DataLayer.Models
{
    public partial class users_get_comment_reply_list_Result
    {
        public Nullable<long> r_number { get; set; }
        public long comment_id { get; set; }
        public string comment_text { get; set; }
        public Nullable<long> comment_user_id { get; set; }
        public Nullable<System.DateTime> comment_time { get; set; }
        public string comment_username { get; set; }
        public Nullable<int> comment_user_type_id { get; set; }
        public Nullable<long> profile_image_t { get; set; }
        public Nullable<int> comment_like_count { get; set; }
        public string comment_is_liked { get; set; }
    }
}
