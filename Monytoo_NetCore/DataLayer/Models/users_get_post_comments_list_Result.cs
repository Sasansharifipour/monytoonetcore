﻿using System;

namespace DataLayer.Models
{
    public class users_get_post_comments_list_Result
    {
        public Nullable<long> r_number { get; set; }
        public Nullable<int> reply_count { get; set; }
        public long comment_id { get; set; }
        public string comment_text { get; set; }
        public Nullable<long> comment_user_id { get; set; }
        public Nullable<System.DateTime> comment_time { get; set; }
        public string comment_username { get; set; }
        public Nullable<int> comment_user_type_id { get; set; }
        public Nullable<long> reply_id { get; set; }
        public string reply_text { get; set; }
        public Nullable<long> replay_user_id { get; set; }
        public Nullable<System.DateTime> reply_time { get; set; }
        public string reply_username { get; set; }
        public Nullable<int> reply_user_type_id { get; set; }
        public Nullable<long> comment_user_profile_image_t { get; set; }
        public Nullable<long> reply_user_profile_image_t { get; set; }
        public Nullable<int> comment_like_count { get; set; }
        public Nullable<int> reply_like_count { get; set; }
        public string comment_is_liked { get; set; }
        public string reply_is_liked { get; set; }
    }
}
