import { user_profile_information_view_model } from "./user_profile_information_view_model.interface";

export interface user_profile_data_view_model {
  username: string;
  name: string ;
  lastname: string ;
  invitation_code: string ;
  strength: number;
  post_count: number ;
  follower_count: number;
  following_count: number;
  user_type: number ;
  is_followed: boolean;
  profile_image_t: number ;
  information_fields: user_profile_information_view_model[] ;
}
