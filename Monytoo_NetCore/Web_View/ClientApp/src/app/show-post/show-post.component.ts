import { Component, Inject, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../shared/services/users.services';
import { register_result_model } from '../shared/models/input_models/register_result_model.interface';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router, ActivatedRoute } from '@angular/router';
import { get_profile_data_model } from '../shared/models/output_models/get_profile_data_model.interface';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { get_post_image_view_model } from '../shared/models/output_models/get_post_image_view_model';
import { users_get_profile_followers_Result } from '../shared/models/input_models/users_get_profile_followers_Result.interface';
import { get_profile_data_result_model } from '../shared/models/input_models/get_profile_data_result_model.interface';
import { user_profile_data_view_model } from '../shared/models/input_models/user_profile_data_view_model.interface';
import { Location } from '@angular/common';
import { get_post_view_model } from '../shared/models/output_models/get_post_view_model.interface';
import { users_get_single_post_result_view_model } from '../shared/models/input_models/users_get_single_post_result_view_model.interface';
import { users_get_single_post_Result } from '../shared/models/input_models/users_get_single_post_Result.interface';
import { users_doing_works_result_int_result_view_model } from '../shared/models/input_models/users_doing_works_result_int_result_view_model.interface';
import { CommentBaseViewModel } from '../shared/models/output_models/CommentBaseViewModel';
import { doing_job_result_view_model } from '../shared/models/input_models/doing_job_result_view_model';

@Component({
  selector: 'app-show-post-page',
  templateUrl: './show-post.component.html'
})
export class ShowPostPageComponent implements OnInit {
  private result_model: register_result_model;
  private post_id: string = '';
  private pg_idx: number = -1;
  private _user_id;
  isLoading = false;
  private post_data: users_get_single_post_Result;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private router: Router, private route: ActivatedRoute, private dialogService: MatDialog,
    private sanitizer: DomSanitizer, private _location: Location) {
    this.APIKEY = localStorage.getItem('API_KEY');
    this._user_id = localStorage.getItem('USER_ID');

    if (this.APIKEY == null) {
      this.router.navigate(['']);
    }

  }

  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      this.post_id = params['post_id'];
      this.loadInfo();
    });

  }

  private APIKEY: string = '';
  private data: users_get_profile_followers_Result[] = [];
  private profile_data: user_profile_data_view_model;

  loadProfileInfo() {
    var user_id = this.APIKEY.split('-')[0];
    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_id, t: '' };

    this.userService.get_profile_data(model).subscribe((result: get_profile_data_result_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.profile_data = result.data;
      }

    });

  }

  loadInfo() {
    this.isLoading = true;
    var model: get_post_view_model = { post_id: parseInt(this.post_id), API_Key: this.APIKEY, index: 0, count: 1, caption: '' };

    this.userService.users_get_single_post(model).subscribe((result: users_get_single_post_result_view_model) => {

      console.log(result);

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.post_data = result.informations;

        this.get_profile_image(this.post_data.user_id, this.post_data.profile_image_t).subscribe(result => {
          let objectURL = URL.createObjectURL(result);
          this.post_data.profile_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
        });

        this.get_post_image(this.post_data.post_id, this.post_data.image_id).subscribe(result => {
          let objectURL = URL.createObjectURL(result);
          this.post_data.post_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
        });

      }

    });

    this.isLoading = false;
  }

  like_comment(comment_id) {
    this.isLoading = true;
    var model: CommentBaseViewModel = { comment_id: comment_id , API_Key: this.APIKEY };

    this.userService.users_like_comment(model).subscribe((result: doing_job_result_view_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.post_data.comment_is_liked = '1';
        this.post_data.comment_like_count += 1;
      }

    });

    this.isLoading = false;
  }

  unlike_comment(comment_id) {
    this.isLoading = true;
    var model: CommentBaseViewModel = { comment_id: comment_id, API_Key: this.APIKEY };

    this.userService.users_unlike_comment(model).subscribe((result: doing_job_result_view_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.post_data.comment_is_liked = '0';
        this.post_data.comment_like_count -= 1;
      }

    });

    this.isLoading = false;
  }

  like_post() {
    this.isLoading = true;
    var model: get_post_view_model = { post_id: parseInt(this.post_id), API_Key: this.APIKEY, index: 0, count: 1, caption: '' };

    this.userService.users_like_post(model).subscribe((result: users_doing_works_result_int_result_view_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.post_data.is_liked = '1';
        this.post_data.like_count += 1;
      }

    });

    this.isLoading = false;
  }

  unlike_post() {
    this.isLoading = true;
    var model: get_post_view_model = { post_id: parseInt(this.post_id), API_Key: this.APIKEY, index: 0, count: 1, caption: '' };

    this.userService.users_unlike_post(model).subscribe((result: users_doing_works_result_int_result_view_model) => {
      console.log(result);
      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.post_data.is_liked = '0';
        this.post_data.like_count -= 1;
      }

    });

    this.isLoading = false;
  }

  backClicked() {
    this._location.back();
  }

  get_profile_image(user_profile_id, t): Observable<string> {

    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_profile_id, t: t };

    return this.userService.get_profile_image(model);
  }

  get_post_image(post_id, image_id): Observable<string> {

    var model: get_post_image_view_model = { API_Key: this.APIKEY, post_id: post_id, image_id: image_id };

    return this.userService.users_get_image(model);
  }

}

