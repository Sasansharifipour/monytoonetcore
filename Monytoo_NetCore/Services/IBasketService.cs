﻿using CommonCode;
using DataLayer.Models;
using System;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;

namespace Services
{
    public interface IBasketService
    {
        void evaluate_baskets();

        void get_all_basket_to_evaluate();
    }

    public class BasketService : IBasketService
    {
        private IDbContextFactory<connectionString> _contextFactory;
        private ILogger _logger;
        long user_id = 0;
        string secret_key = "";

        public BasketService(IDbContextFactory<connectionString> contextFactory, ILogger logger)
        {
            _logger = logger;
            _contextFactory = contextFactory;
            (user_id, secret_key) = Config.get_user_info_from_config("evaluate_basket");
        }

        public void evaluate_baskets()
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    while (true)
                    {
                        SqlParameter res = new SqlParameter("res", 0);
                        SqlParameter basket_id_res = new SqlParameter("basket_id", 0);
                        SqlParameter basket_balance_res = new SqlParameter("basket_balance", 0);
                        SqlParameter basket_initial_balance_res = new SqlParameter("basket_initial_balance", 0);

                        var items = _db.Engine_pick_basket_for_evaluation(user_id, secret_key,
                            res, basket_id_res, basket_balance_res, basket_initial_balance_res).ToList();

                        int basket_id = 0;
                        int basket_balance = 0;
                        int basket_initial_balance = 0;

                        if (basket_id_res != null && basket_id_res.Value != null)
                            int.TryParse(basket_id_res.Value.ToString(), out basket_id);

                        if (basket_balance_res != null && basket_balance_res.Value != null)
                            int.TryParse(basket_balance_res.Value.ToString(), out basket_balance);

                        if (basket_initial_balance_res != null && basket_initial_balance_res.Value != null)
                            int.TryParse(basket_initial_balance_res.Value.ToString(), out basket_initial_balance);

                        if (basket_id <= 0)
                            break;

                        double current_items_value = 0;
                        int total_value = 0;
                        double profit = 0;

                        foreach (var item in items)
                        {
                            int cnt = 0;
                            double current_price = 0;

                            if (item.count.HasValue)
                                cnt = item.count.Value;

                            double.TryParse(item.current_price, out current_price);

                            current_items_value += cnt * current_price;
                        }

                        total_value = basket_balance + (int)current_items_value;

                        double difference = total_value - basket_initial_balance;

                        profit = (difference / basket_initial_balance) * 100;

                        _db.Engine_evaluate_basket(user_id, secret_key, basket_id, total_value, profit, res);

                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void get_all_basket_to_evaluate()
        {
            try
            {
                using (var _db = _contextFactory.Create())
                {
                    SqlParameter res = new SqlParameter("res", 0);
                    var baskets = _db.Engine_get_all_basket_orders(user_id, secret_key, res).ToList();

                    foreach (var item in baskets)
                    {
                        if (item.create_time.HasValue)
                            if (item.create_time.Value.AddDays(1) < DateTime.Now)
                            {
                                SqlParameter evaluationg = new SqlParameter("res", 0);
                                _db.Engine_update_basket_orders_status(user_id, secret_key, item.order_id, false, evaluationg);
                                continue;
                            }
                        double lst_price = 0;
                        double.TryParse(item.last_price, out lst_price);

                        int last_price = (int)lst_price;

                        if (item.offer_price.HasValue && item.count > 0 && item.offer_price.Value > last_price)
                        {
                            SqlParameter evaluationg = new SqlParameter("res", 0);
                            _db.Engine_update_basket_orders_status(user_id, secret_key, item.order_id, true, evaluationg);
                            continue;
                        }

                        if (item.offer_price.HasValue && item.count < 0 && item.offer_price.Value < last_price)
                        {
                            SqlParameter evaluationg = new SqlParameter("res", 0);
                            _db.Engine_update_basket_orders_status(user_id, secret_key, item.order_id, true, evaluationg);
                            continue;
                        }

                        if (item.offer_price.HasValue && item.offer_price.Value == last_price
                            || (item.count > 0 && item.offer_price.Value < last_price)
                            || (item.count < 0 && item.offer_price.Value > last_price))
                        {
                            int price_distance = (int)Math.Max(1, Math.Abs(last_price - item.offer_price.Value));
                            int price_difficulty = Math.Abs(item.count.Value * item.difficulty.Value * price_distance);

                            int current_trade_volume = 0;
                            int.TryParse(item.current_trade_volume, out current_trade_volume);

                            if (item.initial_Trade_Volume.Value + price_difficulty < current_trade_volume)
                            {
                                SqlParameter evaluationg = new SqlParameter("res", 0);
                                _db.Engine_update_basket_orders_status(user_id, secret_key, item.order_id, true, evaluationg);
                                continue;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }

        }
    }
}
