﻿using System;

namespace DataLayer.Models
{
    public class users_get_shared_accounts_Result
    {
        public Nullable<long> ID { get; set; }
        public string secret_key { get; set; }
        public Nullable<int> type_id { get; set; }
        public Nullable<long> t { get; set; }
        public string username { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
    }
}
