﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Users
{
    public class register_model
    {
        public string mobile_number { get; set; }

        public string username { get; set; }

        public string name { get; set; }

        public string lastname { get; set; }

        public string firebase_id { get; set; }

        public string invitation_code { get; set; }
    }
}