import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { UserService } from '../shared/services/users.services';
import { check_user_name_available_model } from '../shared/models/output_models/check_user_name_available_model.interface';
import { UserRegistration } from '../shared/models/inline_models/user.registration.interface';
import { register_model } from '../shared/models/output_models/register_model.interface';
import { register_result_model } from '../shared/models/input_models/register_result_model.interface';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router, ActivatedRoute } from '@angular/router';
import { UserApprove } from '../shared/models/inline_models/UserApprove';
import { verify_by_mobile_number_model } from '../shared/models/output_models/verify_by_mobile_number_model.interface';
import { check_user_name_available_result_model } from '../shared/models/input_models/check_user_name_available_result_model.interface';

@Component({
  selector: 'app-register-user-form',
  templateUrl: './register-user.html'
})
export class UserRegisterComponent implements OnInit {
  private result_model: register_result_model;
  private is_user_available: boolean = true;
  private mobile_number: string = '';
  private user_session_ID: string = '';

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private route: ActivatedRoute, private router: Router , private dialogService: MatDialog) {
  }

  ngOnInit() {
    this.mobile_number = this.route.snapshot.paramMap.get('mobile_number');
    this.user_session_ID = this.route.snapshot.paramMap.get('user_session_ID');
  }

  onUsernameChange(username: string): void {
    if (username.trim() == "") {
      this.is_user_available = true;
      return;
    }

    let model: check_user_name_available_model = { username: username };

    this.userService.check_username_availability(model).subscribe(result => {
      var _result: check_user_name_available_result_model = result;

      if (_result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        if (_result.is_available == 0) {
          this.is_user_available = false;
        }
        else {
          this.is_user_available = true;
        }
      }

    });
  }

  register({ value }: { value: UserApprove }) {

    let model: verify_by_mobile_number_model = { user_session_ID: this.user_session_ID, verification_code: value.code };

    this.userService.verify_by_mobile_number(model).subscribe(result => {
      this.result_model = result;

      if (this.result_model.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        if (this.result_model.is_new_user) {

        }
        else {

        }
      }

    });
  }

}

