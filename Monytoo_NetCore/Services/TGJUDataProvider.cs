﻿using DomainClasses.Instrument;
using DomainClasses.Web_Services;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Services
{
    public class TGJUDataProvider : IDataProvider
    {
        ITGJUService _tgjuService;
        private IInstrumentService _instrumentService;

        public TGJUDataProvider(ITGJUService tgjuService, IInstrumentService instrumentService)
        {
            _tgjuService = tgjuService;
            _instrumentService = instrumentService;
        }

        public Task<List<Instrument_Result>> get_all_instruments()
        {
            throw new NotImplementedException();
        }

        public Instrument_Price_View_Model get_Instruments_Price(long Instrument_Code)
        {
            throw new NotImplementedException();
        }

        public Instrument_Price_View_Model get_Instruments_Trade_One_Day(long Instrument_Code, DateTime datetime)
        {
            throw new NotImplementedException();
        }

        public List<Instrument_Trade_View_Model> get_Trade_From_Date_To_Date(long Instrument_Code, DateTime from_datetime, DateTime to_datetime)
        {
            throw new NotImplementedException();
        }

        public async Task update_info_for_all_instrumentsAsync(long user_id, string secret_key, List<Engine_get_all_namad_Result> all_namads)
        {
            try
            {
                var data = _tgjuService.load_data();

                List<Instrument_Update_Information_Field_View_Model> all_data = new List<Instrument_Update_Information_Field_View_Model>();

                foreach (var item in data)
                {
                    var namad = all_namads.FirstOrDefault(s => s.InsCode.Trim() == item.id.ToString().Trim());

                    if (item == null || item.p == null)
                        continue;

                    if (namad == null || namad.namad_id <= 0)
                    {
                        //_tgjuService.add_namad(item);
                        continue;
                    }

                    var view_model = new Instrument_Price_View_Model()
                    {
                        namad_id = namad.namad_id,
                        InsCode = item.id,
                        ENGINE_Price_Reader_Last_Price = item.p
                    };

                    all_data.AddRange(view_model.Convert_To_Update_Model());
                }

                await Task.Run(() => _instrumentService.update_instrument_info_list(user_id, secret_key, all_data));
            }
            catch (Exception e)
            {

            }
        }
    }
}