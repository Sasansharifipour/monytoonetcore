import { get_base_data_model } from "./get_base_data_model.interface";

export interface PostBaseViewModel extends get_base_data_model {
  post_id: number;
}
