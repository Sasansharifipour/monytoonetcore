export interface users_get_basket_info_Result {
  create_time: Date;
  balance: number;
  total_value: number;
  last_evaluation_time: Date;
  last_profit_change: number;
  name: string;
  is_shareable: boolean;
  stock_count: number;
}
