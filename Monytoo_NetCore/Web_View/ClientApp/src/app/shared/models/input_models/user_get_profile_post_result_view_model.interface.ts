import { result_model } from "./result_model.interface";
import { users_get_profile_posts_Result } from "./users_get_profile_posts_Result.interface";

export interface user_get_profile_post_result_view_model extends result_model {
  informations: users_get_profile_posts_Result[];
}

