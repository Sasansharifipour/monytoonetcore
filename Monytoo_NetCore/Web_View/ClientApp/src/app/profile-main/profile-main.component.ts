import { Component, Inject, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatInputModule } from '@angular/material/input';
import { UserService } from '../shared/services/users.services';
import { check_user_name_available_model } from '../shared/models/output_models/check_user_name_available_model.interface';
import { UserRegistration } from '../shared/models/inline_models/user.registration.interface';
import { register_model } from '../shared/models/output_models/register_model.interface';
import { register_result_model } from '../shared/models/input_models/register_result_model.interface';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router, ActivatedRoute } from '@angular/router';
import { get_post_by_index_view_model } from '../shared/models/output_models/get_post_by_index_view_model.interface';
import { users_home_posts_result_view_model } from '../shared/models/input_models/users_home_posts_result_view_model.interface';
import { users_home_posts_Result } from '../shared/models/input_models/users_home_posts_Result.interface';
import { get_profile_data_model } from '../shared/models/output_models/get_profile_data_model.interface';
import { Observable } from 'rxjs';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { get_post_image_view_model } from '../shared/models/output_models/get_post_image_view_model';
import { get_profile_data_result_model } from '../shared/models/input_models/get_profile_data_result_model.interface';
import { user_profile_data_view_model } from '../shared/models/input_models/user_profile_data_view_model.interface';
import { get_user_follow_view_model } from '../shared/models/output_models/get_user_follow_view_model.interface';
import { user_get_profile_post_result_view_model } from '../shared/models/input_models/user_get_profile_post_result_view_model.interface';
import { users_get_profile_posts_Result } from '../shared/models/input_models/users_get_profile_posts_Result.interface';
import { users_get_profile_baskets_view_model } from '../shared/models/output_models/users_get_profile_baskets_view_model';
import { users_get_basket_order_history_result_view_model } from '../shared/models/input_models/users_get_basket_order_history_result_view_model';
import { users_get_Private_baskets_result_view_model } from '../shared/models/input_models/users_get_Private_baskets_result_view_model';
import { users_get_profile_baskets_output_view_model } from '../shared/models/input_models/users_get_profile_baskets_output_view_model';
import { users_get_basket_order_history_Result } from '../shared/models/input_models/users_get_basket_order_history_Result';

@Component({
  selector: 'app-profile-main-page',
  templateUrl: './profile-main.component.html'
})
export class ProfileMainPageComponent implements OnInit  {
  private result_model: register_result_model;
  private mobile_number: string = '';
  private pg_idx: number = -1;
  private _user_id;
  private profile_user_id;
  isLoading = false;


  ngOnInit() {

    this.route.queryParams.subscribe(params => {
      if (params['user_id'] != undefined)
        this._user_id = params['user_id'];
      else
        this._user_id = this.profile_user_id;

      this.loadProfileInfo();
      this.loadProfilePosts();
      this.loadBasket();
      this.loadPrivateBasket();
      this.loadSharedBasket();
    });

  }

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private router: Router, private route: ActivatedRoute, private dialogService: MatDialog, private sanitizer: DomSanitizer) {
    this.APIKEY = localStorage.getItem('API_KEY');
    this.profile_user_id = localStorage.getItem('USER_ID');

    if (this.APIKEY == null) {
      this.router.navigate(['']);
    }

  }
  private APIKEY: string = '';
  private data: users_home_posts_Result[] = [];
  private profile_data: user_profile_data_view_model;
  private profile_posts: users_get_profile_posts_Result[] = [];
  private profile_image_data: SafeUrl;
  private user_email: string;

  private Last_Price;
  private Namad_Status;
  private Close_Price;
  private Last_Price_Change;
  private Close_Price_Change;
  private Volume;
  private Base_Volume;
  private Hoghoghi_Fluctuation_Sell;
  private Haghighi_Volume_Buy;
  
  private Haghighi_Count_Sell;
  private Haghighi_Fluctuation_Sell;
  private Hoghoghi_Volume_Sell;
  private Haghighi_Fluctuation_Buy;
  private Haghighi_Volume_Sell;
  private Hoghoghi_Count_Buy;
  private Hoghoghi_Volume_Buy;
  private Hoghoghi_Count_Sell;
  private Haghighi_Count_Buy;
  private Market_Type;
  private Hoghoghi_Fluctuation_Buy;

  private Namad_Status_id;
  private MACD_Status;
  private RSI_Data;
  private AI_Predicted;

  @HostListener("window:scroll", ["$event"])
  getScrollHeight(): void {
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight && !this.isLoading) {
      this.loadProfilePosts();
    }
  }

  private_rial_options = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category'
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series: [
      {
        name: 'ریال',
        type: 'line',
        stack: 'counts',
        areaStyle: { normal: {} },
        data: []
      }
    ]
  };

  private_usd_options = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category'
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series: [
      {
        name: 'دلار',
        type: 'line',
        stack: 'counts',
        areaStyle: { normal: {} },
        data: []
      }
    ]
  };

  private_chf_options = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        type: 'cross',
        label: {
          backgroundColor: '#6a7985'
        }
      }
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true
    },
    xAxis: [
      {
        type: 'category'
      }
    ],
    yAxis: [
      {
        type: 'value'
      }
    ],
    series: [
      {
        name: 'فرانک',
        type: 'line',
        stack: 'counts',
        areaStyle: { normal: {} },
        data: []
      }
    ]
  };

  private private_basket: users_get_profile_baskets_output_view_model;

  loadPrivateBasket() {
    var model: users_get_profile_baskets_view_model = { API_Key: this.APIKEY, profile_user_id: parseInt(this._user_id) };

    this.userService.users_get_Private_baskets(model).subscribe((result: users_get_Private_baskets_result_view_model) => {
      console.log(result);
      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.private_basket = result.informations;

        this.private_rial_options.series[0].data = [];
        this.private_usd_options.series[0].data = [];
        this.private_chf_options.series[0].data = [];

        this.private_basket.history.forEach(item => {
          this.private_rial_options.series[0].data.push(item.total_value);
          this.private_usd_options.series[0].data.push(item.total_value_USD);
          this.private_chf_options.series[0].data.push(item.total_value_CHF);
        });

      }

    });
  }

  loadSharedBasket() {
    var model: users_get_profile_baskets_view_model = { API_Key: this.APIKEY, profile_user_id: parseInt(this._user_id) };

    this.userService.users_get_share_baskets(model).subscribe((result: users_get_basket_order_history_result_view_model) => {
      console.log(result);
      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.user_shared_baskets = result.informations;

      }

    });
  }

  private user_baskets: users_get_basket_order_history_Result[] = [];

  private user_shared_baskets: users_get_basket_order_history_Result[] = [];

  loadBasket() {
    var model: users_get_profile_baskets_view_model = { API_Key: this.APIKEY, profile_user_id: parseInt(this._user_id) };

        this.userService.users_get_profile_baskets(model).subscribe((result: users_get_basket_order_history_result_view_model) => {
          
          if (result.error_Id != "0") {
            const dialogRef = this.dialogService.open(ErrorDialog);

          } else {
            this.user_baskets = result.informations;
          }

        });
  }

  loadProfilePosts() {
    var model: get_user_follow_view_model = { API_Key: this.APIKEY, index: this.pg_idx + 1, count: 15, user_profile_id: this._user_id };
      
    this.userService.users_get_profile_posts(model).subscribe((result: user_get_profile_post_result_view_model) => {
      
      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.pg_idx = this.pg_idx + 1;

        let t_data: users_get_profile_posts_Result[];
        t_data = result.informations;

        t_data.forEach(item => {

          this.get_post_image(item.post_id, item.image_id).subscribe(result => {
            let objectURL = URL.createObjectURL(result);
            item.post_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
          });
        });

        t_data.forEach(item => {
          this.profile_posts.push(item);
        });

      }

    });
  }

  get_post_image(post_id, image_id): Observable<string> {

    var model: get_post_image_view_model = { API_Key: this.APIKEY, post_id: post_id, image_id: image_id };

    return this.userService.users_get_image(model);
  }

  short_number(value: any, fractionSize: any): any {
    if (value === null) return null;
    if (value === 0) return "0";
    var abs = Math.abs(value);
    var rounder = Math.pow(10, fractionSize);
    var isNegative = value < 0;
    var key = '';
    var powers = [{ key: "Q", value: Math.pow(10, 15) }, { key: "T", value: Math.pow(10, 12) }, { key: "B", value: Math.pow(10, 9) }, { key: "M", value: Math.pow(10, 6) }, { key: "k", value: 1000 }];
    for (var i = 0; i < powers.length; i++) {
      var reduced = abs / powers[i].value;
      reduced = Math.round(reduced * rounder) / rounder;
      if (reduced >= 1) {
        abs = reduced;
        key = powers[i].key;
        break;
      }
    }
    return (isNegative ? '-' : '') + abs + key;
  }

  loadProfileInfo() {
    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: this._user_id, t : '' };

    this.userService.get_profile_data(model).subscribe((result: get_profile_data_result_model) => {

      if (result.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        this.profile_data = result.data;

        this.Last_Price = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Last_Price");
        this.Namad_Status = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Namad_Status");
        this.Close_Price = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Close_Price");
        this.Last_Price_Change = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Last_Price_Change");
        this.Close_Price_Change = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Close_Price_Change");
        this.Volume = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Volume");
        this.Base_Volume = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Base_Volume");
        this.Hoghoghi_Fluctuation_Sell = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Hoghoghi_Fluctuation_Sell");
        this.Hoghoghi_Fluctuation_Buy = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Hoghoghi_Fluctuation_Buy");
        this.Haghighi_Volume_Buy = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Haghighi_Volume_Buy");

        this.Haghighi_Count_Sell = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Haghighi_Count_Sell");
        this.Haghighi_Fluctuation_Sell = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Haghighi_Fluctuation_Sell");
        this.Hoghoghi_Volume_Sell = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Hoghoghi_Volume_Sell");
        this.Haghighi_Fluctuation_Buy = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Haghighi_Fluctuation_Buy");
        this.Haghighi_Volume_Sell = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Haghighi_Volume_Sell");
        this.Hoghoghi_Count_Buy = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Hoghoghi_Count_Buy");
        this.Hoghoghi_Volume_Buy = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Hoghoghi_Volume_Buy");
        this.Hoghoghi_Count_Sell = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Hoghoghi_Count_Sell");
        this.Haghighi_Count_Buy = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Haghighi_Count_Buy");
        this.Market_Type = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Market_Type");
        this.Namad_Status_id = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_Price_Reader_Namad_Status_id");
        this.MACD_Status = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_MACD_Status");
        this.RSI_Data = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_RSI_Data");
        this.AI_Predicted = this.profile_data.information_fields.find(e => e.key_name == "ENGINE_AI_Predicted");

        
        
        
        
        console.log(this.profile_data);
        this.get_profile_image(this._user_id, result.data.profile_image_t).subscribe(result => {
          let objectURL = URL.createObjectURL(result);
          this.profile_image_data = this.sanitizer.bypassSecurityTrustUrl(objectURL);;
        });

        this.profile_data.information_fields.forEach(item => {
          if (item.key_name == 'EMAIL') {
            this.user_email = item.value;
          }
        });

      }

    });

  }

  get_profile_image(user_profile_id, t): Observable<string> {

    var model: get_profile_data_model = { API_Key: this.APIKEY, user_profile_id: user_profile_id, t: t };

    return this.userService.get_profile_image(model);
  }
}

