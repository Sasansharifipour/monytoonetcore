﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Xml.Linq;

namespace Services
{
    public interface IRSSReaderService
    {
        IEnumerable<XElement> Read_RSS_From_URL(string url, string Descendants_param = "");
    }


    public class RSSReaderService : IRSSReaderService
    {

        public IEnumerable<XElement> Read_RSS_From_URL(string url, string Descendants_param = "")
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                    | SecurityProtocolType.Tls
                    | SecurityProtocolType.Tls11
                    | SecurityProtocolType.Tls12;

                XDocument rssfeedxml;
                rssfeedxml = XDocument.Load(url);

                return rssfeedxml.Descendants(Descendants_param);
            }
            catch (Exception ex)
            {
                return new List<XElement>();
            }

        }
    }
}