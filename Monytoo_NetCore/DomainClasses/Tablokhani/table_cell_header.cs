﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Tablokhani
{

    public class table_cell_header : IEquatable<table_cell_header>
    {
        public int index { get; set; } = 0;

        public string Title { get; set; } = "";

        public bool Equals(table_cell_header other)
        {
            if (Object.ReferenceEquals(other, null)) return false;

            if (Object.ReferenceEquals(this, other)) return true;

            return index.Equals(other.index) && Title.Equals(other.Title);
        }

        public override int GetHashCode()
        {
            int hashProductTitle = Title == null ? 0 : Title.GetHashCode();

            int hashProductCode = index.GetHashCode();

            return hashProductTitle ^ hashProductCode;
        }
    }
}