import { get_base_data_model } from "./get_base_data_model.interface";

export interface update_profile_data_model extends get_base_data_model {
  username: string;
  name: string;
  lastname: string;
}
