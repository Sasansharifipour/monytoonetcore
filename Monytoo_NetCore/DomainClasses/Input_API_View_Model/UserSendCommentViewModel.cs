﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Input_API_View_Model
{
    public class UserSendCommentViewModel : get_base_data_model
    {
        public long Post_id { get; set; } = 0;
        
        public string Text { get; set; } = "";

        public long Reply_to_comment_id { get; set; } = 0;
    }
}