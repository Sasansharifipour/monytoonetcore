﻿using System;

namespace DataLayer.Models
{
    public class users_get_basket_items_Result
    {
        public long basket_item_id { get; set; }
        public string namad_name { get; set; }
        public Nullable<int> count { get; set; }
        public Nullable<double> order_price { get; set; }
        public Nullable<double> current_price { get; set; }
        public double total_profit { get; set; }
        public string daily_profit { get; set; }
        public Nullable<System.DateTime> create_time { get; set; }
        public Nullable<long> namad_id { get; set; }
    }
}
