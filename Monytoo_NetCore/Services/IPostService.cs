﻿using DomainClasses.Users;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net.Http;
using static CommonCode.CommonCodes;

namespace Services
{
    public interface IPostService
    {
        send_post_result_view_model post(string api_address, string file_name, byte[] picture, string API_Key, string Caption, string From_user_id);
    }

    public class PostService : IPostService
    {
        public send_post_result_view_model post(string api_address, string file_name, byte[] picture, string API_Key, string Caption, string From_user_id)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    MultipartFormDataContent form = new MultipartFormDataContent();

                    form.Add(new StringContent(API_Key), "API_Key");
                    form.Add(new StringContent(Caption), "Caption");

                    if (From_user_id != null)
                        form.Add(new StringContent(From_user_id), "From_user_id");

                    form.Add(new ByteArrayContent(picture, 0, picture.Count()), "Image", file_name);

                    HttpResponseMessage response = httpClient.PostAsync(api_address, form).Result;

                    string result = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<send_post_result_view_model>(result);
                }
            }
            catch (Exception e)
            {
                send_post_result_view_model result_model = new send_post_result_view_model();
                result_model.Post_id = 0;
                result_model.Error_Id = error_enum.unknown_error.GetCode();
                result_model.Error_Text = error_enum.unknown_error.GetDescription();
                return result_model;
            }
        }
    }
}
