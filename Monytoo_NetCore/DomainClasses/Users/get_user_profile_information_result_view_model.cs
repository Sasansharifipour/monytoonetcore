﻿
using System.Collections.Generic;

namespace DomainClasses.Users
{
    public class get_user_profile_information_result_view_model :result_model
    {
        public List<user_profile_information_view_model> data { get; set; }
    }
}