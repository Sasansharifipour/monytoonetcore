import { result_model } from "./result_model.interface";
import { users_get_price_packages_Result } from "./users_get_price_packages_Result.interface";

export interface users_get_price_packages_result_view_model extends result_model {
  information: users_get_price_packages_Result[];
}

