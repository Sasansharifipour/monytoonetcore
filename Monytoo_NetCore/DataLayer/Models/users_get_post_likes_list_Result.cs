﻿using System;

namespace DataLayer.Models
{
    public class users_get_post_likes_list_Result
    {
        public Nullable<long> ID { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string username { get; set; }
        public string is_followed { get; set; }
        public Nullable<long> r_number { get; set; }
        public Nullable<int> user_type { get; set; }
        public Nullable<long> profile_image_t { get; set; }
    }
}
