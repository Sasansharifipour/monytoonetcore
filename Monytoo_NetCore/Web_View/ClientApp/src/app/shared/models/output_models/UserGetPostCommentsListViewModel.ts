import { PostBaseViewModel } from "./PostBaseViewModel";

export interface UserGetPostCommentsListViewModel extends PostBaseViewModel {
  index: number;
  count: number;
}
