﻿using CommonCode;
using DomainClasses.Common;
using DomainClasses.RSI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using static CommonCode.CommonCodes;

namespace Services
{
    public static class ImageUtil
    {
        public static Bitmap generate_rsi_image_for_namads(RSI_Posting_View_Model hit_data)
        {
            Bitmap bmp = new Bitmap(Config.get_default_image_path_for_rsi_post(hit_data.post_type));
            using (Graphics graphics = Graphics.FromImage(bmp))
            {
                //Namad Name and Price
                Font font = new Font("B mitra", 11, FontStyle.Bold);
                Color pcolor = Color.Black;
                graphics.DrawString(hit_data.namad_name + hit_data.namad_price.ToString("#,##0"), font, new SolidBrush(pcolor), 515, 870);

                //Namad RSI
                font = new Font("B mitra", 10, FontStyle.Bold);
                pcolor = Color.Black;
                graphics.DrawString(hit_data.rsi.ToString(), font, new SolidBrush(pcolor), 395, 955);

                //Date

                DateTime d = DateTime.Now;
                PersianCalendar pc = new PersianCalendar();
                string date = string.Format("{0}/{1}/{2}", pc.GetYear(d), pc.GetMonth(d), pc.GetDayOfMonth(d));

                font = new Font("B mitra", 8, FontStyle.Bold);
                pcolor = Color.Black;
                graphics.DrawString(date, font, new SolidBrush(pcolor), 65, 970);

                graphics.Flush();
                font.Dispose();
                graphics.Dispose();
            }
            return bmp;
        }

        public static Bitmap generate_rss_image_for_post(string file_path, string title)
        {
            List<(Color, Color)> colors = new List<(Color, Color)>();

            colors.Add((Color.Red, Color.White));
            colors.Add((Color.Yellow, Color.Purple));
            colors.Add((Color.Black, Color.Yellow));
            colors.Add((Color.Blue, Color.White));
            colors.Add((Color.Pink, Color.Purple));
            colors.Add((Color.Green, Color.Blue));

            Random rnd = new Random();
            int idx = rnd.Next(0, colors.Count);
            Color Backgroud = new Color();
            Color fontColor = new Color();
            (Backgroud, fontColor) = colors[idx];
            string font_name = "B Titr";

            Bitmap bmp = new Bitmap(file_path);
            using (Graphics graphics = Graphics.FromImage(bmp))
            {
                //Title
                int font_size = 1;
                Font font = new Font(font_name, font_size, FontStyle.Bold);
                Color pcolor = fontColor;
                SizeF size = new SizeF();

                while (true && font_size < 32)
                {
                    font = new Font(font_name, font_size, FontStyle.Bold);
                    size = graphics.MeasureString(title, font);

                    if (size.Width > bmp.Width - 10)
                    {
                        font_size -= 2;
                        break;
                    }

                    font_size++;
                }

                font = new Font(font_name, font_size, FontStyle.Bold);
                size = graphics.MeasureString(title, font);

                var left = Math.Max(5, (int)((bmp.Width - size.Width) / 2));
                int top = 0;
                if (rnd.Next(2) == 1)
                    top = Math.Max(0, (int)(bmp.Height - size.Height - 10));
                else
                    top = 10;

                var textPosition = new Point(left, top);
                var rect = new RectangleF(textPosition.X, textPosition.Y, size.Width, size.Height);

                Brush brush = new SolidBrush(Backgroud);

                graphics.FillRectangle(brush, rect);

                graphics.DrawString(title, font, new SolidBrush(pcolor), left, top);

                graphics.Flush();
                font.Dispose();
                graphics.Dispose();
            }
            return bmp;
        }

        public static Bitmap generate_board_image_for_post(Board_Type_Element_enum board_Type, List<PreparingText> texts, int number_of_templates)
        {
            try
            {
                Random random = new Random();
                string image_back_ground_file_path = string.Format(@"{0}\Images\Board_Reader\{1}\{2}.jpg", Config.get_root_path(),
                    board_Type.ToString(), random.Next(1, number_of_templates).ToString());

                Bitmap bmp = new Bitmap(image_back_ground_file_path);
                using (Graphics graphics = Graphics.FromImage(bmp))
                {
                    foreach (var item in texts)
                    {
                        int left = 0;

                        if (item.max_width == 0)
                            left = item.X;
                        else
                        {
                            SizeF size = graphics.MeasureString(item.Title, item.GetFont());
                            left = item.X + (item.max_width - (int)size.Width) / 2;
                        }

                        graphics.DrawString(item.Title, item.GetFont(), new SolidBrush(item.GetColor()), left, item.Y);
                    }

                    graphics.Flush();
                    graphics.Dispose();
                }
                return bmp;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static Bitmap generate_image_with_text(string image_path, List<PreparingText> texts)
        {
            try
            {
                Bitmap bmp = new Bitmap(image_path);
                using (Graphics graphics = Graphics.FromImage(bmp))
                {
                    foreach (var item in texts)
                    {
                        graphics.DrawString(item.Title, item.GetFont(), new SolidBrush(item.GetColor()), item.X, item.Y);
                    }

                    graphics.Flush();
                    graphics.Dispose();
                }
                return bmp;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static Bitmap generate_image_with_text_for_codal(string image_path, List<PreparingText> texts)
        {
            try
            {
                Bitmap bmp = new Bitmap(image_path);
                using (Graphics graphics = Graphics.FromImage(bmp))
                {
                    foreach (var item in texts)
                    {
                        graphics.DrawString(item.Title, item.GetFont(), new SolidBrush(item.GetColor()), item.X, item.Y);
                    }

                    graphics.Flush();
                    graphics.Dispose();
                }
                return bmp;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static int get_suitable_font_size_for_codal(string title, string font_name, SizeF max_size, int max_font_size = 32)
        {
            string file_path = string.Format(@"{0}\Images\CODAL_POST_IMAGES\1.jpg", Config.get_root_path());
            return get_suitable_font_size(title, font_name, max_size, file_path, max_font_size);
        }

        private static int get_suitable_font_size(string title, string font_name, SizeF max_size, string file_path, int max_font_size = 32)
        {
            int font_size = 1;
            Graphics graphics = Graphics.FromImage(new Bitmap(file_path));
            Font font = new Font(font_name, font_size, FontStyle.Bold);
            SizeF size = new SizeF();

            while (font_size <= max_font_size)
            {
                font = new Font(font_name, font_size, FontStyle.Bold);
                size = graphics.MeasureString(title, font);

                if (size.Width > max_size.Width || size.Height > max_size.Height)
                {
                    font_size--;
                    break;
                }

                font_size++;
            }

            return font_size;
        }
    }
}