import { result_model } from "./result_model.interface";
import { users_Explore_posts_Result } from "./users_Explore_posts_Result.interface";

export interface users_explore_posts_result_view_model extends result_model {
  informations: users_Explore_posts_Result[];
}

