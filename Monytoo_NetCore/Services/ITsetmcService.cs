﻿using CommonCode;
using DomainClasses.Instrument;
using DomainClasses.Web_Services;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Services
{
    public interface ITsetmcService
    {
        List<Instrument_Trade_View_Model> get_Trade_From_Date_To_Date(long Instrument_Code,
            DateTime from_datetime, DateTime to_datetime);

        Instrument_Trade_View_Model get_Trade_In_Date(long Instrument_Code, DateTime datetime);

        Task<List<Instrument_Result>> Get_TradeOneDay_From_Source(int date, byte Flow);

        Task<List<TradeOneDayAll_Result>> Get_TradeOneDayAll_From_Source(int date, byte Flow);

        Task<List<InstrumentsState_Result>> Get_InstrumentsState_From_Source(byte Flow);

        Task<InstrumentsState_Result> Get_Instruments_State_In_Special_Date_From_Source(long Instrument_Code, DateTime Date);

        Task<List<Trade_Last_Day_Result_View_Model>> Get_TradeLastDay_From_Source(byte Flow);

        Task<List<Instrument_Result>> Get_Instrument_From_Source(byte Flow);

        Task<List<ClientType_Result>> Get_ClientType_From_Source();

        Task<List<Static_Thresholds_Result_View_Model>> Get_StaticThresholds_From_Source(byte Flow);

        Task<List<BestLimitOneIns_Result_View_Model>> Get_Best_Limit_One_Instrument(long Instrument_Code);
    }

    public class TsetmcService : ITsetmcService
    {
        private TsePublicV2 _tsetmc;
        private string tsem_username = Config.get_tsem_username();
        private string tsem_password = Config.get_tsem_password();

        public TsetmcService(TsePublicV2 tsePublicV2)
        {
            _tsetmc = tsePublicV2;
        }

        public Task<List<Instrument_Result>> Get_Instrument_From_Source(byte Flow)
        {
            return Task.Run(() => Get_Instrument_From_Source_Task(Flow));
        }

        public Task<List<Instrument_Result>> Get_TradeOneDay_From_Source(int date, byte Flow)
        {
            return Task.Run(() => Get_TradeOneDay_From_Source_Task(date, Flow));
        }

        private List<Instrument_Result> Get_TradeOneDay_From_Source_Task(int date, byte Flow)
        {
            CancellationTokenSource s_cts = new CancellationTokenSource();
            s_cts.CancelAfter(10000);

            DataSet data = new DataSet();
            var task = Get_TradeOneDay_Data_From_Source(date, Flow);

            try
            {
                task.Wait(s_cts.Token);
            }
            catch (Exception ex)
            {
                if (ex is OperationCanceledException)
                    data = Get_Instrument_Data_From_Source_Force(Flow);
            }

            if (task.IsCompleted)
                data = task.Result;

            List<Instrument_Result> result = CommonCodes.Convert_Dataset_To_Object<Instrument_Result>(data);
            return result;
        }

        private List<Instrument_Result> Get_Instrument_From_Source_Task(byte Flow)
        {
            CancellationTokenSource s_cts = new CancellationTokenSource();
            s_cts.CancelAfter(10000);

            DataSet data = new DataSet();
            var task = Get_Instrument_Data_From_Source(Flow);

            try
            {
                task.Wait(s_cts.Token);
            }
            catch (Exception ex)
            {
                if (ex is OperationCanceledException)
                    data = Get_Instrument_Data_From_Source_Force(Flow);
            }

            if (task.IsCompleted)
                data = task.Result;

            List<Instrument_Result> result = CommonCodes.Convert_Dataset_To_Object<Instrument_Result>(data);
            return result;
        }

        public Task<List<TradeOneDayAll_Result>> Get_TradeOneDayAll_From_Source(int date, byte Flow)
        {
            try
            {
                var data = Get_TradeOneDayAll_Data_From_Source(date, Flow);
                Task<List<TradeOneDayAll_Result>> result = Task.Run(() => CommonCodes.Convert_Dataset_To_Object<TradeOneDayAll_Result>(data.Result));
                return result;
            }
            catch (Exception e)
            {
                return new Task<List<TradeOneDayAll_Result>>(() => new List<TradeOneDayAll_Result>());
            }
        }

        public Task<List<ClientType_Result>> Get_ClientType_From_Source()
        {
            try
            {
                var data = Get_ClientType_Data_From_Source();
                var result = Task.Run(() => CommonCodes.Convert_DataTable_To_Object<ClientType_Result>(data.Result));
                return result;
            }
            catch (Exception e)
            {
                return new Task<List<ClientType_Result>>(() => new List<ClientType_Result>());
            }
        }

        public Task<List<InstrumentsState_Result>> Get_InstrumentsState_From_Source(byte Flow)
        {
            try
            {
                var data = Get_InstrumentsState_Data_From_Source(Flow);
                var result = Task.Run(() => CommonCodes.Convert_Dataset_To_Object<InstrumentsState_Result>(data.Result));
                return result;
            }
            catch (Exception e)
            {
                return new Task<List<InstrumentsState_Result>>(() => new List<InstrumentsState_Result>());
            }
        }

        private List<Instrument_Trade_Result> Get_Instrument_Trade_From_Date_To_Date_From_Source
            (long Instrument_Code, int Date_From, int Date_To)
        {
            try
            {
                var task = Task.Run(() => Get_Instrument_Trade_Data_From_Date_To_Date_From_Source(Instrument_Code, Date_From, Date_To));

                var task_complited = task.Wait(10000);

                if (task.IsCompleted)
                {
                    var result = CommonCodes.Convert_Dataset_To_Object<Instrument_Trade_Result>(task.Result);
                    return result;
                }
                else
                    return new List<Instrument_Trade_Result>();
            }
            catch (Exception e)
            {
                return new List<Instrument_Trade_Result>();
            }
        }

        public List<Instrument_Trade_View_Model> get_Trade_From_Date_To_Date(long Instrument_Code,
            DateTime from_datetime, DateTime to_datetime)
        {
            try
            {
                int from_date = 0;
                int to_date = 0;

                int.TryParse(from_datetime.ToString("yyyyMMdd", CultureInfo.InvariantCulture), out from_date);
                int.TryParse(to_datetime.ToString("yyyyMMdd", CultureInfo.InvariantCulture), out to_date);

                DateTimeFormatInfo datetime_format_provider = new DateTimeFormatInfo()
                { FullDateTimePattern = "yyyyMMdd" };

                if (from_date <= 0 || to_date <= 0 || to_datetime < from_datetime)
                    return null;

                var instrument_trade_Results = Get_Instrument_Trade_From_Date_To_Date_From_Source(Instrument_Code,
                    from_date, to_date);

                var instrument_trade_View_Models = instrument_trade_Results.Select(s => new Instrument_Trade_View_Model()
                {
                    InsCode = s.InsCode,
                    DEven = DateTime.ParseExact(s.DEven.ToString(), "yyyyMMdd", datetime_format_provider),
                    PriceFirst = s.PriceFirst,
                    PClosing = s.PClosing,
                    PDrCotVal = s.PDrCotVal,
                    PriceChange = s.PriceChange,
                    PriceMax = s.PriceMax,
                    PriceMin = s.PriceMin,
                    PriceYesterday = s.PriceYesterday,
                    QTotCap = s.QTotCap,
                    QTotTran5J = s.QTotTran5J,
                    ZTotTran = s.ZTotTran,
                }).ToList();

                return instrument_trade_View_Models.ToList();
            }
            catch (Exception e)
            {
                return new List<Instrument_Trade_View_Model>();
            }
        }

        public Task<InstrumentsState_Result> Get_Instruments_State_In_Special_Date_From_Source(long Instrument_Code, DateTime Datetime)
        {
            try
            {
                int date = 0;

                int.TryParse(Datetime.ToString("yyyyMMdd", CultureInfo.InvariantCulture), out date);

                DateTimeFormatInfo datetime_format_provider = new DateTimeFormatInfo()
                { FullDateTimePattern = "yyyyMMdd" };

                if (date <= 0)
                    return new Task<InstrumentsState_Result>(() => new InstrumentsState_Result());

                var data = Get_Instruments_State_Data_In_Special_Date_From_Source(Instrument_Code, date);
                var result = Task.Run(() => CommonCodes.Convert_Dataset_To_Object<InstrumentsState_Result>(data.Result).FirstOrDefault());
                return result;
            }
            catch (Exception e)
            {
                return new Task<InstrumentsState_Result>(() => new InstrumentsState_Result());
            }
        }

        private Task<DataSet> Get_TradeOneDay_Data_From_Source(int date, byte Flow)
        {
            try
            {
                return Task.Run(() => Get_TradeOneDay_Data_From_Source_Task(date, Flow));
            }
            catch (Exception e)
            {
                return new Task<DataSet>(() => null);
            }
        }

        private Task<DataSet> Get_Instrument_Data_From_Source(byte Flow)
        {
            try
            {
                return Task.Run(() => Get_Instrument_Data_From_Source_Task(Flow));
            }
            catch (Exception e)
            {
                return new Task<DataSet>(() => null);
            }
        }

        private DataSet Get_TradeOneDay_Data_From_Source_Task(int date, byte Flow)
        {
            try
            {
                return _tsetmc.TradeOneDay(tsem_username, tsem_password, date, Flow);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private DataSet Get_Instrument_Data_From_Source_Task(byte Flow)
        {
            try
            {
                return _tsetmc.Instrument(tsem_username, tsem_password, Flow);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private DataSet Get_Instrument_Data_From_Source_Force(byte Flow)
        {
            try
            {
                return _tsetmc.Instrument(tsem_username, tsem_password, Flow);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private Task<DataSet> Get_Instruments_State_Data_In_Special_Date_From_Source(long Instrument_Code, int Date)
        {
            try
            {
                return Task.Run(() => _tsetmc.InstrumentsStateChange(tsem_username, tsem_password, (int)Instrument_Code, Date));
            }
            catch (Exception e)
            {
                return new Task<DataSet>(() => null);
            }
        }

        private DataSet Get_Instrument_Trade_Data_From_Date_To_Date_From_Source(long Instrument_Code, int Date_From, int Date_To)
        {
            try
            {
                return _tsetmc.InstTrade(tsem_username, tsem_password, Instrument_Code, Date_From, Date_To);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private DataSet Get_Best_Limit_One_Instrument_From_Source(long Instrument_Code)
        {
            try
            {
                return _tsetmc.BestLimitOneIns(tsem_username, tsem_password, Instrument_Code);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Task<List<Static_Thresholds_Result_View_Model>> Get_StaticThresholds_From_Source(byte Flow)
        {
            try
            {
                var data = Get_StaticThresholds_Data_From_Source(Flow);
                Task<List<Static_Thresholds_Result_View_Model>> result = Task.Run(() => CommonCodes.Convert_Dataset_To_Object<Static_Thresholds_Result_View_Model>(data.Result));

                return result;
            }
            catch (Exception e)
            {
                return new Task<List<Static_Thresholds_Result_View_Model>>(() => new List<Static_Thresholds_Result_View_Model>());
            }
        }

        public Task<List<BestLimitOneIns_Result_View_Model>> Get_Best_Limit_One_Instrument(long Instrument_Code)
        {
            try
            {
                var data = Get_Best_Limit_One_Instrument_From_Source(Instrument_Code);
                Task<List<BestLimitOneIns_Result_View_Model>> result = Task.Run(() => CommonCodes.Convert_Dataset_To_Object<BestLimitOneIns_Result_View_Model>(data));

                return result;
            }
            catch (Exception e)
            {
                return new Task<List<BestLimitOneIns_Result_View_Model>>(() => new List<BestLimitOneIns_Result_View_Model>());
            }
        }

        public Task<List<Trade_Last_Day_Result_View_Model>> Get_TradeLastDay_From_Source(byte Flow)
        {
            try
            {
                var data = Get_TradeLastDay_Data_From_Source(Flow);
                Task<List<Trade_Last_Day_Result_View_Model>> result = Task.Run(() => CommonCodes.Convert_Dataset_To_Object<Trade_Last_Day_Result_View_Model>(data.Result));
                return result;
            }
            catch (Exception e)
            {
                return new Task<List<Trade_Last_Day_Result_View_Model>>(() => new List<Trade_Last_Day_Result_View_Model>());
            }
        }

        private Task<DataSet> Get_TradeLastDay_Data_From_Source(byte Flow)
        {
            try
            {
                return Task.Run(() => Get_TradeLastDay_Data_From_Source_Task(Flow));
            }
            catch (Exception e)
            {
                return new Task<DataSet>(() => null);
            }
        }

        private DataSet Get_TradeLastDay_Data_From_Source_Task(byte Flow)
        {
            try
            {
                return _tsetmc.TradeLastDay(tsem_username, tsem_password, Flow);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private Task<DataSet> Get_StaticThresholds_Data_From_Source(byte Flow)
        {
            try
            {
                return Task.Run(() => Get_StaticThresholds_Data_From_Source_Task(Flow));
            }
            catch (Exception e)
            {
                return new Task<DataSet>(() => null);
            }
        }

        private DataSet Get_StaticThresholds_Data_From_Source_Task(byte Flow)
        {
            try
            {
                return _tsetmc.StaticThresholds(tsem_username, tsem_password, Flow);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private Task<DataSet> Get_TradeOneDayAll_Data_From_Source(int date, byte Flow)
        {
            try
            {
                return Task.Run(() => Get_TradeOneDayAll_Data_From_Source_Task(date, Flow));
            }
            catch (Exception e)
            {
                return new Task<DataSet>(() => null);
            }
        }

        private DataSet Get_TradeOneDayAll_Data_From_Source_Task(int date, byte Flow)
        {
            try
            {
                return _tsetmc.TradeOneDayAll(tsem_username, tsem_password, date, Flow);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private Task<DataSet> Get_InstrumentsState_Data_From_Source(byte Flow)
        {
            try
            {
                return Task.Run(() => Get_InstrumentsState_Data_From_Source_Task(Flow));
            }
            catch (Exception e)
            {
                return new Task<DataSet>(() => null);
            }
        }

        private DataSet Get_InstrumentsState_Data_From_Source_Task(byte Flow)
        {
            try
            {
                return _tsetmc.InstrumentsState(tsem_username, tsem_password, Flow);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        private Task<DataTable> Get_ClientType_Data_From_Source()
        {
            try
            {
                return Task.Run(() => Get_ClientType_Data_From_Source_Task());
            }
            catch (Exception e)
            {
                return new Task<DataTable>(() => null);
            }
        }

        private DataTable Get_ClientType_Data_From_Source_Task()
        {
            try
            {
                return _tsetmc.ClientType(tsem_username, tsem_password);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Instrument_Trade_View_Model get_Trade_In_Date(long Instrument_Code, DateTime datetime)
        {
            var data = get_Trade_From_Date_To_Date(Instrument_Code, datetime, datetime);

            return data.FirstOrDefault();
        }
    }
}