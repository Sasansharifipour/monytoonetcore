export interface users_get_price_packages_Result {
  iD: number;
  name: string;
  description: string;
  duration: number;
  price: number;
  image_url: string;
}
