﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.TGJU
{
    public class TGJU_Data_View_Model
    {
        public int id { get; set; } = 0;

        public string title { get; set; } = "";

        public string slug { get; set; } = "";

        public string p { get; set; } = "";

        public string o { get; set; } = "";

        public string h { get; set; } = "";

        public string l { get; set; } = "";

        public double? d { get; set; } = 0;

        public string dt { get; set; } = "";

        public double? dp { get; set; } = 0;

        public string t { get; set; } = "";

        public DateTime? updated_at { get; set; }
    }
}