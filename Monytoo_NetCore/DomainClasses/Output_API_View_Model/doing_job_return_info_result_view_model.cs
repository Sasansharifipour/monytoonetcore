﻿using DomainClasses.Output_API_View_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Output_API_View_Model
{
    public class doing_job_return_info_result_view_model : doing_job_result_view_model
    {
        public object info { get; set; }
    }
}
