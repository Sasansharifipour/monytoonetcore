import { get_base_data_model } from "./get_base_data_model.interface";

export interface search_profile_data_model extends get_base_data_model {
  search_str: string;
  type_id: number;
}
