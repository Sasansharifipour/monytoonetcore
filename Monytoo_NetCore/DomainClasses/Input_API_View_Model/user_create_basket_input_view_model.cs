﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class user_create_basket_input_view_model : get_base_data_model
    {
        public string name { get; set; } = "";

        public int basket_balance { get; set; } = 0;

        public int type_id { get; set; } = 0;
    }
}
