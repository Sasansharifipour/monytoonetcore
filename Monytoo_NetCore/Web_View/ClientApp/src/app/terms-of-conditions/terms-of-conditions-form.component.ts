import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../shared/services/users.services';
import { UserRegistration } from '../shared/models/inline_models/user.registration.interface';
import { register_model } from '../shared/models/output_models/register_model.interface';
import { privacy_result_model } from '../shared/models/input_models/privacy_result_model.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'app-terms-of-conditions-form',
  templateUrl: './terms-of-conditions-form.component.html'
})
export class TermsOfConditionsComponent {
  private result_model: privacy_result_model;
  private policy: string;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService, private _location: Location) {

    this.userService.get_terms_of_conditions().subscribe(result => {
      this.result_model = result;

      if (this.result_model.error_Id == "0") {
        this.policy = this.result_model.text;
      }

    });
  }

  backClicked() {
    this._location.back();
  }
}
