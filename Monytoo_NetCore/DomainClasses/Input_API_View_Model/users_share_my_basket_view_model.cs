﻿using DomainClasses.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClasses.Input_API_View_Model
{
    public class users_share_my_basket_view_model : get_base_data_model
    {
        public int basket_id { get; set; } = 0;

        public string name { get; set; } = "";

        public int duration { get; set; } = 0;

        public int price { get; set; } = 0;
    }
}
