﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.RSS
{
    public class RSS_eghtesadonline
    {
        public string title { get; set; } = "";

        public string description { get; set; } = "";

        public string link { get; set; } = "";

        public string pubDate { get; set; } = "";

        public string guid { get; set; } = "";
    }
}