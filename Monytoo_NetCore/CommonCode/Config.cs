﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using static CommonCode.CommonCodes;

namespace CommonCode
{
    public static class Config
    {
        public static int week_days_count { get; set; } = 7;

        public static int week_open_days_count { get; set; } = 4;

        public static int eve_days_count { get; set; } = 10;

        public static string get_root_path()
        {
            string web_path_key = "web_path_key";
            if (ConfigurationManager.AppSettings[web_path_key] == null)
                return AppDomain.CurrentDomain.BaseDirectory;
            else
                return ConfigurationManager.AppSettings[web_path_key];
        }

        public static string get_info_from_config(string key)
        {
            if (ConfigurationManager.AppSettings[key] == null)
                return "";
            else
                return ConfigurationManager.AppSettings[key];
        }

        public static string get_default_log_file_for_factors()
        {
            return get_root_path() + @"Logging\log_factor.txt";
        }

        public static string get_default_log_file_for_errors()
        {
            return get_root_path() + @"Logging\log_errors.txt";
        }

        public static string get_default_image_path_for_rsi_post(RSI_POST_enum post_type)
        {
            switch (post_type)
            {
                case RSI_POST_enum.Enter_Bought:
                    return get_root_path() + @"\Images\RSI_POST_IMAGES\Enter_Buy_Saturation.jpg";
                case RSI_POST_enum.Enter_Sales:
                    return get_root_path() + @"\Images\RSI_POST_IMAGES\Enter_Sale_Saturation.jpg";
                case RSI_POST_enum.Exit_Bought:
                    return get_root_path() + @"\Images\RSI_POST_IMAGES\Exit_Buy_Saturation.jpg";
                case RSI_POST_enum.Exit_Sales:
                    return get_root_path() + @"\Images\RSI_POST_IMAGES\Exit_Sale_Saturation.jpg";
                default:
                    return "";
            }
        }

        public static (long, string) get_board_user_info(Board_Type_Element_enum board_Type)
        {
            switch (board_Type)
            {
                case Board_Type_Element_enum.Smart_Money_Entry:
                case Board_Type_Element_enum.Smart_Money_Outflow:
                    return get_user_info_from_config("Smart_Money");
                default:
                    return get_user_info_from_config(board_Type.ToString());
            }
        }

        public static string image_saving_path(saving_image_location value)
        {
            switch (value)
            {
                case saving_image_location.post_image:
                    return get_info_from_config("post_image_saving_path");
                case saving_image_location.profile_image:
                    return get_info_from_config("profile_image_saving_path");
                case saving_image_location.temp_image:
                    return get_info_from_config("temp_image_saving_path");
                default:
                    return "";
            }
        }

        public static string get_image_path_for_special_macd_post(bool is_sell, double macd_histogram)
        {
            string file_name = "";

            file_name = is_sell ? "Sales_" : "Bought_";
            file_name += macd_histogram < 0 ? "Negative" : "Positive";

            return string.Format(@"{0}\Images\MACD_SPECIAL_POST_IMAGES\{1}.jpg", get_root_path(), file_name);
        }

        public static string get_image_path_for_macd_post(bool is_sell, double macd_histogram)
        {
            string file_name = "";

            file_name = is_sell ? "Sales_" : "Bought_";
            file_name += macd_histogram < 0 ? "Negative" : "Positive";

            return string.Format(@"{0}\Images\MACD_POST_IMAGES\{1}.jpg", get_root_path(), file_name);
        }

        public static string get_image_path_for_codal_post(int idx)
        {
            return string.Format(@"{0}\Images\CODAL_POST_IMAGES\{1}.jpg", get_root_path(), idx);
        }

        public static List<string> get_rss_links()
        {
            List<string> results = new List<string>();

            if (ConfigurationManager.AppSettings["RSS_URLs"] == null)
                return new List<string>();

            string rss = ConfigurationManager.AppSettings["RSS_URLs"];
            results = rss.Split(',').ToList();
            return results;
        }

        public static string get_post_default_image_path()
        {
            string image_path = "";

            if (ConfigurationManager.AppSettings["rss_post_default_image_path"] == null)
                image_path = "";

            image_path = ConfigurationManager.AppSettings["rss_post_default_image_path"];

            return image_path;
        }

        public static (long, string) get_user_info_from_config(string title)
        {
            long user_id = 0;
            string user_id_config = "";
            string secret_key = "";

            string user_id_title = title + "_user_id";
            string secret_key_title = title + "_secret_key";

            if (ConfigurationManager.AppSettings[user_id_title] == null)
                user_id_config = "";

            user_id_config = ConfigurationManager.AppSettings[user_id_title];
            long.TryParse(user_id_config, out user_id);

            if (ConfigurationManager.AppSettings[secret_key_title] == null)
                secret_key = "";

            secret_key = ConfigurationManager.AppSettings[secret_key_title];

            return (user_id, secret_key);
        }

        public static List<(string, string)> get_Tablokhani_info_from_config()
        {
            List<(string, string)> result = new List<(string, string)>();
            string tablokhani_string = "";
            string tablokhani_tables = "tablokhani_tables";

            if (ConfigurationManager.AppSettings[tablokhani_tables] == null)
                return new List<(string, string)>();

            tablokhani_string = ConfigurationManager.AppSettings[tablokhani_tables];

            var tablokhani_patterns = tablokhani_string.Split(',');

            foreach (var item in tablokhani_patterns)
            {
                var one_string_patterns = item.Split('-');

                if (one_string_patterns.Length >= 2)
                    result.Add((one_string_patterns[0], one_string_patterns[1]));
            }

            return result;
        }

        public static string get_unique_name(string image_path, string image_extention)
        {
            string file_name = Guid.NewGuid().ToString() + image_extention;

            while (File.Exists(image_path + file_name))
                file_name = Guid.NewGuid().ToString() + image_extention;

            return file_name;
        }

        public static string get_tsem_username()
        {
            string data = "";

            if (ConfigurationManager.AppSettings["tsem_username"] == null)
                data = "";

            data = ConfigurationManager.AppSettings["tsem_username"];

            return data;
        }

        public static string get_tsem_password()
        {
            string data = "";

            if (ConfigurationManager.AppSettings["tsem_password"] == null)
                data = "";

            data = ConfigurationManager.AppSettings["tsem_password"];

            return data;
        }
    }
}
