export interface users_get_comment_likes_list_Result {
  iD: number;
  name: string;
  lastname: string;
  username: string;
  is_followed: string;
  r_number: number;
  user_type: number;
  profile_image_t: number;
}
