import { get_base_data_model } from "./get_base_data_model.interface";

export interface get_post_image_view_model extends get_base_data_model {
  image_id: number;
  post_id: number;
}
