﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Users
{
    public class user_profile_data_view_model
    {
        public string username { get; set; }

        public string name { get; set; }

        public string lastname { get; set; }

        public string invitation_code { get; set; }

        public double? strength { get; set; }

        public int? post_count { get; set; }

        public long? follower_count { get; set; }

        public long? following_count { get; set; }

        public int? user_type { get; set; }

        public bool? is_followed { get; set; }

        public long? profile_image_t { get; set; }

        public List<user_profile_information_view_model> information_fields { get; set; }
    }
}