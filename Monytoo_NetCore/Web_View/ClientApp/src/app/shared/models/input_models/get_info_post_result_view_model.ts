import { result_model } from "./result_model.interface";
import { users_get_shared_accounts_Result } from "./users_get_shared_accounts_Result";

export interface get_info_post_result_view_model extends result_model {
  informations: users_get_shared_accounts_Result[];
}

