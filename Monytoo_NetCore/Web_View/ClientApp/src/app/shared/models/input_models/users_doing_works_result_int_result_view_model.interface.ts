import { result_model } from "./result_model.interface";

export interface users_doing_works_result_int_result_view_model extends result_model {
  informations: number;
}

