﻿using DomainClasses.Common;
using DomainClasses.Input_API_View_Model;
using DomainClasses.Output_API_View_Model;
using DomainClasses.Post;
using DomainClasses.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Web_View.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IuserServices _userService;

        public UsersController(IuserServices iuserServices)
        {
            _userService = iuserServices;
        }

        [HttpGet]
        [Route("get_system_messages")]
        public privacy_result_model get_system_messages()
        {
            var result = _userService.get_system_messages();
            return result;
        }

        [HttpGet]
        [Route("get_terms_of_conditions")]
        public privacy_result_model get_terms_of_conditions()
        {
            privacy_result_model result = _userService.get_terms_of_conditions();
            return result;
        }

        [Route("get_privacy_policy")]
        [HttpGet]
        public privacy_result_model get_privacy_policy()
        {
            privacy_result_model result = _userService.get_privacy_policy();
            return result;
        }

        [Route("check_username_availability")]
        [HttpPost]
        public check_user_name_available_result_model check_username_availability([FromBody] check_user_name_available_model model)
        {
            check_user_name_available_result_model result = _userService.check_username_availability(model.username);
            return result;
        }

        [Route("register")]
        [HttpPost]
        public register_result_model register([FromBody] register_model model)
        {
            register_result_model result = _userService.register(model);
            return (result);
        }

        [Route("verify_user_session")]
        [HttpPost]
        public verify_by_mobile_number_result_model verify_user_session([FromBody] verify_by_mobile_number_model model)
        {
            verify_by_mobile_number_result_model result = _userService.users_verify_user_session(model);
            return (result);
        }

        [Route("update_profile_data")]
        [HttpPost]
        public update_profile_data_result_model update_profile_data([FromBody] update_profile_data_model model)
        {
            update_profile_data_result_model result = _userService.update_profile_data(model);
            return (result);
        }

        [Route("update_information_field_value")]
        [HttpPost]
        public List<update_profile_data_result_model> update_information_field_value([FromBody] update_information_field_value_model model)
        {
            List<update_profile_data_result_model> result = _userService.update_information_field_value(model);
            return (result);
        }

        [HttpPost]
        [Route("follow")]
        public update_profile_data_result_model follow([FromBody] get_profile_data_model model)
        {
            update_profile_data_result_model result = _userService.follow_user(model);
            return (result);
        }

        [HttpPost]
        [Route("unfollow")]
        public update_profile_data_result_model unfollow([FromBody] get_profile_data_model model)
        {
            update_profile_data_result_model result = _userService.unfollow_user(model);
            return (result);
        }

        [HttpPost]
        [Route("get_profile_data")]
        public get_profile_data_result_model get_profile_data([FromBody] get_profile_data_model model)
        {
            get_profile_data_result_model result = _userService.get_profile_data(model);
            return (result);
        }

        [HttpPost]
        [Route("update_profile_image")]
        public update_profile_data_result_model update_profile_image()
        {
            IFormFile image_info = HttpContext.Request.Form.Files.Count > 0 ?
                HttpContext.Request.Form.Files["Image"] : null;

            string api_key = HttpContext.Request.Form.Count > 0 ?
                HttpContext.Request.Form["API_Key"].ToString() : null;

            update_profile_image_view_model model = new update_profile_image_view_model() { API_Key = api_key, image = image_info };

            update_profile_data_result_model result = _userService.update_profile_image(model);

            return (result);
        }

        [HttpPost]
        [Route("send_post")]
        public send_post_result_view_model send_post()
        {
            IFormFile image_info = HttpContext.Request.Form.Files.Count > 0 ?
                HttpContext.Request.Form.Files["Image"] : null;

            string api_key = HttpContext.Request.Form.Count > 0 ?
                HttpContext.Request.Form["API_Key"].ToString() : null;

            string caption = HttpContext.Request.Form.Count > 0 ?
                HttpContext.Request.Form["Caption"].ToString() : null;

            string _from_user_id = HttpContext.Request.Form.Count > 0 ?
                HttpContext.Request.Form["From_user_id"].ToString() : null;

            long? from_user_id = null;
            long temp_from_user_id;
            long.TryParse(_from_user_id, out temp_from_user_id);

            from_user_id = temp_from_user_id;

            send_post_view_model model = new send_post_view_model()
            { API_Key = api_key, image = image_info, caption = caption, from_user_id = from_user_id };

            send_post_result_view_model result = _userService.send_post(model);

            return (result);
        }

        [HttpGet]
        [Route("get_profile_image")]
        public IActionResult get_profile_image([FromQuery] get_profile_data_model model)
        {
            var result = _userService.get_profile_image(model);

            if (result.Error_Id.Trim() == "0")
            {
                return File(result.image_data, "image/jpeg");
            }

            return NotFound();
        }

        [HttpPost]
        [Route("users_get_profile_posts")]
        public user_get_profile_post_result_view_model users_get_profile_posts([FromBody] get_user_follow_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_profile_posts(model);

            return (result);
        }

        [HttpPost]
        [Route("users_search")]
        public user_get_profile_post_result_view_model users_search([FromBody] search_profile_data_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_search(model);

            return (result);
        }

        [HttpPost]
        [Route("users_home_posts")]
        public user_get_profile_post_result_view_model users_home_posts([FromBody] get_post_by_index_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_home_posts(model);

            return (result);
        }

        [HttpPost]
        [Route("users_explore_posts")]
        public user_get_profile_post_result_view_model users_explore_posts([FromBody] get_post_by_index_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_explore_posts(model);

            return (result);
        }

        [HttpGet]
        [Route("users_get_image")]
        public HttpResponseMessage users_get_image([FromQuery] get_post_image_view_model model)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var result = _userService.users_get_image(model);

            if (result.Error_Id.Trim() == "0")
            {
                Byte[] b = (result.image_data);
                response.Content = new ByteArrayContent(b);
                response.Content.LoadIntoBufferAsync(b.Length).Wait();
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpeg");
            }
            else
            {
                response.StatusCode = HttpStatusCode.Gone;
            }

            return response;
        }

        [HttpPost]
        [Route("users_get_single_post")]
        public user_get_profile_post_result_view_model users_get_single_post([FromBody] get_post_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_single_post(model);

            return (result);
        }

        [HttpPost]
        [Route("users_like_post")]
        public user_get_profile_post_result_view_model users_like_post([FromBody] get_post_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_like_post(model);

            return (result);
        }

        [HttpPost]
        [Route("users_unlike_post")]
        public user_get_profile_post_result_view_model users_unlike_post([FromBody] get_post_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_unlike_post(model);

            return (result);
        }


        [HttpPost]
        [Route("users_get_profile_followers")]
        public user_get_profile_post_result_view_model users_get_profile_followers([FromBody] get_user_follow_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_profile_followers(model);

            return (result);
        }

        [HttpPost]
        [Route("users_get_profile_followings")]
        public user_get_profile_post_result_view_model users_get_profile_followings([FromBody] get_user_follow_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_profile_followings(model);

            return (result);
        }

        [HttpPost]
        [Route("users_get_likes_list")]
        public user_get_profile_post_result_view_model users_get_likes_list([FromBody] get_post_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_likes_list(model);

            return (result);
        }

        [HttpPost]
        [Route("users_edit_post")]
        public update_profile_data_result_model users_edit_post([FromBody] get_post_view_model model)
        {
            update_profile_data_result_model result = _userService.users_edit_post(model);

            return (result);
        }

        [HttpPost]
        [Route("users_delete_profile_picture")]
        public delete_info_result_view_model users_delete_profile_picture([FromBody] get_base_data_model model)
        {
            delete_info_result_view_model result = _userService.users_delete_profile_picture(model);

            return (result);
        }

        [HttpPost]
        [Route("users_delete_post")]
        public delete_info_result_view_model users_delete_post([FromBody] get_post_view_model model)
        {
            delete_info_result_view_model result = _userService.users_delete_post(model);

            return (result);
        }

        [HttpPost]
        [Route("users_get_price_packages")]
        public user_get_profile_post_result_view_model users_get_price_packages([FromBody] get_base_data_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_price_packages(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_create_factor")]
        public user_get_profile_post_result_view_model users_create_factor([FromBody] users_create_factor_API_View_Model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_create_factor(model);

            return (result);
        }

        [HttpPost]
        [Route("users_get_notification_list")]
        public user_get_profile_post_result_view_model users_get_notification_list([FromBody] user_get_notification_API_View_Model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_notification_list(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_seen_notification_list")]
        public update_profile_data_result_model users_seen_notification_list([FromBody] get_base_data_model model)
        {
            update_profile_data_result_model result = _userService.users_seen_notification_list(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_send_comment")]
        public add_comment_result_view_model users_send_comment([FromBody] UserSendCommentViewModel model)
        {
            add_comment_result_view_model result = _userService.users_send_comment(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_delete_comment")]
        public delete_info_result_view_model users_delete_comment([FromBody] CommentBaseViewModel model)
        {
            delete_info_result_view_model result = _userService.users_delete_comment(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_report_post")]
        public doing_job_result_view_model users_report_post([FromBody] PostBaseViewModel model)
        {
            doing_job_result_view_model result = _userService.users_report_post(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_get_comment_reply_list")]
        public user_get_profile_post_result_view_model users_get_comment_reply_list([FromBody] UserGetCommentReplyListViewModel model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_comment_reply_list(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_get_post_comments_list")]
        public user_get_profile_post_result_view_model users_get_post_comments_list([FromBody] UserGetPostCommentsListViewModel model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_post_comments_list(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_check_version")]
        public check_version_result_view_model users_check_version([FromBody] get_version_post_input_view_model model)
        {
            check_version_result_view_model result = _userService.users_check_version(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_get_shared_accounts")]
        public get_info_post_result_view_model users_get_shared_accounts([FromBody] get_base_data_model model)
        {
            get_info_post_result_view_model result = _userService.users_get_shared_accounts(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_get_follow_suggestion_list")]
        public get_info_post_result_view_model users_get_follow_suggestion_list([FromBody] get_base_data_model model)
        {
            get_info_post_result_view_model result = _userService.users_get_follow_suggestion_list(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_like_comment")]
        public doing_job_result_view_model users_like_comment([FromBody] CommentBaseViewModel model)
        {
            doing_job_result_view_model result = _userService.users_like_comment(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_unlike_comment")]
        public doing_job_result_view_model users_unlike_comment([FromBody] CommentBaseViewModel model)
        {
            doing_job_result_view_model result = _userService.users_unlike_comment(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_get_comment_likes_list")]
        public get_info_post_result_view_model users_get_comment_likes_list([FromBody] UserGetCommentReplyListViewModel model)
        {
            get_info_post_result_view_model result = _userService.users_get_comment_likes_list(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_create_basket")]
        public doing_job_result_view_model users_create_basket([FromBody] user_create_basket_input_view_model model)
        {
            doing_job_result_view_model result = _userService.users_create_basket(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_create_basket_order")]
        public doing_job_result_view_model users_create_basket_order([FromBody] user_create_basket_order_input_view_model model)
        {
            doing_job_result_view_model result = _userService.users_create_basket_order(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_search_stock")]
        public user_get_profile_post_result_view_model users_search_stock([FromBody] search_profile_data_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_search_stock(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_basket_cancel_order")]
        public doing_job_result_view_model users_basket_cancel_order([FromBody] users_basket_cancel_order_input_view_model model)
        {
            doing_job_result_view_model result = _userService.users_basket_cancel_order(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_get_basket_order_history")]
        public user_get_profile_post_result_view_model users_get_basket_order_history([FromBody] users_get_basket_order_history_input_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_basket_order_history(model);

            return (result);
        }
        
        [HttpPost]
        [Route("users_get_basket_data")]
        public user_get_profile_post_result_view_model users_get_basket_data([FromBody] users_get_basket_order_history_input_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_basket_data(model);

            return (result);
        }
        //--------------------------------------------
        [HttpPost]
        [Route("users_get_profile_baskets")]
        public user_get_profile_post_result_view_model users_get_profile_baskets([FromBody] users_get_profile_baskets_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_profile_baskets(model);

            return (result);
        }

        [HttpPost]
        [Route("users_get_share_baskets")]
        public user_get_profile_post_result_view_model users_get_share_baskets([FromBody] users_get_profile_baskets_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_share_baskets(model);

            return (result);
        }

        [HttpPost]
        [Route("users_create_factor_wallet")]
        public doing_job_return_info_result_view_model users_create_factor_wallet([FromBody] users_create_factor_wallet_input_view_model model)
        {
            doing_job_return_info_result_view_model result = _userService.users_create_factor_wallet(model);

            return (result);
        }

        [HttpPost]
        [Route("users_add_item_to_private_basket")]
        public doing_job_result_view_model users_add_item_to_private_basket([FromBody] users_add_item_to_private_basket_input_view_model model)
        {
            doing_job_result_view_model result = _userService.users_add_item_to_private_basket(model);

            return (result);
        }

        [HttpPost]
        [Route("users_get_Private_baskets")]
        public user_get_profile_post_result_view_model users_get_Private_baskets([FromBody] users_get_profile_baskets_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_Private_baskets(model);

            return (result);
        }

        [HttpPost]
        [Route("users_get_my_balance")]
        public user_get_profile_post_result_view_model users_get_my_balance([FromBody] get_base_data_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_my_balance(model);

            return (result);
        }

        [HttpPost]
        [Route("users_share_my_basket")]
        public user_get_profile_post_result_view_model users_share_my_basket([FromBody] users_share_my_basket_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_share_my_basket(model);

            return (result);
        }

        [HttpPost]
        [Route("users_delete_basket_share_package")]
        public doing_job_result_view_model users_delete_basket_share_package([FromBody] basket_share_package_input_view_model model)
        {
            doing_job_result_view_model result = _userService.users_delete_basket_share_package(model);

            return (result);
        }

        [HttpPost]
        [Route("users_get_basket_share_packages")]
        public user_get_profile_post_result_view_model users_get_basket_share_packages([FromBody] get_basket_share_package_input_view_model model)
        {
            user_get_profile_post_result_view_model result = _userService.users_get_basket_share_packages(model);

            return (result);
        }

        [HttpPost]
        [Route("users_buy_basket_share_packages")]
        public doing_job_result_view_model users_buy_basket_share_packages([FromBody] basket_share_package_input_view_model model)
        {
            doing_job_result_view_model result = _userService.users_buy_basket_share_packages(model);

            return (result);
        }

        [HttpPost]
        [Route("users_set_basket_item_price")]
        public doing_job_result_view_model users_set_basket_item_price([FromBody] user_set_basket_item_price_view_model model)
        {
            doing_job_result_view_model result = _userService.users_set_basket_item_price(model);

            return (result);
        }


        [Route("cafebazar_payment_confirm")]
        [HttpPost]
        public doing_job_result_view_model cafebazar_Payment_Confirm(cafebazar_confirm_view_model model)
        {
            doing_job_result_view_model result = _userService.user_cafebazar_confirm_payment(model);

            return (result);
        }


    }
}
