﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DomainClasses.Instrument
{
    public class Instrument_Update_Information_Field_View_Model
    {

        public long Instrument_Id { get; set; } = 0;

        public long Namad_Id { get; set; } = 0;

        public string Information_Field_Name { get; set; } = "";

        public string Information_Field_Value { get; set; } = "";
    }

}