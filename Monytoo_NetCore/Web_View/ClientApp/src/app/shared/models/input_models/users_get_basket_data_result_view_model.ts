import { result_model } from "./result_model.interface";
import { users_get_basket_info_Result } from "./users_get_basket_info_Result";

export interface users_get_basket_data_result_view_model extends result_model {
  informations: users_get_basket_info_Result[];
}

