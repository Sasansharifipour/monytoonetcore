﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Linq;

namespace CommonCode
{
    public static class CommonCodes
    {
        public static bool IsNullOrEmptyOrWhiteSpace(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            if (string.IsNullOrWhiteSpace(value))
                return true;

            return false;
        }

        public enum RSI_POST_enum
        {
            Exit_Sales = 1,
            Enter_Sales = 2,
            Exit_Bought = 3,
            Enter_Bought = 4
        }

        public enum Board_Type_Element_enum
        {
            Probable_Purchases_Queue_Tomorrow = 1,
            Probable_Sales_Queue_Tomorrow = 2,
            Smart_Money_Entry = 3,
            Smart_Money_Outflow = 4,
            Suspicious_Volumes = 5
        }

        public enum rss_provider_enum
        {
            Null = 0,
            NabzeBourse = 1,
            EghtesadOnline = 2,
            EghtesadNews = 3,
            BourseNews = 4,
            DoyaeEghtesad = 5
        }

        public enum provider_enum
        {
            Bourse = 1,
            TGJU = 2
        }

        public enum error_enum
        {
            Authentication_failed = -1,
            User_is_blocked = -2,
            Wait_atleast_one_minute_to_resend_the_verification_code = -3,
            Sending_verification_code_failed = -4,
            User_not_found = -5,
            Username_is_not_available = -6,
            verification_code_expired = -7,
            wrong_verification_code = -8,
            wrong_invitation_code = -9,
            unknown_error = -10,
            user_profile_not_found = -11,
            information_field_not_found = -12,
            posting_from_this_user_is_denied = -13,
            Is_followed_before = -14,
            Is_unfollowed_before = -15,
            Namad_exists = -16,
            This_user_is_not_an_expert = -17,
            Post_is_not_available = -18,
            Is_liked_before = -19,
            Post_Not_found = -20,
            Is_not_liked_before = -21,
            Following_user_not_found = -22,
            Post_is_not_yours = -23,
            Price_Pckage_is_not_available = -24,
            Coupon_not_found = -25,
            Coupon_expired = -26,
            Coupon_is_not_yours = -27,
            Coupon_used_before, _and_is_not_valid_any_more = -28,
            Factor_Not_found = -29,
            Comment_not_found = -30,
            Is_reported_before = -31,
            Minimum_balance_to_create_a_basket_is_100_milion_Rial = -32,
            your_virtual_balance_is_not_enough = -33,
            basket_is_not_yours = -34,
            not_enough_balance_for_this_order = -35,
            namad_not_found_for_basket = -36,
            price_is_not_in_range = -37,
            Namad_is_closed, _no_trade_is_allowed = -38,
            Namad_is_not_available_to_sell, you_donot_have_it_in_your_basket = -39,
            you_can_not_sell_this_Namad_id_more_than_you_have = -40,
            order_not_found = -41,
            order_can_not_be_cancelled = -42,
            basket_is_not_available = -43,
            basket_is_not_accessable_for_you = -44,
            minimum_payment_amount_is_100000_rial = -45,
            order_not_found_basket = -46,
            no_basket_to_evaluate = -47,
            basket_not_found = -48
        }

        public enum saving_image_location
        {
            profile_image,
            post_image,
            temp_image
        }

        public static string getColorCodeForCodalPosts(int rand)
        {
            switch (rand)
            {
                case 1:
                    return "071a41";
                case 2:
                    return "3d003a";
                case 3:
                    return "023502";
                case 4:
                    return "01604d";
                case 5:
                    return "540402";
                default:
                    return "180e4d";
            }
        }

        public static string get_namad_noee_string(int code)
        {
            switch (code)
            {
                case 67:
                case 69:
                case 68: return "شاخص";
                case 321: return " اختیار فولاد هرمزگان";
                case 322: return " اختیار خ اخزا(اسناد خزانه داری اسلامی)";
                case 323: return " اختیارف اخزا(اسناد خزانه داری اسلامی)";
                case 601: return " اختیار فروش تبعی(ذوب آهن اصفهان)";
                case 903: return " دارایی فکری";
                case 70:
                case 306:
                case 308:
                case 208:
                case 207:
                case 200:
                case 706:
                case 301: return "اوراق مشاركت";
                case 304:
                case 311:
                case 309:
                case 303:
                case 300:
                case 307:
                case 313:
                case 312: return "سهام عادی";
                case 600:
                case 602: return "اختیار";
                case 403:
                case 400:
                case 404: return "حق تقدم";
                case 305:
                case 315: return "صندوق سرمایه گذاری";
                case 801:
                case 802:
                case 803:
                case 804: return "سلف بورس انرژی";
                case 901:
                case 902:
                    return "انرژی";
                case 701: return "کالا";
                default: return "";
            }
        }

        public static string get_bazar_noee_string(int code)
        {
            switch (code)
            {
                case 0: return "عمومی - مشترک بین بورس و فرابورس";
                case 1: return " بورس";
                case 2: return "فرابورس";
                case 3: return "آتی";
                case 4: return "پایه فرابورس";
                case 5: return "پایه فرابورس (منتشر نمی شود)";
                case 6: return "بورس انرژی";
                case 7: return "بورس کالا";
                default: return "";
            }
        }

        public static rss_provider_enum Convert_URL_To_Rss_Provider_Enum(string url)
        {
            url = url.Trim().ToUpper();
            string nabzeBourse_url = "https://nabzebourse.com".ToUpper().Trim();
            string eghtesadonline_url = "https://www.eghtesadonline.com".ToUpper().Trim();
            string bourseNews_url = "https://www.boursenews.ir".ToUpper().Trim();
            string eghtesadNews_url = "https://www.eghtesadnews.com".ToUpper().Trim();
            string donyaeeghtesad_url = "https://donya-e-eqtesad.com".ToUpper().Trim();

            if (url.StartsWith(nabzeBourse_url))
                return rss_provider_enum.NabzeBourse;

            if (url.StartsWith(bourseNews_url))
                return rss_provider_enum.BourseNews;

            if (url.StartsWith(eghtesadNews_url))
                return rss_provider_enum.EghtesadNews;

            if (url.StartsWith(donyaeeghtesad_url))
                return rss_provider_enum.DoyaeEghtesad;

            return rss_provider_enum.Null;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetCurrentMethod()
        {
            var st = new StackTrace();
            var sf = st.GetFrame(1);

            return sf.GetMethod().Name;
        }

        public static string GetDesciption(this RSI_POST_enum value)
        {
            switch (value)
            {
                case RSI_POST_enum.Enter_Bought:
                    return "ورود به اشباع خريد";
                case RSI_POST_enum.Enter_Sales:
                    return "ورود به اشباع فروش";
                case RSI_POST_enum.Exit_Bought:
                    return "خروج از اشباع خريد";
                case RSI_POST_enum.Exit_Sales:
                    return "خروج از اشباع فروش";
                default:
                    return "";
            }
        }

        public static int GetCode(this provider_enum value)
        {
            return (int)value;
        }

        public static string GetDescription(this error_enum value)
        {
            return value.ToString().Replace('_', ' ');
        }

        public static string GetCode(this error_enum value)
        {
            return ((int)value).ToString();
        }

        public static (string, string) convert_API_Key_To_User_id_and_secret_key(string API_Key)
        {
            string User_id = "";
            string secret_key = "";

            var result = API_Key.Split('-');

            if (result.Count() != 2)
                return (User_id, secret_key);

            User_id = result[0];
            secret_key = result[1];

            return (User_id, secret_key);
        }

        public static (long, long) convert_Reference_To_User_id_and_factor_id(string reference)
        {
            long user_id = 0;
            long factor_id = 0;

            var result = reference.Split('-');

            if (result.Count() != 2)
                return (user_id, factor_id);

            long.TryParse(result[0], out user_id);
            long.TryParse(result[1], out factor_id);

            return (user_id, factor_id);
        }

        public static (long?, string) convert_inviter_id_code_To_inviter_id_and_inviter_code(string inviter_id_code_value)
        {
            long? inviter_id = null;
            string inviter_code = "";


            if (inviter_id_code_value != null)
            {
                var inviter_id_code = inviter_id_code_value.Split('-');

                long temp_long;

                if (inviter_id_code.Count() > 0)
                {
                    long.TryParse(inviter_id_code[0], out temp_long);
                    inviter_id = temp_long;
                }

                if (inviter_id_code.Count() > 1)
                    inviter_code = inviter_id_code[1];
            }

            return (inviter_id, inviter_code);
        }

        private static void save_log(Exception ex)
        {
            string path = Config.get_default_log_file_for_errors();

            if (!File.Exists(path))
                using (StreamWriter sw = File.CreateText(path))
                    sw.WriteLine(ex.Message);
            else
                using (StreamWriter sw = File.AppendText(path))
                    sw.WriteLine(ex.Message);
        }

        public static List<T> Convert_DataTable_To_Object<T>(DataTable dt) where T : new()
        {
            try
            {
                List<T> result = new List<T>();

                if (dt == null)
                    return new List<T>();

                foreach (DataRow item in dt.Rows)
                {
                    T item_Result = new T();

                    foreach (var prop in item_Result.GetType().GetProperties())
                        prop.SetValue(item_Result, Convert.ChangeType(item[prop.Name], prop.PropertyType));

                    result.Add(item_Result);
                }

                return result;
            }
            catch (Exception ex)
            {
                save_log(ex);

                return new List<T>();
            }
        }

        public static List<T> Convert_Dataset_To_Object<T>(DataSet data) where T : new()
        {
            try
            {
                List<T> result = new List<T>();

                if (data == null || data.Tables == null || data.Tables.Count != 1)
                    return new List<T>();

                DataTable dt = data.Tables[0];

                foreach (DataRow item in dt.Rows)
                {
                    T item_Result = new T();

                    foreach (var prop in item_Result.GetType().GetProperties())
                        prop.SetValue(item_Result, Convert.ChangeType(item[prop.Name], prop.PropertyType));

                    result.Add(item_Result);
                }

                return result;
            }
            catch (Exception ex)
            {
                save_log(ex);

                return new List<T>();
            }
        }

        public static string Change_Arabic_To_Persian(this string value)
        {
            return value.Replace("ي", "ی").Replace("ك", "ک");
        }

        public static string Remove_Number_From_String(this string value)
        {
            while ("0123456789".Contains(value[value.Length - 1]))
                value = value.Substring(0, value.Length - 1);

            return value;
        }

        public static List<T> Convert_RSS_Feed_To_Object<T>(IEnumerable<XElement> data) where T : new()
        {
            List<T> result = new List<T>();

            if (data == null || data.Count() <= 0)
                return new List<T>();

            foreach (XElement item in data)
            {
                T item_Result = new T();

                foreach (var prop in item_Result.GetType().GetProperties())
                    if (item.Element(prop.Name) != null)
                        if (prop.Name.Trim() == "enclosure")
                            prop.SetValue(item_Result, Convert.ChangeType(item.Element(prop.Name).Attribute("url").Value, prop.PropertyType));
                        else
                            prop.SetValue(item_Result, Convert.ChangeType(item.Element(prop.Name).Value, prop.PropertyType));
                result.Add(item_Result);
            }

            return result;
        }

        public static string get_instrument_status(string instrument_Status)
        {
            switch (instrument_Status.Trim())
            {
                case "O":
                case "A":
                    return "مجاز";
                case "AG":
                    return "مجاز-مسدود";
                case "AR":
                    return "مجاز-محفوظ";
                case "AS":
                    return "مجاز-متوقف";
                case "I":
                    return "ممنوع";
                case "IG":
                    return "ممنوع-مسدود";
                case "IR":
                    return "ممنوع-متوقف";
                case "IS":
                    return "ممنوع-محفوظ";
                default:
                    return "";
            }
        }
    }
}
