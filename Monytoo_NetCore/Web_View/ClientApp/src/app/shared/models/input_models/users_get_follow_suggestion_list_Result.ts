import { SafeUrl } from "@angular/platform-browser";

export interface users_get_follow_suggestion_list_Result {
  ID: number;
  name: string;
  lastname: string;
  username: string;
  user_type: number;
  profile_image_t: number;
  profile_image_data: SafeUrl;
}
