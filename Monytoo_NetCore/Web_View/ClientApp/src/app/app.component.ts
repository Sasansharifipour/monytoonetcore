import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor( private router: Router) {
  }

  ngOnInit() {
    let APIKEY = localStorage.getItem('API_KEY');

    if (APIKEY == null) {
      this.router.navigate(['']);
    }
    /*else {
      this.router.navigate(['home-page']);
    }*/
  }
}
