import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../shared/services/users.services';
import { register_model } from '../shared/models/output_models/register_model.interface';
import { MatDialog } from '@angular/material/dialog';
import { ErrorDialog } from '../error-dialoge/error-dialoge';
import { Router, ActivatedRoute } from '@angular/router';
import { UserApprove } from '../shared/models/inline_models/UserApprove';
import { verify_by_mobile_number_model } from '../shared/models/output_models/verify_by_mobile_number_model.interface';
import { verify_by_mobile_number_result_model } from '../shared/models/input_models/verify_by_mobile_number_result_model.interface';

@Component({
  selector: 'app-login-approve-form',
  templateUrl: './login-approve.html'
})
export class LoginApproveComponent implements OnInit {
  private result_model: verify_by_mobile_number_result_model;
  private mobile_number: string = '';
  private user_session_ID: string = '';

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string, private userService: UserService,
    private route: ActivatedRoute, private router: Router , private dialogService: MatDialog) {
  }

  ngOnInit() {
    this.mobile_number = this.route.snapshot.paramMap.get('mobile_number');
    this.user_session_ID = this.route.snapshot.paramMap.get('user_session_ID');
  }

  register({ value }: { value: UserApprove }) {

    let model: verify_by_mobile_number_model = { user_session_ID: this.user_session_ID, verification_code: value.code };

    this.userService.verify_by_mobile_number(model).subscribe(result => {

      this.result_model = result;

      if (this.result_model.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      } else {
        if (this.result_model.apI_KEY != undefined &&
          this.result_model.apI_KEY != null &&
          this.result_model.apI_KEY.trim() != '') {
          localStorage.setItem('API_KEY', this.result_model.apI_KEY.trim());
          localStorage.setItem('USER_ID', this.result_model.user_id.toString());
          this.router.navigate(['home-page']);
        }
      }

    });
  }


  resend_code() {

    let model: register_model = {
      mobile_number: this.mobile_number, firebase_id: null, invitation_code: null,
      lastname: null, name: null, username: null
    };

    this.userService.register(model).subscribe(result => {
      this.result_model = result;

      if (this.result_model.error_Id != "0") {
        const dialogRef = this.dialogService.open(ErrorDialog);

      }

    });
  }
}

